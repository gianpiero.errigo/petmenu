package petmenu.services;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.Hibernate;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ActiveProfiles;

import petmenu.BaseContainerSetup;
import petmenu.config.datasources.LiquibaseConfig;
import petmenu.config.datasources.ProductsDataSourceConfig;
import petmenu.config.datasources.UsersDataSourceConfig;
import petmenu.dtos.products.ComponentQuotaDTO;
import petmenu.dtos.products.IngredientQuotaDTO;
import petmenu.entities.products.ComponentQuota;
import petmenu.entities.products.Ingredient;
import petmenu.entities.products.IngredientQuota;
import petmenu.entities.products.Product;
import petmenu.services.products.ComponentQuotaService;
import petmenu.services.products.IngredientQuotaService;
import petmenu.services.products.IngredientService;

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ActiveProfiles("integrationTest")
@Import({ ProductsDataSourceConfig.class, UsersDataSourceConfig.class, LiquibaseConfig.class,
			IngredientService.class, IngredientQuotaService.class })

public class IngredientQuotaServiceTest extends BaseContainerSetup {
	
	@MockBean
	private ComponentQuotaService cqService;
	@Autowired
	private IngredientService ingService;
	@Autowired
	private IngredientQuotaService iqService;
	@Autowired
	private TestEntityManager em;
	
	@Test
	void success_create_iq_without_cqs_and_fetching_existing_ing() {
		Product prodDummy = em.persistFlushFind(new Product(null, "dummy", null, null, null, new HashSet<>()));
		Hibernate.initialize(prodDummy.getIngQuotasList());
		Ingredient ingFake = em.persistFlushFind(new Ingredient("ing1", new HashSet<>()));
		Hibernate.initialize(ingFake.getIngQuotas());
		IngredientQuotaDTO iqDtoFake = new IngredientQuotaDTO("ing1", null, List.of());

		em.flush();
		em.clear();
		
		IngredientQuota iqFake = iqService.createFrom(prodDummy, iqDtoFake, 0);
		List<Ingredient> fetchedIng = ingService.listByName();
		
		assertThat(iqService.findAll()).containsExactly(iqFake);
		assertThat(fetchedIng).containsExactly(ingFake);
		assertThat(prodDummy.getIngQuotasList()).containsExactly(iqFake);
		assertThat(fetchedIng.iterator().next().getIngQuotas()).containsExactly(iqFake);
	}
	
	@Test
	void success_create_iq_without_cqs_and_persisting_new_ing() {
		Product prodDummy = em.persistFlushFind(new Product(null, "dummy", null, null, null, new HashSet<>()));
		Hibernate.initialize(prodDummy.getIngQuotasList());
		IngredientQuotaDTO iqDtoFake = new IngredientQuotaDTO("ing1", null, List.of());

		em.flush();
		em.clear();
		
		IngredientQuota iqFake = iqService.createFrom(prodDummy, iqDtoFake, 0);
		List<Ingredient> fetchedIng = ingService.listByName();
		
		assertThat(iqService.findAll()).containsExactly(iqFake);
		assertThat(fetchedIng).hasSize(1);
		assertThat(prodDummy.getIngQuotasList()).containsExactly(iqFake);
		assertThat(fetchedIng.iterator().next().getIngQuotas()).containsExactly(iqFake);
	}
	
	@Test
	void success_create_iq_with_cqs_and_fetching_existing_ing() {
		Product prodDummy = em.persistFlushFind(new Product(null, "dummy", null, null, null, new HashSet<>()));
		Hibernate.initialize(prodDummy.getIngQuotasList());
		Ingredient ingFake = em.persistFlushFind(new Ingredient("ing1", new HashSet<>()));
		Hibernate.initialize(ingFake.getIngQuotas());
		ComponentQuotaDTO cqDTO1 = new ComponentQuotaDTO("comp1", 80D);
		ComponentQuotaDTO cqDTO2 = new ComponentQuotaDTO("comp2", null);
		IngredientQuotaDTO iqDtoFake = new IngredientQuotaDTO("ing1", null, List.of(cqDTO1, cqDTO2));

		em.flush();
		em.clear();
		
		IngredientQuota iqFake = iqService.createFrom(prodDummy, iqDtoFake, 0);
		List<Ingredient> fetchedIng = ingService.listByName();

		assertThat(iqService.findAll()).containsExactly(iqFake);
		assertThat(fetchedIng).containsExactly(ingFake);
		assertThat(prodDummy.getIngQuotasList()).containsExactly(iqFake);
		assertThat(fetchedIng.iterator().next().getIngQuotas()).containsExactly(iqFake);
		verify(cqService, times(1)).createFrom(iqFake, cqDTO1, 0);
		verify(cqService, times(1)).createFrom(iqFake, cqDTO2, 1);
	}
	
	@Test
	void success_delete_iqlist_and_parent_ingredient_of_one_only_child_iq() {
		Product prodDummy1 = em.persistFlushFind(new Product(null, "dummy1", null, null, null, new HashSet<>()));
		Hibernate.initialize(prodDummy1.getIngQuotasList());
		Product prodDummy2 = em.persistFlushFind(new Product(null, "dummy2", null, null, null, new HashSet<>()));
		Hibernate.initialize(prodDummy2.getIngQuotasList());
		Ingredient ingFake1 = em.persistFlushFind(new Ingredient("ing1", new HashSet<>()));
		Hibernate.initialize(ingFake1.getIngQuotas());
		Ingredient ingFake2 = em.persistFlushFind(new Ingredient("ing2", new HashSet<>()));
		Hibernate.initialize(ingFake2.getIngQuotas());
		IngredientQuota iqFake1 = new IngredientQuota(0, prodDummy1, ingFake1, 70D, Set.of());
		IngredientQuota iqFake2 = new IngredientQuota(0, prodDummy2, ingFake1, 40D, Set.of());
		IngredientQuota iqFake3 = new IngredientQuota(1, prodDummy1, ingFake2, 70D, Set.of());
		prodDummy1.addIngredientQuota(iqFake1);
		prodDummy1.addIngredientQuota(iqFake3);
		prodDummy2.addIngredientQuota(iqFake2);
		ingFake1.addIngredientQuota(iqFake1);
		ingFake1.addIngredientQuota(iqFake2);
		ingFake2.addIngredientQuota(iqFake3);

		em.flush();
		em.clear();
		
		iqService.deleteListFrom(prodDummy1);
		
		assertThat(ingService.listByName()).containsExactly(ingFake1);
		assertThat(prodDummy1.getIngQuotasList().isEmpty());
		assertThat(prodDummy2.getIngQuotasList()).containsExactly(iqFake2);
	}
	
	@Test
	void success_delete_iqlist_and_cqlist_of_one_iq_keeping_parent_ing() {
		Product prodDummy1 = em.persistFlushFind(new Product(null, "dummy1", null, null, null, new HashSet<>()));
		Hibernate.initialize(prodDummy1.getIngQuotasList());
		Product prodDummy2 = em.persistFlushFind(new Product(null, "dummy2", null, null, null, new HashSet<>()));
		Hibernate.initialize(prodDummy1.getIngQuotasList());
		Ingredient ingFake1 = em.persistFlushFind(new Ingredient("ing1", new HashSet<>()));
		Hibernate.initialize(ingFake1.getIngQuotas());
		IngredientQuota iqFake1 = new IngredientQuota(0, prodDummy1, ingFake1, 70D, new HashSet<>());
		IngredientQuota iqFake2 = new IngredientQuota(0, prodDummy2, ingFake1, 70D, new HashSet<>());
		ComponentQuota cqDummy1 = new ComponentQuota(0, iqFake1, null, null);
		ComponentQuota cqDummy2 = new ComponentQuota(1, iqFake1, null, null);
		ComponentQuota cqDummy3 = new ComponentQuota(0, iqFake2, null, null);
		prodDummy1.addIngredientQuota(iqFake1);
		prodDummy2.addIngredientQuota(iqFake2);
		ingFake1.addIngredientQuota(iqFake1);
		ingFake1.addIngredientQuota(iqFake2);
		iqFake1.addComponentQuota(cqDummy1);
		iqFake1.addComponentQuota(cqDummy2);
		iqFake2.addComponentQuota(cqDummy3);

		em.flush();
		em.clear();
		
		iqService.deleteListFrom(prodDummy1);
		
		assertThat(ingService.listByName()).containsExactly(ingFake1);
		assertThat(prodDummy1.getIngQuotasList().isEmpty());
		assertThat(prodDummy2.getIngQuotasList()).hasSize(1);
		verify(cqService, times(1)).deleteListFrom(argThat(iqArg -> iqArg.getId().equals(iqFake1.getId())));
	}
	
}
