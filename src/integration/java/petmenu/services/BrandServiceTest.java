package petmenu.services;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Set;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ActiveProfiles;

import petmenu.BaseContainerSetup;
import petmenu.config.datasources.LiquibaseConfig;
import petmenu.config.datasources.ProductsDataSourceConfig;
import petmenu.config.datasources.UsersDataSourceConfig;
import petmenu.entities.products.Brand;
import petmenu.services.products.BrandService;

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ActiveProfiles("integrationTest")
@Import({ ProductsDataSourceConfig.class, UsersDataSourceConfig.class, LiquibaseConfig.class, BrandService.class })

public class BrandServiceTest extends BaseContainerSetup {
	
	@Autowired
	private BrandService brandService;
	@Autowired
	private TestEntityManager em;
	
	@Test
	void success_create_brand_fetching_existing() {
		Brand brandFake = em.persist(new Brand("Purina", Set.of()));
		
		brandService.createFrom("purina");
		
		em.flush();
		em.clear();
		
		assertThat(brandService.list()).containsExactly(brandFake);
	}
	
	@Test
	void success_create_brand_persisting_new() {
		Brand brandFake = new Brand("Purina", Set.of());
		
		brandService.createFrom("Purina");
		
		em.flush();
		em.clear();
		
		assertThat(brandService.list()).containsExactly(brandFake);
	}

}
