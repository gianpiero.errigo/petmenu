package petmenu.services;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.Hibernate;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ActiveProfiles;

import petmenu.BaseContainerSetup;
import petmenu.config.datasources.LiquibaseConfig;
import petmenu.config.datasources.ProductsDataSourceConfig;
import petmenu.config.datasources.UsersDataSourceConfig;
import petmenu.dtos.products.ComponentQuotaDTO;
import petmenu.entities.products.Component;
import petmenu.entities.products.ComponentQuota;
import petmenu.entities.products.Ingredient;
import petmenu.entities.products.IngredientQuota;
import petmenu.entities.products.Product;
import petmenu.services.products.ComponentQuotaService;
import petmenu.services.products.ComponentService;

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ActiveProfiles("integrationTest")
@Import({ ProductsDataSourceConfig.class, UsersDataSourceConfig.class, LiquibaseConfig.class,
			ComponentService.class, ComponentQuotaService.class })

public class ComponentQuotaServiceTest extends BaseContainerSetup {
	
	@Autowired
	private ComponentService compService;
	@Autowired
	private ComponentQuotaService cqService;
	@Autowired
	private TestEntityManager em;
	
	@Test
	void success_create_cq_fetching_existing_comp() {
		IngredientQuota iqDummy = em.persistFlushFind(new IngredientQuota(0, null, null, null, new HashSet<>()));
		Hibernate.initialize(iqDummy.getCompQuotasList());
		Component compFake = em.persistFlushFind(new Component("comp1", new HashSet<>()));
		Hibernate.initialize(compFake.getCompQuotas());
		ComponentQuotaDTO cqDtoFake = new ComponentQuotaDTO("comp1", null);

		em.flush();
		em.clear();
		
		ComponentQuota cqFake = cqService.createFrom(iqDummy, cqDtoFake, 0);
		List<Component> fetchedComp = compService.listByName();
		
		assertThat(cqService.findAll()).containsExactly(cqFake);
		assertThat(fetchedComp).containsExactly(compFake);
		assertThat(iqDummy.getCompQuotasList()).containsExactly(cqFake);
		assertThat(fetchedComp.iterator().next().getCompQuotas()).containsExactly(cqFake);
	}
	
	@Test
	void success_create_cq_and_persisting_new_comp() {
		IngredientQuota iqDummy = em.persistFlushFind(new IngredientQuota(0, null, null, null, new HashSet<>()));
		Hibernate.initialize(iqDummy.getCompQuotasList());
		ComponentQuotaDTO cqDtoFake = new ComponentQuotaDTO("comp1", null);

		em.flush();
		em.clear();
		
		ComponentQuota cqFake = cqService.createFrom(iqDummy, cqDtoFake, 0);
		List<Component> fetchedComp = compService.listByName();
		
		assertThat(cqService.findAll()).containsExactly(cqFake);
		assertThat(fetchedComp).hasSize(1);
		assertThat(iqDummy.getCompQuotasList()).containsExactly(cqFake);
		assertThat(fetchedComp.iterator().next().getCompQuotas()).containsExactly(cqFake);
	}
	
	@Test
	void success_delete_cqlist_and_parent_component_of_one_only_child_cq() {
		Ingredient ingDummy1 = em.persist(new Ingredient("ingDummy1", Set.of()));
		Ingredient ingDummy2 = em.persist(new Ingredient("ingDummy2", Set.of()));
		IngredientQuota iqDummy1 = em.persistFlushFind(new IngredientQuota(0, null, ingDummy1, null, new HashSet<>()));
		Hibernate.initialize(iqDummy1.getCompQuotasList());
		IngredientQuota iqDummy2 = em.persistFlushFind(new IngredientQuota(1, null, ingDummy2, null, new HashSet<>()));
		Hibernate.initialize(iqDummy2.getCompQuotasList());
		Component compFake1 = em.persistFlushFind(new Component("comp1", new HashSet<>()));
		Hibernate.initialize(compFake1.getCompQuotas());
		Component compFake2 = em.persistFlushFind(new Component("comp2", new HashSet<>()));
		Hibernate.initialize(compFake2.getCompQuotas());
		ComponentQuota cqFake1 = new ComponentQuota(0, iqDummy1, compFake1, 70D);
		ComponentQuota cqFake2 = new ComponentQuota(0, iqDummy2, compFake1, 40D);
		ComponentQuota cqFake3 = new ComponentQuota(1, iqDummy1, compFake2, 70D);
		iqDummy1.addComponentQuota(cqFake1);
		iqDummy1.addComponentQuota(cqFake3);
		iqDummy2.addComponentQuota(cqFake2);
		compFake1.addComponentQuota(cqFake1);
		compFake1.addComponentQuota(cqFake2);
		compFake2.addComponentQuota(cqFake3);

		em.flush();
		em.clear();
		
		cqService.deleteListFrom(iqDummy1);
		
		assertThat(compService.listByName()).containsExactly(compFake1);
		assertThat(iqDummy1.getCompQuotasList().isEmpty());
		assertThat(iqDummy2.getCompQuotasList()).containsExactly(cqFake2);
	}
	
	@Test
	void success_delete_cqlist_keeping_parent_component() {
		Product prodDummy1 = em.persistFlushFind(new Product(null, "dummy1", null, null, null, new HashSet<>()));
		Product prodDummy2 = em.persistFlushFind(new Product(null, "dummy2", null, null, null, new HashSet<>()));
		Ingredient ingDummy1 = em.persist(new Ingredient("ingDummy1", Set.of()));
		IngredientQuota iqDummy1 = em.persistFlushFind(new IngredientQuota(0, prodDummy1, ingDummy1, null, new HashSet<>()));
		Hibernate.initialize(iqDummy1.getCompQuotasList());
		IngredientQuota iqDummy2 = em.persistFlushFind(new IngredientQuota(0, prodDummy2, ingDummy1, null, new HashSet<>()));
		Hibernate.initialize(iqDummy2.getCompQuotasList());
		Component compFake1 = em.persistFlushFind(new Component("comp1", new HashSet<>()));
		Hibernate.initialize(compFake1.getCompQuotas());
		ComponentQuota cqFake1 = new ComponentQuota(0, iqDummy1, compFake1, 70D);
		ComponentQuota cqFake2 = new ComponentQuota(0, iqDummy2, compFake1, 60D);
		iqDummy1.addComponentQuota(cqFake1);
		iqDummy2.addComponentQuota(cqFake2);
		compFake1.addComponentQuota(cqFake1);
		compFake1.addComponentQuota(cqFake2);

		em.flush();
		em.clear();
		
		cqService.deleteListFrom(iqDummy1);
		
		assertThat(compService.listByName()).containsExactly(compFake1);
		assertThat(iqDummy1.getCompQuotasList().isEmpty());
		assertThat(iqDummy2.getCompQuotasList()).hasSize(1);
	}


}