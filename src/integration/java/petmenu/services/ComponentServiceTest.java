package petmenu.services;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Set;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ActiveProfiles;

import petmenu.BaseContainerSetup;
import petmenu.config.datasources.LiquibaseConfig;
import petmenu.config.datasources.ProductsDataSourceConfig;
import petmenu.config.datasources.UsersDataSourceConfig;
import petmenu.entities.products.Component;
import petmenu.services.products.ComponentService;

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ActiveProfiles("integrationTest")
@Import({ ProductsDataSourceConfig.class, UsersDataSourceConfig.class, LiquibaseConfig.class, ComponentService.class })

public class ComponentServiceTest extends BaseContainerSetup {
	
	@Autowired
	private ComponentService compService;
	@Autowired
	private TestEntityManager em;
	
	@Test
	void success_create_component_fetching_existing() {
		Component compFake = em.persist(new Component("Comp1", Set.of()));
		
		em.flush();
		em.clear();
		
		compService.createFrom("comp1");
		
		assertThat(compService.listByName()).containsExactly(compFake);
	}
	
	@Test
	void success_create_component_persisting_new() {
		Component compFake = new Component("comp1", Set.of());
		
		compService.createFrom("comp1");
		
		em.flush();
		em.clear();
		
		assertThat(compService.listByName()).containsExactly(compFake);
	}
}
