package petmenu.services;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.HashSet;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ActiveProfiles;

import petmenu.BaseContainerSetup;
import petmenu.config.datasources.LiquibaseConfig;
import petmenu.config.datasources.ProductsDataSourceConfig;
import petmenu.config.datasources.UsersDataSourceConfig;
import petmenu.entities.products.Brand;
import petmenu.entities.products.Line;
import petmenu.services.products.BrandService;
import petmenu.services.products.LineService;

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ActiveProfiles("integrationTest")
@Import({ ProductsDataSourceConfig.class, UsersDataSourceConfig.class, LiquibaseConfig.class, BrandService.class, LineService.class })

public class LineServiceTest extends BaseContainerSetup {
	
	@Autowired
	private BrandService brandService;
	@Autowired
	private LineService lineService;
	@Autowired
	private TestEntityManager em;
	
	@Test
	void success_create_line_fetching_existing() {
		Brand brandFake = em.persist(new Brand("Purina", new HashSet<Line>()));
		Line lineFake = new Line("fake line", brandFake, null);
		brandFake.addLine(lineFake);
		
		em.flush();
		em.clear();
		
		lineService.createFrom("fake line", "Purina");
		
		assertThat(lineService.findByBrand(brandFake)).containsExactly(lineFake);
	}

	@Test
	void success_create_new_line_fetching_existing_brand() {
		Brand brandFake = em.persist(new Brand("Purina", new HashSet<Line>()));
		Line lineDummy = new Line("dummy line", brandFake, null);
		brandFake.addLine(lineDummy);
				
		em.flush();
		em.clear();
		
		Line lineFake = lineService.createFrom("fake line", "Purina");
		
		assertThat(lineService.findByBrand(brandFake)).containsExactlyInAnyOrder(lineDummy, lineFake);
	}
	
	@Test
	void success_create_new_line_and_brand() {
		Line lineFake = lineService.createFrom("fake line", "Purina");
		
		em.flush();
		em.clear();
		
		assertThat(lineService.findByNameAndBrandName("fake line", "purina")).isEqualTo(lineFake);
		
	}
	
	@Test
	void success_delete_only_child_line_and_brand() {
		Brand brandFake = em.persist(new Brand("Purina", new HashSet<Line>()));
		Line lineFake = new Line("fake line", brandFake, null);
		brandFake.addLine(lineFake);

		em.flush();
		em.clear();
		
		lineService.delete(lineFake);
		
		assertThat(brandService.findByName("Purina")).isNull();
		assertThat(lineService.findByName("fake line")).isEmpty();
	}
	
	@Test
	void success_delete_child_line_keeping_sibling_and_brand() {
		Brand brandFake = em.persist(new Brand("Purina", new HashSet<Line>()));
		Line lineFake = new Line("fake line", brandFake, null);
		Line lineDummy = new Line("dummy line", brandFake, null);
		brandFake.addLine(lineFake);
		brandFake.addLine(lineDummy);
		
		em.flush();
		em.clear();
		
		lineService.delete(lineFake);
		
		assertThat(brandService.findByName("Purina")).isEqualTo(brandFake);
		assertThat(lineService.findByName("fake line")).isEmpty();
		assertThat(lineService.findByBrand(brandFake)).containsExactly(lineDummy);
	}
}
