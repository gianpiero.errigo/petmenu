package petmenu.services;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Set;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ActiveProfiles;

import petmenu.BaseContainerSetup;
import petmenu.config.datasources.LiquibaseConfig;
import petmenu.config.datasources.ProductsDataSourceConfig;
import petmenu.config.datasources.UsersDataSourceConfig;
import petmenu.entities.products.Ingredient;
import petmenu.services.products.IngredientService;

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ActiveProfiles("integrationTest")
@Import({ ProductsDataSourceConfig.class, UsersDataSourceConfig.class, LiquibaseConfig.class, IngredientService.class })

public class IngredientServiceTest extends BaseContainerSetup {
	
	@Autowired
	private IngredientService ingService;
	@Autowired
	private TestEntityManager em;
	
	@Test
	void success_create_ingredient_fetching_existing() {
		Ingredient ingFake = em.persist(new Ingredient("Ing1", Set.of()));
		
		em.flush();
		em.clear();
		
		ingService.createFrom("ing1");
		
		assertThat(ingService.listByName()).containsExactly(ingFake);
	}
	
	@Test
	void success_create_ingredient_persisting_new() {
		Ingredient ingFake = new Ingredient("ing1", Set.of());
		
		ingService.createFrom("ing1");
		
		em.flush();
		em.clear();
		
		assertThat(ingService.listByName()).containsExactly(ingFake);
	}
}
