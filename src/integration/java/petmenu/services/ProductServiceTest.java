package petmenu.services;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.ArgumentMatchers.notNull;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.web.multipart.MultipartFile;

import petmenu.BaseContainerSetup;
import petmenu.config.datasources.LiquibaseConfig;
import petmenu.config.datasources.ProductsDataSourceConfig;
import petmenu.config.datasources.UsersDataSourceConfig;
import petmenu.dtos.products.FirstStageDTO;
import petmenu.dtos.products.IngredientQuotaDTO;
import petmenu.dtos.products.ThirdStageDTO;
import petmenu.entities.products.Brand;
import petmenu.entities.products.Ingredient;
import petmenu.entities.products.IngredientQuota;
import petmenu.entities.products.Line;
import petmenu.entities.products.Product;
import petmenu.entities.products.Product.ProdPackage;
import petmenu.entities.products.Product.ProdType;
import petmenu.exceptions.ProductAlreadyPresentException;
import petmenu.services.products.BrandService;
import petmenu.services.products.IngredientQuotaService;
import petmenu.services.products.LineService;
import petmenu.services.products.ProductService;
import petmenu.services.products.StageMapperService;
import petmenu.services.products.UploadImageService;

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ActiveProfiles("integrationTest")
@Import({ ProductsDataSourceConfig.class, UsersDataSourceConfig.class, LiquibaseConfig.class,
			LineService.class, ProductService.class, StageMapperService.class,
			BrandService.class })

public class ProductServiceTest extends BaseContainerSetup {

	@MockBean
	private UploadImageService uiService;
	@MockBean
	private IngredientQuotaService iqService;
	@Autowired
	private TestEntityManager em;
	@Autowired
	private ProductService prodService;
	@Autowired
	private LineService lineService;
	@Autowired
	private StageMapperService smService;
	
	//CREATE PRODUCT
	
	@Test
	void success_create_new_product_from_FSDTO_persisting_new_line() throws ProductAlreadyPresentException {
		Brand brandDummy = new Brand("brand", null);
		Line lineDummy = new Line("line",  brandDummy, null);
		FirstStageDTO fsDtoFake = new FirstStageDTO("brand", "line", "recipe", "WET", "CAN", 100);
		
		em.flush();
		em.clear();
		
		Product prodFake = prodService.createFrom(fsDtoFake);
		
		assertThat(smService.fsDtoFrom(prodFake)).isEqualTo(fsDtoFake);
		assertThat(lineService.list()).containsExactly(lineDummy);
	}
	
	@Test
	void success_create_new_product_from_FSDTO_fetching_existing_line() throws ProductAlreadyPresentException {
		Brand brandDummy = em.persist(new Brand("brand", null));
		Line lineDummy = em.persist(new Line("line",  brandDummy, null));
		FirstStageDTO fsDtoFake = new FirstStageDTO("brand", "line", "recipe", "WET", "CAN", 100);
		
		em.flush();
		em.clear();

		Product prodFake = prodService.createFrom(fsDtoFake);
		
		assertThat(smService.fsDtoFrom(prodFake)).isEqualTo(fsDtoFake);
		assertThat(lineService.list()).containsExactly(lineDummy);
	}
	
	@Test
	void fail_create_new_product_from_FSDTO_throwing_already_present() throws ProductAlreadyPresentException {
		Brand brandDummy = em.persist(new Brand("brand", null));
		Line lineDummy = em.persist(new Line("line",  brandDummy, null));
		Product prodFake = em.persist(new Product(lineDummy, "recipe", ProdType.WET, ProdPackage.CAN, 100, null));
		FirstStageDTO fsDtoFake = new FirstStageDTO("brand", "line", "recipe", "WET", "CAN", 100);
		
		em.flush();
		em.clear();

		assertThrows(ProductAlreadyPresentException.class, () -> prodService.createFrom(fsDtoFake));
		assertThat(smService.fsDtoFrom(prodFake)).isEqualTo(fsDtoFake);
		assertThat(lineService.list()).containsExactly(lineDummy);
	}
	
	//UPDATE PRODUCT
	
	@Test
	void success_update_product_from_FSDTO_persisting_new_line_removing_old_with_no_more_child() throws ProductAlreadyPresentException {
		Brand brandDummy = em.persist(new Brand("brand", null));
		Line lineDummy = em.persist(new Line("line",  brandDummy, null));
		Product prodOldFake = em.persist(new Product(lineDummy, "recipe", ProdType.WET, ProdPackage.CAN, 100, null));
		FirstStageDTO fsDtoFake = new FirstStageDTO("brand", "new_line", "recipe", "WET", "CAN", 100);
		
		em.flush();
		em.clear();

		Product prodNewFake = prodService.updateFrom(prodOldFake.getId(),fsDtoFake);
		
		assertThat(smService.fsDtoFrom(prodNewFake)).isEqualTo(fsDtoFake);
		assertThat(lineService.list()).containsExactly(new Line("new_line", brandDummy, null));
	}
	
	@Test
	void success_update_product_from_FSDTO_fetching_existing_line_keeping_old_with_other_child() throws ProductAlreadyPresentException {
		Brand brandDummy = em.persist(new Brand("brand", null));
		Line lineDummy = em.persist(new Line("line",  brandDummy, new HashSet<>()));
		Product prodSiblingDummy = new Product(lineDummy, "sibling_recipe", ProdType.WET, ProdPackage.CAN, 100, null);
		lineDummy.addProduct(prodSiblingDummy);
		Product prodOldFake = em.persist(new Product(lineDummy, "recipe", ProdType.WET, ProdPackage.CAN, 100, null));
		FirstStageDTO fsDtoFake = new FirstStageDTO("brand", "new_line", "recipe", "WET", "CAN", 100);
		
		em.flush();
		em.clear();

		Product prodNewFake = prodService.updateFrom(prodOldFake.getId(),fsDtoFake);
		
		assertThat(smService.fsDtoFrom(prodNewFake)).isEqualTo(fsDtoFake);
		assertThat(lineService.list()).containsExactlyInAnyOrder(new Line("new_line", brandDummy, null), lineDummy);
	}
	
	@Test
	void fail_update_product_from_FSDTO_throwing_already_present() {
		Brand brandDummy = em.persist(new Brand("brand", null));
		Line lineDummy = em.persist(new Line("line",  brandDummy, null));
		Product prodFake = em.persist(new Product(lineDummy, "recipe", ProdType.WET, ProdPackage.CAN, 100, null));
		FirstStageDTO fsDtoFake = new FirstStageDTO("brand", "line", "recipe", "WET", "CAN", 100);
		
		em.flush();
		em.clear();

		assertThrows(ProductAlreadyPresentException.class, () -> prodService.updateFrom(prodFake.getId(), fsDtoFake));
		assertThat(smService.fsDtoFrom(prodFake)).isEqualTo(fsDtoFake);
		assertThat(lineService.list()).containsExactly(lineDummy);
	}
	
	//DELETE PRODUCT
	
	@Test
	void success_delete_product_and_images_keeping_line_with_other_child() {
		Brand brandDummy = em.persist(new Brand("brand", null));
		Line lineDummy = em.persist(new Line("line",  brandDummy, new HashSet<>()));
		Product prodSiblingDummy = new Product(lineDummy, "sibling_recipe", ProdType.WET, ProdPackage.CAN, 100, null);
		lineDummy.addProduct(prodSiblingDummy);
		Product prodToRemoveFake = em.persist(new Product(lineDummy, "recipe", ProdType.WET, ProdPackage.CAN, 100, null));
		prodToRemoveFake.set_stage(2);
		prodToRemoveFake.setHasFrontImg(true);
		prodToRemoveFake.setHasBackImg(true);
		
		em.flush();
		em.clear();

		prodService.delete(prodToRemoveFake.getId());
		
		assertThat(lineService.list()).containsExactly(lineDummy);
		assertThat(prodService.findProductById(prodToRemoveFake.getId())).isEqualTo(null);
		verify(uiService, times(1)).deleteImgsFrom(prodToRemoveFake.getId());
	}
	
	@Test
	void success_delete_only_child_product_and_images_and_iqlist_deleting_line() {
		Brand brandDummy = em.persist(new Brand("brand", null));
		Line lineDummy = em.persist(new Line("line",  brandDummy, new HashSet<>()));
		Product prodToRemoveFake = em.persist(new Product(lineDummy, "recipe", ProdType.WET, ProdPackage.CAN, 100, new HashSet<>()));
		Ingredient ingDummy1 = em.persist(new Ingredient("ing1", null));
		Ingredient ingDummy2 = em.persist(new Ingredient("ing2", null));
		IngredientQuota iqDummy1 = new IngredientQuota(0, prodToRemoveFake, ingDummy1, null, null);
		IngredientQuota iqDummy2 = new IngredientQuota(1, prodToRemoveFake, ingDummy2, null, null);
		prodToRemoveFake.set_stage(3);
		prodToRemoveFake.setHasFrontImg(true);
		prodToRemoveFake.setHasBackImg(true);
		prodToRemoveFake.addIngredientQuota(iqDummy1);
		prodToRemoveFake.addIngredientQuota(iqDummy2);
		
		em.flush();
		em.clear();

		prodService.delete(prodToRemoveFake.getId());
		
		assertThat(lineService.list()).isEmpty();
		assertThat(prodService.findProductById(prodToRemoveFake.getId())).isNull();
		verify(uiService, times(1)).deleteImgsFrom(prodToRemoveFake.getId());
		verify(iqService, times(1)).deleteListFrom(argThat(
				arg -> arg.getId().equals(prodToRemoveFake.getId())
			));
	}
	
	// IMAGES
	
	@Test
	void success_set_and_upload_images() throws ProductAlreadyPresentException {
		Brand brandDummy = em.persist(new Brand("brand", null));
		Line lineDummy = em.persist(new Line("line",  brandDummy, new HashSet<>()));
		Product prodFake = em.persist(new Product(lineDummy, "recipe", ProdType.WET, ProdPackage.CAN, 100, new HashSet<>()));
		prodFake.set_stage(1);
		MultipartFile frontImageFake = new MockMultipartFile("front", "front".getBytes());
		MultipartFile backImageFake = new MockMultipartFile("back", "back".getBytes());
		
		when(uiService.uploadImgFor(any(), notNull())).thenReturn(true);
		
		em.flush();
		em.clear();
		
		prodService.setSecondStage(prodFake.getId(), frontImageFake, backImageFake, null, null);
		Product fetchedProd = prodService.findProductById(prodFake.getId());

		assertThat(fetchedProd.getHasFrontImg()).isTrue();
		assertThat(fetchedProd.getHasBackImg()).isTrue();
		assertThat(fetchedProd.getHasDetailsOneImg()).isFalse();
		assertThat(fetchedProd.getHasDetailsTwoImg()).isFalse();
		assertThat(fetchedProd.get_stage()).isEqualTo(2);
		verify(uiService, times(1)).uploadImgFor(fetchedProd.getId(), frontImageFake);
		verify(uiService, times(1)).uploadImgFor(fetchedProd.getId(), backImageFake);
	}
	
	@Test
	void fail_set_and_not_upload_images_throwing_already_present() throws ProductAlreadyPresentException {
		Brand brandDummy = em.persist(new Brand("brand", null));
		Line lineDummy = em.persist(new Line("line",  brandDummy, new HashSet<>()));
		Product prodFake = em.persist(new Product(lineDummy, "recipe", ProdType.WET, ProdPackage.CAN, 100, new HashSet<>()));
		prodFake.set_stage(2);
		MultipartFile frontImageFake = new MockMultipartFile("front", "front".getBytes());
		MultipartFile backImageFake = new MockMultipartFile("back", "back".getBytes());
				
		em.flush();
		em.clear();
		
		assertThrows(ProductAlreadyPresentException.class, 
				() -> prodService.setSecondStage(prodFake.getId(), frontImageFake, backImageFake, null, null)
			);
	}
	
	@Test
	void success_update_and_upload_images() throws ProductAlreadyPresentException {
		Brand brandDummy = em.persist(new Brand("brand", null));
		Line lineDummy = em.persist(new Line("line",  brandDummy, new HashSet<>()));
		Product prodFake = em.persist(new Product(lineDummy, "recipe", ProdType.WET, ProdPackage.CAN, 100, new HashSet<>()));
		prodFake.set_stage(3);
		MultipartFile frontImageFake = new MockMultipartFile("front", "front".getBytes());
		MultipartFile backImageFake = new MockMultipartFile("back", "back".getBytes());
		
		when(uiService.changeImgFor(any(), any(), notNull())).thenReturn(true);
		
		em.flush();
		em.clear();
		
		prodService.updateSecondStage(prodFake.getId(), frontImageFake, backImageFake, null, null);
		Product fetchedProd = prodService.findProductById(prodFake.getId());

		assertThat(fetchedProd.getHasFrontImg()).isTrue();
		assertThat(fetchedProd.getHasBackImg()).isTrue();
		assertThat(fetchedProd.getHasDetailsOneImg()).isFalse();
		assertThat(fetchedProd.getHasDetailsTwoImg()).isFalse();
		assertThat(fetchedProd.get_stage()).isEqualTo(3);
		verify(uiService, times(1)).changeImgFor("frontImg", fetchedProd.getId(), frontImageFake);
		verify(uiService, times(1)).changeImgFor("backImg", fetchedProd.getId(), backImageFake);
	}
	
	//THIRD STAGE
	
	@Test
	void success_set_new_third_stage() throws ProductAlreadyPresentException {
		Brand brandDummy = em.persist(new Brand("brand", null));
		Line lineDummy = em.persist(new Line("line",  brandDummy, new HashSet<>()));
		Product prodFake = em.persist(new Product(lineDummy, "recipe", ProdType.WET, ProdPackage.CAN, 100, new HashSet<>()));
		prodFake.set_stage(2);
		IngredientQuotaDTO iqDtoDummy1 = new IngredientQuotaDTO("ing1", 60D, null);
		IngredientQuotaDTO iqDtoDummy2 = new IngredientQuotaDTO("ing2", 40D, null);
		Map<String, Double> analysisDummy = Map.of(
				"rawProtein", 1D, "rawFibre", 2D, "rawOilFat", 3D, "rawAshes", 4D, "moisture", 5D, "energy", 100D
			);
		ThirdStageDTO tsDtoFake = new ThirdStageDTO(List.of(iqDtoDummy1, iqDtoDummy2), analysisDummy);
		
		em.flush();
		em.clear();
		
		prodService.setThirdStage(prodFake.getId(), tsDtoFake);
		Product fetchedProd = prodService.findProductById(prodFake.getId());
		
		assertThat(fetchedProd.getRawProtein()).isEqualTo(1D);
		assertThat(fetchedProd.getRawFibre()).isEqualTo(2D);
		assertThat(fetchedProd.getRawOilFat()).isEqualTo(3D);
		assertThat(fetchedProd.getRawAshes()).isEqualTo(4D);
		assertThat(fetchedProd.getMoisture()).isEqualTo(5D);
		assertThat(fetchedProd.getEnergy()).isEqualTo(100D);
		assertThat(fetchedProd.get_stage()).isEqualTo(3);
		verify(iqService, times(1)).createFrom(fetchedProd, iqDtoDummy1, 0);
		verify(iqService, times(1)).createFrom(fetchedProd, iqDtoDummy2, 1);
	}
	
	@Test
	void fail_set_new_third_stage_throwing_already_present() {
		Brand brandDummy = em.persist(new Brand("brand", null));
		Line lineDummy = em.persist(new Line("line",  brandDummy, new HashSet<>()));
		Product prodFake = em.persist(new Product(lineDummy, "recipe", ProdType.WET, ProdPackage.CAN, 100, new HashSet<>()));
		prodFake.set_stage(3);
		
		em.flush();
		em.clear();
		
		assertThrows(ProductAlreadyPresentException.class, 
				() -> prodService.setThirdStage(prodFake.getId(), null)
			);
	}
	
	@Test
	void success_update_third_stage_deleting_previous_iqlist() {
		Brand brandDummy = em.persist(new Brand("brand", null));
		Line lineDummy = em.persist(new Line("line",  brandDummy, new HashSet<>()));
		Product prodFake = em.persist(new Product(lineDummy, "recipe", ProdType.WET, ProdPackage.CAN, 100, new HashSet<>()));
		prodFake.set_stage(2);
		IngredientQuotaDTO iqDtoDummy1 = new IngredientQuotaDTO("ing1", 60D, null);
		IngredientQuotaDTO iqDtoDummy2 = new IngredientQuotaDTO("ing2", 40D, null);
		Map<String, Double> analysisDummy = Map.of(
				"rawProtein", 1D, "rawFibre", 2D, "rawOilFat", 3D, "rawAshes", 4D, "moisture", 5D, "energy", 100D
			);
		ThirdStageDTO tsDtoFake = new ThirdStageDTO(List.of(iqDtoDummy1, iqDtoDummy2), analysisDummy);
		
		em.flush();
		em.clear();
		
		prodService.updateThirdStage(prodFake.getId(), tsDtoFake);
		Product fetchedProd = prodService.findProductById(prodFake.getId());
		
		assertThat(fetchedProd.getRawProtein()).isEqualTo(1D);
		assertThat(fetchedProd.getRawFibre()).isEqualTo(2D);
		assertThat(fetchedProd.getRawOilFat()).isEqualTo(3D);
		assertThat(fetchedProd.getRawAshes()).isEqualTo(4D);
		assertThat(fetchedProd.getMoisture()).isEqualTo(5D);
		assertThat(fetchedProd.getEnergy()).isEqualTo(100D);
		assertThat(fetchedProd.get_stage()).isEqualTo(3);
		verify(iqService, times(1)).createFrom(fetchedProd, iqDtoDummy1, 0);
		verify(iqService, times(1)).createFrom(fetchedProd, iqDtoDummy2, 1);
		verify(iqService, times(1)).deleteListFrom(prodFake);
	}
}
