package petmenu;

import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.testcontainers.containers.MariaDBContainer;

public abstract class BaseContainerSetup {
	
	private static final MariaDBContainer container = new MariaDBContainer<>("mariadb:10.7.3")
	.withDatabaseName("products_db_test")
	.withReuse(true)
	.withUsername("root")
	.withPassword("")
	.withInitScript("repositories/create_DBs.sql");

	@DynamicPropertySource
	static void mariaDBProperties(DynamicPropertyRegistry registry) {

		registry.add("petmenu.datasource.products.url", 
				() -> "jdbc:mariadb://" + container.getHost() + ":" + container.getMappedPort(3306) + "/products_db_test");

		registry.add("petmenu.datasource.users.url", 
				() -> "jdbc:mariadb://" + container.getHost() + ":" + container.getMappedPort(3306) + "/users_db_test");
		
		container.start();

	}

}
