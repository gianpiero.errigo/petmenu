package petmenu.RESTcontrollers;

import static org.mockito.Mockito.when;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.hamcrest.Matchers. hasSize;
import static org.hamcrest.CoreMatchers. equalTo;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.jwt;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.autoconfigure.context.PropertyPlaceholderAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import petmenu.RESTControllers.products.ProductController;
import petmenu.config.SecurityConfig;
import petmenu.dtos.products.FirstStageDTO;
import petmenu.dtos.products.ProductDTO;
import petmenu.dtos.products.SecondStageDTO;
import petmenu.dtos.products.ThirdStageDTO;
import petmenu.entities.products.Product;
import petmenu.entities.users.User;
import petmenu.exceptions.ProductAlreadyPresentException;
import petmenu.services.products.ProductService;
import petmenu.services.products.StageMapperService;
import petmenu.services.users.UserCommitService;
import petmenu.services.users.UserService;

@WebMvcTest(ProductController.class)
@ImportAutoConfiguration(PropertyPlaceholderAutoConfiguration.class)
@Import({SecurityConfig.class})
@ActiveProfiles("integrationTest")
@TestInstance(TestInstance.Lifecycle.PER_CLASS)

public class ProductControllerTest {
	
	@MockBean
	private ProductService prodService;
	@MockBean
	private StageMapperService smService;
	@MockBean
	private UserCommitService ucService;
	@MockBean(name = "userService")
	private UserService userService;
	@Autowired
	private MockMvc mockMvc;
	@Autowired
	private ObjectMapper obMapper;
	
	@Value("classpath:RESTControllers/payloads.json")
	Resource payloadsResource;
	
	JsonNode payloads;
	
	@BeforeAll
	void setUpResource() throws IOException {
		File jsonFile = payloadsResource.getFile();
		payloads = obMapper.readTree(jsonFile);
	}
	
	// FIRST STAGE GET
	
	@Test
	void success_get_firstStage_for_prodId() throws Exception {
		Product prodDummy = new Product();
		FirstStageDTO fsDTOFake = obMapper.treeToValue(payloads.get("good_FS"), FirstStageDTO.class);
		
		when(prodService.findProductById(eq(99))).thenReturn(prodDummy);
		when(smService.fsDtoFrom(eq(prodDummy))).thenReturn(fsDTOFake);
		
		mockMvc.perform(get("/product/{id}/firststage", 99)
			.contentType(MediaType.APPLICATION_JSON))
		.andExpect(jsonPath("$", equalTo(fsDTOFake), FirstStageDTO.class))
		.andExpect(status().isOk());
	}
	
	// FIRST STAGE POST

	@Test
	void success_post_new_firststage_and_return_id_and_stage() throws Exception {
		Jwt test_jwt = new Jwt("test", null, null, Map.of("test_header", new Object()), Map.of("scope", "input:product"));
		User userDummy = new User();
		JsonNode fsNodeFake = payloads.get("good_FS");
		Product productFake = new Product();
		productFake.setId(99); productFake.set_stage(1);
		
		when(userService.createFrom(any())).thenReturn(userDummy);		
		when(prodService.createFrom(any())).thenReturn(productFake);
		
		mockMvc.perform(post("/product")
			.content(fsNodeFake.toString())
			.contentType(MediaType.APPLICATION_JSON)
			.with(jwt().jwt(test_jwt)))
		.andExpect(jsonPath("$.id").value(99))
		.andExpect(jsonPath("$._stage").value(1))
		.andExpect(status().isCreated());
		
		verify(ucService, times(1)).createFrom(eq(userDummy), eq(99), eq("NEW"), eq(1));
	}
	
	@Test
	void fail_post_new_firststage_invalid_payload_blank_property() throws Exception {
		Jwt test_jwt = new Jwt("test", null, null, Map.of("test_header", new Object()), Map.of("scope", "input:product"));
		JsonNode fsNodeFake = payloads.get("bad_FS_blank_prop");
		
		mockMvc.perform(post("/product")
			.content(fsNodeFake.toString())
			.contentType(MediaType.APPLICATION_JSON)
			.with(jwt().jwt(test_jwt)))
		.andExpect(status().isBadRequest());
		
		verify(prodService, never()).createFrom(any());
		verify(ucService, never()).createFrom(any(), any(), any(), any());
	}
	
	@Test
	void fail_post_new_firststage_invalid_payload_negative_netWeight() throws Exception {
		Jwt test_jwt = new Jwt("test", null, null, Map.of("test_header", new Object()), Map.of("scope", "input:product"));
		FirstStageDTO fsDTOFake = obMapper.treeToValue(payloads.get("bad_FS_negative_netWeight"), FirstStageDTO.class);
		
		mockMvc.perform(post("/product")
			.content(obMapper.writeValueAsString(fsDTOFake))
			.contentType(MediaType.APPLICATION_JSON)
			.with(jwt().jwt(test_jwt)))
		.andExpect(status().isBadRequest());

		verify(prodService, never()).createFrom(any());
		verify(ucService, never()).createFrom(any(), any(), any(), any());
	}
	
	@Test
	void fail_post_new_firststage_insufficient_auth() throws Exception {
		Jwt test_jwt = new Jwt("test", null, null, Map.of("test_header", new Object()), Map.of("scope", ""));
		JsonNode fsNodeFake = payloads.get("good_FS");
		
		mockMvc.perform(post("/product")
			.content(fsNodeFake.toString())
			.contentType(MediaType.APPLICATION_JSON)
			.with(jwt().jwt(test_jwt)))
		.andExpect(status().isForbidden());

		verify(prodService, never()).createFrom(any());
		verify(ucService, never()).createFrom(any(), any(), any(), any());
	}
	
	@Test
	void fail_post_new_firststage_conflicting_product() throws Exception {
		Jwt test_jwt = new Jwt("test", null, null, Map.of("test_header", new Object()), Map.of("scope", "input:product"));
		JsonNode fsNodeFake = payloads.get("good_FS");
		
		when(prodService.createFrom(any()))
		.thenThrow(ProductAlreadyPresentException.class);
		when(ucService.createFrom(any(), eq(99), eq("NEW"), eq(1))).thenReturn(null);
		
		mockMvc.perform(post("/product")
			.content(fsNodeFake.toString())
			.contentType(MediaType.APPLICATION_JSON)
			.with(jwt().jwt(test_jwt)))
		.andExpect(status().isConflict());

		verify(ucService, never()).createFrom(any(), any(), any(), any());
	}
	
	// FIRST STAGE PUT
	
	@Test
	void success_put_edit_firststage() throws Exception {
		Jwt test_jwt = new Jwt("test", null, null, Map.of("test_header", new Object()), Map.of("scope", "input:product"));
		User userDummy = new User();
		FirstStageDTO fsDTOFake = obMapper.treeToValue(payloads.get("good_FS"), FirstStageDTO.class);
		Product productDummy = new Product();
		
		when(userService.createFrom(any())).thenReturn(userDummy);
		when(prodService.updateFrom(eq(99), eq(fsDTOFake))).thenReturn(productDummy);
		
		mockMvc.perform(put("/product/99/firststage")
			.content(obMapper.writeValueAsString(fsDTOFake))
			.contentType(MediaType.APPLICATION_JSON)
			.with(jwt().jwt(test_jwt)))
		.andExpect(jsonPath("$.msg").value("Product updated"))
		.andExpect(status().isOk());
			
		verify(ucService, times(1)).createFrom(eq(userDummy), eq(99), eq("EDIT"), eq(1));
	}
	
	@Test
	void fail_put_edit_firststage_invalid_payload_blank_property() throws Exception {
		Jwt test_jwt = new Jwt("test", null, null, Map.of("test_header", new Object()), Map.of("scope", "input:product"));
		FirstStageDTO fsDTOFake = obMapper.treeToValue(payloads.get("bad_FS_blank_prop"), FirstStageDTO.class);
		
		mockMvc.perform(put("/product/99/firststage")
			.content(obMapper.writeValueAsString(fsDTOFake))
			.contentType(MediaType.APPLICATION_JSON)
			.with(jwt().jwt(test_jwt)))
		.andExpect(status().isBadRequest());
	
		verify(prodService, never()).updateFrom(any(), any());
		verify(ucService, never()).createFrom(any(), any(), any(), any());
	}
	
	@Test
	void fail_put_edit_firststage_invalid_payload_negative_netWeight() throws Exception {
		Jwt test_jwt = new Jwt("test", null, null, Map.of("test_header", new Object()), Map.of("scope", "input:product"));
		FirstStageDTO fsDTOFake = obMapper.treeToValue(payloads.get("bad_FS_negative_netWeight"), FirstStageDTO.class);
		
		mockMvc.perform(put("/product/99/firststage")
			.content(obMapper.writeValueAsString(fsDTOFake))
			.contentType(MediaType.APPLICATION_JSON)
			.with(jwt().jwt(test_jwt)))
		.andExpect(status().isBadRequest());
		
		verify(prodService, never()).updateFrom(any(), any());
		verify(ucService, never()).createFrom(any(), any(), any(), any());
		
	}
	
	@Test
	void fail_put_edit_firststage_insufficient_auth() throws Exception {
		Jwt test_jwt = new Jwt("test", null, null, Map.of("test_header", new Object()), Map.of("scope", ""));
		FirstStageDTO fsDTOFake = obMapper.treeToValue(payloads.get("good_FS"), FirstStageDTO.class);
		
		mockMvc.perform(put("/product/99/firststage")
			.content(obMapper.writeValueAsString(fsDTOFake))
			.contentType(MediaType.APPLICATION_JSON)
			.with(jwt().jwt(test_jwt)))
		.andExpect(status().isForbidden());
		
		verify(prodService, never()).updateFrom(any(), any());
		verify(ucService, never()).createFrom(any(), any(), any(), any());
		
	}
	
	@Test
	void fail_put_edit_firststage_conflicting_product() throws Exception {
		Jwt test_jwt = new Jwt("test", null, null, Map.of("test_header", new Object()), Map.of("scope", "input:product"));
		FirstStageDTO fsDTOFake = obMapper.treeToValue(payloads.get("good_FS"), FirstStageDTO.class);

		when(prodService.updateFrom(eq(99), eq(fsDTOFake))).thenThrow(ProductAlreadyPresentException.class);
		
		mockMvc.perform(put("/product/99/firststage")
			.content(obMapper.writeValueAsString(fsDTOFake))
			.contentType(MediaType.APPLICATION_JSON)
			.with(jwt().jwt(test_jwt)))
		.andExpect(status().isConflict());
		
		verify(ucService, never()).createFrom(any(), any(), any(), any());
		
	}
	
	
	// SECOND STAGE GET
	
	@Test
	void success_get_secondStage_for_prodId() throws Exception {
		Product prodDummy = new Product();
		SecondStageDTO ssDTOFake = obMapper.treeToValue(payloads.get("good_SS"), SecondStageDTO.class);
		
		when(prodService.findProductById(eq(99))).thenReturn(prodDummy);
		when(smService.ssDtoFrom(eq(prodDummy))).thenReturn(ssDTOFake);
		
		mockMvc.perform(get("/product/{id}/secondstage", 99)
			.contentType(MediaType.APPLICATION_JSON))
		.andExpect(jsonPath("$", equalTo(ssDTOFake), SecondStageDTO.class))
		.andExpect(status().isOk());
		
	}
	
	// SECOND STAGE POST
	
	@Test
	void success_post_new_secondstage_and_return_id_and_stage() throws Exception {
		Jwt test_jwt = new Jwt("test", null, null, Map.of("test_header", new Object()), Map.of("scope", "input:product"));
		User userDummy = new User();
		MockMultipartFile frontImgFake = new MockMultipartFile("frontImg", "test", MediaType.MULTIPART_FORM_DATA_VALUE, "test".getBytes());	
		MockMultipartFile backImgFake = new MockMultipartFile("backImg", "test", MediaType.MULTIPART_FORM_DATA_VALUE, "test".getBytes());	
		MockMultipartFile detailsOneImgFake = new MockMultipartFile("detailsOneImg", "test", MediaType.MULTIPART_FORM_DATA_VALUE, "test".getBytes());	

		when(userService.createFrom(any())).thenReturn(userDummy);
		mockMvc.perform(multipart("/product/{id}/secondstage", 99)
			.file(frontImgFake).file(backImgFake).file(detailsOneImgFake)
			.contentType(MediaType.MULTIPART_FORM_DATA)
			.with(jwt().jwt(test_jwt)))
		.andExpect(jsonPath("$.id").value(99))
		.andExpect(jsonPath("$._stage").value(2))
		.andExpect(status().isCreated());
			
		verify(prodService, times(1)).setSecondStage(99, frontImgFake, backImgFake, detailsOneImgFake, null);
		verify(ucService, times(1)).createFrom(eq(userDummy), eq(99), eq("NEW"), eq(2));
	
	}
	
	@Test
	void fail_post_new_secondstage_insufficient_auth() throws Exception {
		Jwt test_jwt = new Jwt("test", null, null, Map.of("test_header", new Object()), Map.of("scope", ""));
		MockMultipartFile frontImgFake = new MockMultipartFile("frontImg", "test", MediaType.MULTIPART_FORM_DATA_VALUE, "test".getBytes());	
		MockMultipartFile backImgFake = new MockMultipartFile("backImg", "test", MediaType.MULTIPART_FORM_DATA_VALUE, "test".getBytes());	
		MockMultipartFile detailsOneImgFake = new MockMultipartFile("detailsOneImg", "test", MediaType.MULTIPART_FORM_DATA_VALUE, "test".getBytes());	

		mockMvc.perform(multipart("/product/{id}/secondstage", 99)
			.file(frontImgFake).file(backImgFake).file(detailsOneImgFake)
			.contentType(MediaType.MULTIPART_FORM_DATA)
			.with(jwt().jwt(test_jwt)))
		.andExpect(status().isForbidden());
		
		verify(prodService, never()).setSecondStage(any(), any(), any(), any(), any());
		verify(ucService, never()).createFrom(any(), any(), any(), any());
			
	}
	
	@Test
	void fail_post_new_secondstage_conflicting_product() throws Exception {
		Jwt test_jwt = new Jwt("test", null, null, Map.of("test_header", new Object()), Map.of("scope", "input:product"));
		MockMultipartFile frontImgFake = new MockMultipartFile("frontImg", "test", MediaType.MULTIPART_FORM_DATA_VALUE, "test".getBytes());	
		MockMultipartFile backImgFake = new MockMultipartFile("backImg", "test", MediaType.MULTIPART_FORM_DATA_VALUE, "test".getBytes());	
		MockMultipartFile detailsOneImgFake = new MockMultipartFile("detailsOneImg", "test", MediaType.MULTIPART_FORM_DATA_VALUE, "test".getBytes());	

		doThrow(ProductAlreadyPresentException.class)
		.when(prodService).setSecondStage(99, frontImgFake, backImgFake, detailsOneImgFake, null);
		
		mockMvc.perform(multipart("/product/{id}/secondstage", 99)
			.file(frontImgFake).file(backImgFake).file(detailsOneImgFake)
			.contentType(MediaType.MULTIPART_FORM_DATA)
			.with(jwt().jwt(test_jwt)))
		.andExpect(status().isConflict());
		
		verify(ucService, never()).createFrom(any(), any(), any(), any());
			
	}
	
	// SECOND STAGE PUT
	
	@Test
	void success_put_edit_secondstage() throws Exception {
		Jwt test_jwt = new Jwt("test", null, null, Map.of("test_header", new Object()), Map.of("scope", "input:product"));
		User userDummy = new User();
		MockMultipartFile frontImgFake = new MockMultipartFile("frontImg", "test", MediaType.MULTIPART_FORM_DATA_VALUE, "test".getBytes());	
		MockMultipartFile backImgFake = new MockMultipartFile("backImg", "test", MediaType.MULTIPART_FORM_DATA_VALUE, "test".getBytes());	
		MockMultipartFile detailsOneImgFake = new MockMultipartFile("detailsOneImg", "test", MediaType.MULTIPART_FORM_DATA_VALUE, "test".getBytes());	

		when(userService.createFrom(any())).thenReturn(userDummy);
		mockMvc.perform(multipart("/product/{id}/secondstage", 99)
			.file(frontImgFake).file(backImgFake).file(detailsOneImgFake)
			.contentType(MediaType.MULTIPART_FORM_DATA)
			.with(jwt().jwt(test_jwt))
			.with(req -> { req.setMethod("PUT"); return req; }))
		.andExpect(jsonPath("$.msg").value("Stage 2 updated"))
		.andExpect(status().isOk());
			
		verify(prodService, times(1)).updateSecondStage(99, frontImgFake, backImgFake, detailsOneImgFake, null);
		verify(ucService, times(1)).createFrom(eq(userDummy), eq(99), eq("EDIT"), eq(2));

	}
	
	@Test
	void fail_put_edit_secondstage_insufficient_auth() throws Exception {
		Jwt test_jwt = new Jwt("test", null, null, Map.of("test_header", new Object()), Map.of("scope", ""));
		MockMultipartFile frontImgFake = new MockMultipartFile("frontImg", "test", MediaType.MULTIPART_FORM_DATA_VALUE, "test".getBytes());	
		MockMultipartFile backImgFake = new MockMultipartFile("backImg", "test", MediaType.MULTIPART_FORM_DATA_VALUE, "test".getBytes());	
		MockMultipartFile detailsOneImgFake = new MockMultipartFile("detailsOneImg", "test", MediaType.MULTIPART_FORM_DATA_VALUE, "test".getBytes());	

		mockMvc.perform(multipart("/product/{id}/secondstage", 99)
			.file(frontImgFake).file(backImgFake).file(detailsOneImgFake)
			.contentType(MediaType.MULTIPART_FORM_DATA)
			.with(jwt().jwt(test_jwt)))
		.andExpect(status().isForbidden());
		
		verify(prodService, never()).setSecondStage(any(), any(), any(), any(), any());
		verify(ucService, never()).createFrom(any(), any(), any(), any());
			
	}
	
	
	// THIRD STAGE GET
	
	@Test
	void success_get_thirdStage_for_prodId() throws Exception {
		Product prodDummy = new Product();
		ThirdStageDTO tsDTOFake = obMapper.treeToValue(payloads.get("good_TS"), ThirdStageDTO.class);
		
		when(prodService.findProductByIdFullFetch(eq(99))).thenReturn(prodDummy);
		when(smService.tsDtoFrom(eq(prodDummy))).thenReturn(tsDTOFake);
		
		mockMvc.perform(get("/product/{id}/thirdstage", 99)
			.contentType(MediaType.APPLICATION_JSON))
		.andExpect(jsonPath("$", equalTo(tsDTOFake), ThirdStageDTO.class))
		.andExpect(status().isOk());

	}
	
	
	// THIRD STAGE POST
	
	@Test
	void success_post_new_thirdstage_and_return_id_and_stage() throws Exception {
		Jwt test_jwt = new Jwt("test", null, null, Map.of("test_header", new Object()), Map.of("scope", "input:product"));
		User userDummy = new User();
		JsonNode tsDTOFakeNode = payloads.get("good_TS");	

		when(userService.createFrom(any())).thenReturn(userDummy);
		
		mockMvc.perform(post("/product/{id}/thirdstage", 99)
			.content((tsDTOFakeNode).toString())
			.contentType(MediaType.APPLICATION_JSON)
			.with(jwt().jwt(test_jwt)))
		.andExpect(jsonPath("$.id").value(99))
		.andExpect(jsonPath("$._stage").value(3))
		.andExpect(status().isCreated());
			
		verify(prodService, times(1)).setThirdStage(eq(99), eq(obMapper.treeToValue(tsDTOFakeNode, ThirdStageDTO.class)));
		verify(ucService, times(1)).createFrom(eq(userDummy), eq(99), eq("NEW"), eq(3));

	}
	
	@Test
	void fail_post_new_thirdstage_invalid_payload_max_ingPerc() throws Exception {
		Jwt test_jwt = new Jwt("test", null, null, Map.of("test_header", new Object()), Map.of("scope", "input:product"));
		JsonNode tsDTOFakeNode = payloads.get("bad_TS_max_ingPerc");	

		mockMvc.perform(post("/product/{id}/thirdstage", 99)
			.content(tsDTOFakeNode.toString())
			.contentType(MediaType.APPLICATION_JSON)
			.with(jwt().jwt(test_jwt)))
		.andExpect(status().isBadRequest());
		
		verify(prodService, never()).updateThirdStage(any(), any());
		verify(ucService, never()).createFrom(any(), any(), any(), any());
			
	}
	
	@Test
	void fail_post_new_thirdstage_invalid_payload_blank_comp() throws Exception {
		Jwt test_jwt = new Jwt("test", null, null, Map.of("test_header", new Object()), Map.of("scope", "input:product"));
		JsonNode tsDTOFakeNode = payloads.get("bad_TS_blank_comp");	

		mockMvc.perform(post("/product/{id}/thirdstage", 99)
			.content(tsDTOFakeNode.toString())
			.contentType(MediaType.APPLICATION_JSON)
			.with(jwt().jwt(test_jwt)))
		.andExpect(status().isBadRequest());
		
		verify(prodService, never()).updateThirdStage(any(), any());
		verify(ucService, never()).createFrom(any(), any(), any(), any());
			
	}
	
	@Test
	void fail_post_new_thirdstage_insufficient_auth() throws Exception {
		Jwt test_jwt = new Jwt("test", null, null, Map.of("test_header", new Object()), Map.of("scope", ""));
		ThirdStageDTO tSDummy = new ThirdStageDTO();	

		mockMvc.perform(post("/product/{id}/thirdstage", 99)
			.content(tSDummy.toString())
			.contentType(MediaType.APPLICATION_JSON)
			.with(jwt().jwt(test_jwt)))
		.andExpect(status().isForbidden());
		
		verify(prodService, never()).setThirdStage(any(), any());
		verify(ucService, never()).createFrom(any(), any(), any(), any());
			
	}
	
	@Test
	void fail_post_new_thirdstage_conflicting_product() throws Exception {
		Jwt test_jwt = new Jwt("test", null, null, Map.of("test_header", new Object()), Map.of("scope", "input:product"));
		ThirdStageDTO tSDummy = new ThirdStageDTO();	

		doThrow(ProductAlreadyPresentException.class)
		.when(prodService).setThirdStage(eq(99), eq(tSDummy));
		
		mockMvc.perform(post("/product/{id}/thirdstage", 99)
			.content("{}")
			.contentType(MediaType.APPLICATION_JSON)
			.with(jwt().jwt(test_jwt)))
		.andExpect(status().isConflict());
		
		verify(ucService, never()).createFrom(any(), any(), any(), any());
			
	}
	
	
	// THIRD STAGE PUT
	
	@Test
	void success_put_edit_thirdstage() throws Exception {
		Jwt test_jwt = new Jwt("test", null, null, Map.of("test_header", new Object()), Map.of("scope", "input:product"));
		User userDummy = new User();
		ThirdStageDTO tSDummy = new ThirdStageDTO();	

		when(userService.createFrom(any())).thenReturn(userDummy);
		
		mockMvc.perform(put("/product/{id}/thirdstage", 99)
			.content("{}")
			.contentType(MediaType.APPLICATION_JSON)
			.with(jwt().jwt(test_jwt)))
		.andExpect(jsonPath("$.msg").value("Stage 3 updated"))
		.andExpect(status().isOk());
			
		verify(prodService, times(1)).updateThirdStage(eq(99), eq(tSDummy));
		verify(ucService, times(1)).createFrom(eq(userDummy), eq(99), eq("EDIT"), eq(3));
	
	}
	
	@Test
	void fail_put_edit_thirdstage_invalid_payload_blank_ing() throws Exception {
		Jwt test_jwt = new Jwt("test", null, null, Map.of("test_header", new Object()), Map.of("scope", "input:product"));
		JsonNode tsDTOFakeNode = payloads.get("bad_TS_blank_ing");	

		mockMvc.perform(put("/product/{id}/thirdstage", 99)
			.content(tsDTOFakeNode.toString())
			.contentType(MediaType.APPLICATION_JSON)
			.with(jwt().jwt(test_jwt)))
		.andExpect(status().isBadRequest());
		
		verify(prodService, never()).updateThirdStage(any(), any());
		verify(ucService, never()).createFrom(any(), any(), any(), any());
			
	}
	
	@Test
	void fail_put_edit_thirdstage_invalid_payload_max_compPerc() throws Exception {
		Jwt test_jwt = new Jwt("test", null, null, Map.of("test_header", new Object()), Map.of("scope", "input:product"));
		JsonNode tsDTOFakeNode = payloads.get("bad_TS_max_compPerc");
		
		mockMvc.perform(put("/product/{id}/thirdstage", 99)
			.content(tsDTOFakeNode.toString())
			.contentType(MediaType.APPLICATION_JSON)
			.with(jwt().jwt(test_jwt)))
		.andExpect(status().isBadRequest());
		
		verify(prodService, never()).updateThirdStage(any(), any());
		verify(ucService, never()).createFrom(any(), any(), any(), any());
			
	}
	
	@Test
	void fail_put_edit_thirdstage_insufficient_auth() throws Exception {
		Jwt test_jwt = new Jwt("test", null, null, Map.of("test_header", new Object()), Map.of("scope", ""));
		ThirdStageDTO tSDummy = new ThirdStageDTO();	

		mockMvc.perform(put("/product/{id}/thirdstage", 99)
			.content(tSDummy.toString())
			.contentType(MediaType.APPLICATION_JSON)
			.with(jwt().jwt(test_jwt)))
		.andExpect(status().isForbidden());
		
		verify(prodService, never()).updateThirdStage(any(), any());
		verify(ucService, never()).createFrom(any(), any(), any(), any());
			
	}
	
	
	// SINGLE PRODUCT
	
	@Test
	void success_get_product_for_prodId() throws Exception {
		Product prodDummy = new Product();
		FirstStageDTO fsDTOFake = obMapper.treeToValue(payloads.get("good_FS"), FirstStageDTO.class);
		SecondStageDTO ssDTOFake = obMapper.treeToValue(payloads.get("good_SS"), SecondStageDTO.class);
		ThirdStageDTO tsDTOFake = obMapper.treeToValue(payloads.get("good_TS"), ThirdStageDTO.class);
		
		ProductDTO prodDTOFake = new ProductDTO("99", "3", fsDTOFake, ssDTOFake, tsDTOFake);
		
		when(prodService.findProductByIdFullFetch(eq(99))).thenReturn(prodDummy);
		when(smService.productDtoFrom(eq(prodDummy))).thenReturn(prodDTOFake);

		mockMvc.perform(get("/product/{id}", 99)
			.contentType(MediaType.APPLICATION_JSON))
		.andExpect(jsonPath("$", equalTo(prodDTOFake), ProductDTO.class))
		.andExpect(status().isOk());
		
	}
	
	@Test
	void success_delete_product_for_prodId() throws Exception {
		
		mockMvc.perform(delete("/product/{id}", 99)
			.contentType(MediaType.APPLICATION_JSON))
		.andExpect(jsonPath("$.msg").value("Product 99 deleted"));
		
		verify(prodService, times(1)).delete(eq(99));

	}
	
	
	// SEARCH
	
	@Test	
	void success_generic_search() throws Exception {
		
		ProductDTO foundProdDTOFake1 = obMapper.treeToValue(payloads.get("found_prod_1"), ProductDTO.class);
		ProductDTO foundProdDTOFake2 = obMapper.treeToValue(payloads.get("found_prod_2"), ProductDTO.class);
		Product prodDummy1 = new Product(null, "dummy1", null, null, null, null);
		Product prodDummy2 = new Product(null, "dummy2", null, null, null, null);
		
		when(prodService.findProductByMainTraitsSpec("Brand1", null, null, "ing1", null, null, null, null, null, null))
			.thenReturn(List.of(prodDummy1, prodDummy2));
		when(smService.productDtoFrom(prodDummy1)).thenReturn(foundProdDTOFake1);
		when(smService.productDtoFrom(prodDummy2)).thenReturn(foundProdDTOFake2);

		mockMvc.perform(get("/product/search")
			.queryParam("brand", "Brand1")
			.queryParam("ing", "ing1")
			.contentType(MediaType.APPLICATION_JSON))
		.andExpect(jsonPath("$", hasSize(2)))
		.andExpect(jsonPath("$[0]", equalTo(foundProdDTOFake1), ProductDTO.class))
		.andExpect(jsonPath("$[1]", equalTo(foundProdDTOFake2), ProductDTO.class))
		.andExpect(status().isOk());
	
	}
	
	@Test	
	void success_stage_search() throws Exception {
		
		ProductDTO foundProdDTOFake1 = obMapper.treeToValue(payloads.get("found_prod_1"), ProductDTO.class);
		ProductDTO foundProdDTOFake2 = obMapper.treeToValue(payloads.get("found_prod_2"), ProductDTO.class);
		Product prodDummy1 = new Product(null, "dummy1", null, null, null, null);
		Product prodDummy2 = new Product(null, "dummy2", null, null, null, null);
		
		when(prodService.findProductByStageFullFetch(3)).thenReturn(List.of(prodDummy1, prodDummy2));
		when(smService.productDtoFrom(prodDummy1)).thenReturn(foundProdDTOFake1);
		when(smService.productDtoFrom(prodDummy2)).thenReturn(foundProdDTOFake2);

		mockMvc.perform(get("/product/search/stage")
			.queryParam("stage", "3")
			.contentType(MediaType.APPLICATION_JSON))
		.andExpect(jsonPath("$", hasSize(2)))
		.andExpect(jsonPath("$[0]").value(equalTo(foundProdDTOFake1), ProductDTO.class))
		.andExpect(jsonPath("$[1]", equalTo(foundProdDTOFake2), ProductDTO.class))
		.andExpect(status().isOk());
	
	}
	
	@Test	
	void success_recipe_by_line_search() throws Exception {
		
		when(prodService.findRecipeByLine("Brand1", "Line1")).thenReturn(List.of("matching_recipe_1", "matching_recipe_2"));

		mockMvc.perform(get("/product/search/firstStageByLine")
			.queryParam("brand", "Brand1")
			.queryParam("line", "Line1")
			.contentType(MediaType.APPLICATION_JSON))
		.andExpect(jsonPath("$[0]").value("matching_recipe_1"))
		.andExpect(jsonPath("$[1]").value("matching_recipe_2"))
		.andExpect(status().isOk());
	
	}
	
	
}
