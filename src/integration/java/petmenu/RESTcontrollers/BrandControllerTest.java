package petmenu.RESTcontrollers;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.autoconfigure.context.PropertyPlaceholderAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import petmenu.RESTControllers.products.BrandController;
import petmenu.config.SecurityConfig;
import petmenu.entities.products.Brand;
import petmenu.services.products.BrandService;

@ImportAutoConfiguration(PropertyPlaceholderAutoConfiguration.class)
@Import(SecurityConfig.class)
@ActiveProfiles("integrationTest")
@WebMvcTest(BrandController.class)

public class BrandControllerTest {
	
	@MockBean
 	private BrandService brandService;
	@Autowired
	private MockMvc mockMvc;
    
	@Test
	void listBrands() throws Exception {
    	Brand stub1 = new Brand(); Brand stub2 = new Brand(); Brand stub3 = new Brand();
    	stub1.setName("Purina"); stub2.setName("Coop"); stub3.setName("Hill's");
    	List<Brand> brandsStub = new ArrayList<Brand>(List.of(stub1, stub2, stub3));
    	
    	when(brandService.listByName()).thenReturn(brandsStub);
    	
    	mockMvc.perform(get("/brand/search"))
		.andExpect(status().isOk())
		.andExpect(jsonPath("$.[0]").value(stub1.getName()))
		.andExpect(jsonPath("$.[1]").value(stub2.getName()))
		.andExpect(jsonPath("$.[2]").value(stub3.getName()));

    }

}
