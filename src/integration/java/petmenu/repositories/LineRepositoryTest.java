package petmenu.repositories;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;
import java.util.Set;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ActiveProfiles;

import petmenu.BaseContainerSetup;
import petmenu.config.datasources.LiquibaseConfig;
import petmenu.config.datasources.ProductsDataSourceConfig;
import petmenu.config.datasources.UsersDataSourceConfig;
import petmenu.entities.products.Brand;
import petmenu.entities.products.Line;
import petmenu.entities.products.Product;
import petmenu.entities.products.Product.ProdPackage;
import petmenu.entities.products.Product.ProdType;
import petmenu.repositories.products.LineRepository;

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ActiveProfiles("integrationTest")
@Import({ ProductsDataSourceConfig.class, UsersDataSourceConfig.class, LiquibaseConfig.class })

public class LineRepositoryTest extends BaseContainerSetup{

	@Autowired
	private LineRepository lineRepo;
	@Autowired
	private TestEntityManager em;

	@Test
	void success_find_line_by_id() {
		Brand brandDummy = new Brand("Purina", Set.of());
		Line lineFake = new Line("Fake_line", brandDummy, Set.of());
		
		em.persist(brandDummy);
		Integer fetchedLineId = em.persistAndGetId(lineFake, Integer.class);
		em.flush();
		em.clear();

		Line fetchedLine = lineRepo.findLineById(fetchedLineId);
		assertThat(fetchedLine).isEqualTo(lineFake);
	}

	@Test
	void success_find_line_by_name_case_insensitive_and_blank_chars() {
		Brand brandDummy1 = new Brand("Purina", Set.of());
		Brand brandDummy2 = new Brand("Hill's", Set.of());

		Line lineFake1 = em.persist(new Line("Name with Blanks", brandDummy1, Set.of()));
		Line lineFake2 = em.persist(new Line("Name with Blanks", brandDummy2, Set.of()));
		em.persist(new Line("Different_name", brandDummy2, Set.of()));
		em.persist(brandDummy1);
		em.persist(brandDummy2);
		em.flush();
		em.clear();
		
		List<Line> fetchedLines = lineRepo.findLineByName("name With BLANKS");

		assertThat(fetchedLines).containsOnly(lineFake1, lineFake2);
	}

	@Test
	void fail_find_line_by_name() {
		Brand brandDummy = em.persistFlushFind(new Brand("Purina", Set.of()));
		em.persistFlushFind(new Line("Line1", brandDummy, Set.of()));
		em.clear();

		List<Line> fetchedLines = lineRepo.findLineByName("Different Name");

		assertThat(fetchedLines).isEmpty();
	}
	
	@Test
	void success_find_line_by_name_and_brand_name_case_insensitive_and_blank_chars() {
		Brand brandFake1 = new Brand("Purina", Set.of());
		Brand brandFake2 = new Brand("Hill's", Set.of());

		Line lineFake1 = em.persist(new Line("Name with Blanks", brandFake1, Set.of()));
		em.persist(new Line("Name with Blanks", brandFake2, Set.of()));
		em.persist(brandFake1);
		em.persist(brandFake2);
		
		em.flush();
		em.clear();
		
		Line fetchedLine = lineRepo.findLineByNameAndBrandName("name With BLANKS", "Purina");

		assertThat(fetchedLine).isEqualTo(lineFake1);
	}

	@Test
	void fail_find_line_by_name_and_brand_name() {
		Brand brandFake = em.persist(new Brand("Purina", Set.of()));
		em.persistFlushFind(new Line("Line1", brandFake, Set.of()));
		em.clear();

		Line fetchedLine = lineRepo.findLineByNameAndBrandName("Different Name", "Purina");

		assertThat(fetchedLine).isNull();
	}
	
	@Test
	void success_find_line_by_id_fetching_products() {
		Brand brandFake = new Brand("Purina", Set.of());
		Line lineFake1 = new Line("Line1", brandFake, Set.of());
		Line lineFake2 = new Line("Not_matching_line", brandFake, Set.of());
		Product prodFake1 = new Product(lineFake1, "Recipe 1", ProdType.WET, ProdPackage.POUCH, 100, null);
		Product prodFake2 = new Product(lineFake1, "Recipe 2", ProdType.DRY, ProdPackage.BOX, 200, null);
		Product prodFake3 = new Product(lineFake2, "Not_matching_prod", ProdType.WET, ProdPackage.POUCH, 300, null);
		lineFake1.setProducts(Set.of(prodFake1, prodFake2));
		lineFake2.setProducts(Set.of(prodFake3));
		brandFake.setLines(Set.of(lineFake1, lineFake2));

		Integer lineFakeId = em.persistAndGetId(lineFake1, Integer.class);
		em.persist(brandFake);
		em.flush();
		em.clear();
		
		Line fetchedLine = lineRepo.findLineByIdFetchProducts(lineFakeId);

		assertThat(fetchedLine).isEqualTo(lineFake1);
		assertThat(fetchedLine.getProducts()).containsOnly(prodFake1, prodFake2);
	}
	
	@Test
	void success_find_line_by_id_fetching_empty_lines() {
		Brand brandFake = em.persistFlushFind(new Brand("Purina", Set.of()));
		Line lineFake = new Line("Line1", brandFake, Set.of());
		
		Integer lineFakeId = em.persistAndGetId(lineFake, Integer.class);
		em.flush();
		em.clear();
		
		Line fetchedLine = lineRepo.findLineByIdFetchProducts(lineFakeId);

		assertThat(fetchedLine).isEqualTo(fetchedLine);
		assertThat(fetchedLine.getProducts()).isEmpty();
	}

}
