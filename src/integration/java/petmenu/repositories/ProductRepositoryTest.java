package petmenu.repositories;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ActiveProfiles;

import petmenu.BaseContainerSetup;
import petmenu.config.datasources.LiquibaseConfig;
import petmenu.config.datasources.ProductsDataSourceConfig;
import petmenu.config.datasources.UsersDataSourceConfig;
import petmenu.entities.products.Brand;
import petmenu.entities.products.Component;
import petmenu.entities.products.ComponentQuota;
import petmenu.entities.products.Ingredient;
import petmenu.entities.products.IngredientQuota;
import petmenu.entities.products.Line;
import petmenu.entities.products.Product;
import petmenu.entities.products.Product.ProdPackage;
import petmenu.entities.products.Product.ProdType;
import petmenu.repositories.products.ProductRepository;
import petmenu.repositories.products.ProductSpecs;

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ActiveProfiles("integrationTest")
@Import({ ProductsDataSourceConfig.class, UsersDataSourceConfig.class, LiquibaseConfig.class })

public class ProductRepositoryTest extends BaseContainerSetup {
	
	@Autowired
	private ProductRepository prodRepo;
	@Autowired
	private TestEntityManager em;


	@Test
	void success_find_product_by_id() {
		Brand brandDummy = new Brand("Purina", Set.of());
		Line lineFake = new Line("Fake_line", brandDummy, Set.of());
		Product prodFake = new Product(lineFake, "Recipe 1", ProdType.WET, ProdPackage.POUCH, 100, null);
		
		em.persist(brandDummy);
		em.persist(lineFake);
		Integer fetchedProdId = em.persistAndGetId(prodFake, Integer.class);
		em.flush();
		em.clear();

		Product fetchedProd = prodRepo.findProductById(fetchedProdId);
		assertThat(fetchedProd).isEqualTo(prodFake);
	}

	@Test
	void success_find_product_by_line_and_recipe_case_insensitive_and_blank_chars() {
		Brand brandDummy1 = em.persist(new Brand("Purina", Set.of()));
		Brand brandDummy2 = em.persist(new Brand("Hill's", Set.of()));
		Line lineFake1 = em.persist(new Line("Name with Blanks", brandDummy1, Set.of()));
		Line lineFake2 = em.persist(new Line("Name with Blanks", brandDummy2, Set.of()));
		Line lineFake3 = em.persist(new Line("Different Name with Blanks", brandDummy1, Set.of()));
		Product prodFake1 = em.persist(new Product(lineFake1, "Matching_line_and_recipe", ProdType.WET, ProdPackage.POUCH, 100, null));
		Product prodFake2 = em.persist(new Product(lineFake1, "Matching_line_and_recipe", ProdType.DRY, ProdPackage.BOX, 1300, null));
		em.persist(new Product(lineFake1, "Matching_line_wrong_recipe", ProdType.WET, ProdPackage.POUCH, 100, null));
		em.persist(new Product(lineFake2, "Wrong_brand_and_matching_line_and_recipe", ProdType.WET, ProdPackage.POUCH, 100, null));
		em.persist(new Product(lineFake3, "Wrong_line_and_matching_recipe", ProdType.WET, ProdPackage.POUCH, 100, null));
		
		em.flush();
		em.clear();
		
		List<Product> fetchedProducts = prodRepo.findProductByLineAndRecipe(lineFake1, "Matching_line_and_recipe");

		assertThat(fetchedProducts).containsOnly(prodFake1, prodFake2);
	}

	@Test
	void fail_find_product_by_line_and_recipe() {
		Brand brandDummy1 = em.persist(new Brand("Purina", Set.of()));
		Brand brandDummy2 = em.persist(new Brand("Hill's", Set.of()));
		Line lineFake1 = em.persist(new Line("Name with Blanks", brandDummy1, Set.of()));
		Line lineFake2 = em.persist(new Line("Name with Blanks", brandDummy2, Set.of()));
		Line lineFake3 = em.persist(new Line("Different Name with Blanks", brandDummy1, Set.of()));
		em.persist(new Product(lineFake1, "Matching_line_and_recipe", ProdType.WET, ProdPackage.POUCH, 100, null));
		em.persist(new Product(lineFake1, "Matching_line_and_recipe", ProdType.DRY, ProdPackage.BOX, 1300, null));
		em.persist(new Product(lineFake1, "Matching_line_wrong_recipe", ProdType.WET, ProdPackage.POUCH, 100, null));
		em.persist(new Product(lineFake2, "Wrong_brand_and_matching_line_and_recipe", ProdType.WET, ProdPackage.POUCH, 100, null));
		em.persist(new Product(lineFake3, "Wrong_line_and_matching_recipe", ProdType.WET, ProdPackage.POUCH, 100, null));
		
		em.flush();
		em.clear();
		
		List<Product> fetchedProducts = prodRepo.findProductByLineAndRecipe(lineFake1, "Non_existent_recipe");

		assertThat(fetchedProducts).isEmpty();
	}

	@Test
	void success_find_product_by_fsDto() {
		Brand brandDummy1 = em.persist(new Brand("Purina", Set.of()));
		Line lineFake1 = em.persist(new Line("Name with Blanks", brandDummy1, Set.of()));
		Product prodFake = em.persist(new Product(lineFake1, "Matching_recipe", ProdType.DRY, ProdPackage.BAG, 800, null));
	
		em.flush();
		em.clear();
		
		Product fetchedProduct = prodRepo.findProductByFsDto(lineFake1, "Matching_recipe", ProdPackage.BAG, ProdType.DRY, 800);

		assertThat(fetchedProduct).isEqualTo(prodFake);
	}
	
	@Test
	void fail_find_product_by_fsDto() {
		Brand brandDummy1 = em.persist(new Brand("Purina", Set.of()));
		Brand brandDummy2 = em.persist(new Brand("Hill's", Set.of()));
		Line lineFake1 = em.persist(new Line("Name with Blanks", brandDummy1, Set.of()));
		Line lineFake2 = em.persist(new Line("Different_name", brandDummy1, Set.of()));
		Line lineFake3 = em.persist(new Line("Name with Blanks", brandDummy2, Set.of()));
		em.persist(new Product(lineFake1, "Wrong_type", ProdType.WET, ProdPackage.BAG, 800, null));
		em.persist(new Product(lineFake1, "Wrong_package", ProdType.DRY, ProdPackage.BOX, 800, null));
		em.persist(new Product(lineFake1, "Wrong_weight", ProdType.DRY, ProdPackage.BAG, 500, null));
		em.persist(new Product(lineFake2, "Wrong_line", ProdType.DRY, ProdPackage.BAG, 800, null));
		em.persist(new Product(lineFake3, "Wrong_brand", ProdType.DRY, ProdPackage.BAG, 800, null));
		
		em.flush();
		em.clear();
		
		Product fetchedProduct = prodRepo.findProductByFsDto(lineFake1, "Matching_recipe", ProdPackage.BAG, ProdType.DRY, 800);

		assertThat(fetchedProduct).isEqualTo(null);
	}
	
	@Test
	void success_find_product_by_stage_full_fetch() {
		Brand brandDummy1 = em.persist(new Brand("Purina", Set.of()));
		Brand brandDummy2 = em.persist(new Brand("Hill's", Set.of()));
		Line lineFake1 = em.persist(new Line("Name with Blanks", brandDummy1, Set.of()));
		Line lineFake2 = em.persist(new Line("Different_name", brandDummy1, Set.of()));
		Line lineFake3 = em.persist(new Line("Name with Blanks", brandDummy2, Set.of()));
		Product prodFake1 = new Product(lineFake1, "Wrong_type", ProdType.WET, ProdPackage.BAG, 800, null);
		prodFake1.set_stage(1); em.persist(prodFake1);
		Product prodFake2 = new Product(lineFake1, "Wrong_package", ProdType.DRY, ProdPackage.BOX, 800, null);
		prodFake2.set_stage(2); em.persist(prodFake2);
		Product prodFake3 = new Product(lineFake1, "Wrong_weight", ProdType.DRY, ProdPackage.BAG, 500, null);
		prodFake3.set_stage(3); em.persist(prodFake3);
		Product prodFake4 = new Product(lineFake2, "Wrong_line", ProdType.DRY, ProdPackage.BAG, 800, null);
		prodFake4.set_stage(1); em.persist(prodFake4);
		Product prodFake5 = new Product(lineFake3, "Wrong_brand", ProdType.DRY, ProdPackage.BAG, 800, null);
		prodFake5.set_stage(1); em.persist(prodFake5);
		
		em.flush();
		em.clear();
		
		List<Product> fetchedProducts = prodRepo.findProductByStageFullFetch(1);

		assertThat(fetchedProducts).containsOnly(prodFake1, prodFake4, prodFake5);
	}
	
	@Test
	void success_find_recipe_by_line() {
		Brand brandDummy1 = em.persist(new Brand("Purina", Set.of()));
		Brand brandDummy2 = em.persist(new Brand("Hill's", Set.of()));
		Line lineFake1 = em.persist(new Line("Matching_line", brandDummy1, Set.of()));
		Line lineFake2 = em.persist(new Line("Wrong_line", brandDummy1, Set.of()));
		Line lineFake3 = em.persist(new Line("Wrong_brand", brandDummy2, Set.of()));
		em.persist(new Product(lineFake1, "Matching_recipe1", ProdType.WET, ProdPackage.BAG, 800, null));
		em.persist(new Product(lineFake1, "Matching_recipe2", ProdType.DRY, ProdPackage.BOX, 800, null));
		em.persist(new Product(lineFake1, "Matching_recipe3", ProdType.DRY, ProdPackage.BAG, 500, null));
		em.persist(new Product(lineFake2, "Wrong_line_recipe", ProdType.DRY, ProdPackage.BAG, 800, null));
		em.persist(new Product(lineFake3, "Wrong_brand_recipe", ProdType.DRY, ProdPackage.BAG, 800, null));
		
		em.flush();
		em.clear();
		
		List<String> fetchedProduct = prodRepo.findRecipeByLine("purina", "matching_LINE");

		assertThat(fetchedProduct).containsOnly("Matching_recipe1", "Matching_recipe2", "Matching_recipe3");
	}
	
	@Test
	void success_find_product_by_id_full_fetch() {
		Brand brandDummy1 = em.persist(new Brand("Purina", Set.of()));
		Line lineFake1 = em.persist(new Line("Name with Blanks", brandDummy1, Set.of()));
		Product prodFake = em.persistFlushFind(new Product(lineFake1, "Matching_recipe", ProdType.DRY, ProdPackage.BAG, 800, Set.of()));
		Ingredient ing1 = em.persist(new Ingredient("ing1", Set.of()));
		Ingredient ing2 = em.persist(new Ingredient("ing2", Set.of()));
		Ingredient ing3 = em.persist(new Ingredient("ing3", Set.of()));
		Component comp1 = em.persist(new Component("comp1", Set.of()));
		Component comp2 = em.persist(new Component("comp2", Set.of()));
		Component comp3 = em.persist(new Component("comp3", Set.of()));
		IngredientQuota iq1 = em.persist(new IngredientQuota(1, prodFake, ing1, 70D, Set.of()));
		IngredientQuota iq2 = em.persist(new IngredientQuota(2, prodFake, ing2, 20D, Set.of()));
		IngredientQuota iq3 = em.persist(new IngredientQuota(3, prodFake, ing3, null, Set.of()));
		ComponentQuota cq1 = em.persist(new ComponentQuota(1, iq1, comp1, 70D));
		ComponentQuota cq2 = em.persist(new ComponentQuota(2, iq1, comp2, null));
		ComponentQuota cq3 = em.persist(new ComponentQuota(1, iq3, comp3, 30D));

		em.flush();
		em.clear();
		
		Product fetchedProduct = prodRepo.findProductByIdFullFetch(prodFake.getId());

		assertThat(fetchedProduct).isEqualTo(prodFake);
		assertThat(fetchedProduct.getIngQuotasList()).containsExactly(iq1, iq2, iq3);
		Iterator<IngredientQuota> iqListIter = fetchedProduct.getIngQuotasList().iterator();
		assertThat(iqListIter.next().getCompQuotasList()).containsExactly(cq1, cq2);
		assertThat(iqListIter.next().getCompQuotasList()).isEmpty();
		assertThat(iqListIter.next().getCompQuotasList()).containsExactly(cq3);
	}
	
	@Test
	void success_find_product_by_stage_full_fetch_ordered() {
		Brand brandDummy1 = em.persist(new Brand("Purina", Set.of()));
		Brand brandDummy2 = em.persist(new Brand("Hill's", Set.of()));
		Line lineFake1 = em.persist(new Line("Name with Blanks", brandDummy1, Set.of()));
		Line lineFake2 = em.persist(new Line("Different_name", brandDummy1, Set.of()));
		Line lineFake3 = em.persist(new Line("Name with Blanks", brandDummy2, Set.of()));
		Product prodFake1 = new Product(lineFake1, "Wrong_type", ProdType.WET, ProdPackage.BAG, 800, null);
		prodFake1.set_stage(3); em.persist(prodFake1);
		Product prodFake2 = new Product(lineFake1, "Wrong_package", ProdType.DRY, ProdPackage.BOX, 800, null);
		prodFake2.set_stage(2); em.persist(prodFake2);
		Product prodFake3 = new Product(lineFake1, "Wrong_weight", ProdType.DRY, ProdPackage.BAG, 500, null);
		prodFake3.set_stage(1); em.persist(prodFake3);
		Product prodFake4 = new Product(lineFake2, "Wrong_line", ProdType.DRY, ProdPackage.BAG, 800, null);
		prodFake4.set_stage(3); em.persist(prodFake4);
		Product prodFake5 = new Product(lineFake3, "Wrong_brand", ProdType.DRY, ProdPackage.BAG, 800, null);
		prodFake5.set_stage(3); em.persist(prodFake5);
		
		Ingredient ing1 = em.persist(new Ingredient("ing1", Set.of()));
		Ingredient ing2 = em.persist(new Ingredient("ing2", Set.of()));
		Ingredient ing3 = em.persist(new Ingredient("ing3", Set.of()));
		Component comp1 = em.persist(new Component("comp1", Set.of()));
		Component comp2 = em.persist(new Component("comp2", Set.of()));
		Component comp3 = em.persist(new Component("comp3", Set.of()));
		IngredientQuota iq1 = em.persist(new IngredientQuota(1, prodFake1, ing1, 70D, Set.of()));
		IngredientQuota iq2 = em.persist(new IngredientQuota(2, prodFake4, ing2, 20D, Set.of()));
		IngredientQuota iq3 = em.persist(new IngredientQuota(3, prodFake4, ing3, null, Set.of()));
		ComponentQuota cq1 = em.persist(new ComponentQuota(1, iq1, comp1, 70D));
		ComponentQuota cq2 = em.persist(new ComponentQuota(2, iq1, comp2, null));
		ComponentQuota cq3 = em.persist(new ComponentQuota(1, iq3, comp3, 30D));

		em.flush();
		em.clear();
		
		ArrayList<Product> fetchedProducts = new ArrayList<Product>();
		fetchedProducts.addAll(prodRepo.findProductByStageFullFetch(3));

		assertThat(fetchedProducts).containsExactly(prodFake5, prodFake4, prodFake1);
		assertThat(fetchedProducts.get(0).getIngQuotasList()).isEmpty();
		assertThat(fetchedProducts.get(1).getIngQuotasList()).containsExactly(iq2, iq3);
		assertThat(fetchedProducts.get(2).getIngQuotasList()).containsExactly(iq1);
		Iterator<IngredientQuota> prod2iter = fetchedProducts.get(1).getIngQuotasList().iterator();
		assertThat(prod2iter.next().getCompQuotasList()).isEmpty();
		assertThat(prod2iter.next().getCompQuotasList()).containsExactly(cq3);
		Iterator<IngredientQuota> prod3iter = fetchedProducts.get(2).getIngQuotasList().iterator();
		assertThat(prod3iter.next().getCompQuotasList()).containsExactly(cq1, cq2);
	}
	
	@Test
	void success_find_product_by_spec_for_brand_full_fetch_default_order() {
		Brand brandDummy1 = em.persist(new Brand("Purina", Set.of()));
		Brand brandDummy2 = em.persist(new Brand("Hill's", Set.of()));
		Line lineFake1 = em.persist(new Line("Name with Blanks", brandDummy1, Set.of()));
		Line lineFake2 = em.persist(new Line("Different_name", brandDummy1, Set.of()));
		Line lineFake3 = em.persist(new Line("Name with Blanks", brandDummy2, Set.of()));
		Product prodFake1 = new Product(lineFake1, "Fourth_because_type", ProdType.WET, ProdPackage.BAG, 800, null);
		prodFake1.set_stage(3); em.persist(prodFake1);
		Product prodFake2 = new Product(lineFake1, "Third_because_package", ProdType.DRY, ProdPackage.BOX, 800, null);
		prodFake2.set_stage(2); em.persist(prodFake2);
		Product prodFake3 = new Product(lineFake1, "Second_because_package", ProdType.DRY, ProdPackage.BAG, 500, null);
		prodFake3.set_stage(1); em.persist(prodFake3);
		Product prodFake4 = new Product(lineFake2, "First_because_line", ProdType.DRY, ProdPackage.BAG, 800, null);
		prodFake4.set_stage(3); em.persist(prodFake4);
		Product prodFake5 = new Product(lineFake3, "Wrong_brand", ProdType.DRY, ProdPackage.BAG, 800, null);
		prodFake5.set_stage(3); em.persist(prodFake5);
		
		Ingredient ing1 = em.persist(new Ingredient("ing1", Set.of()));
		Ingredient ing2 = em.persist(new Ingredient("ing2", Set.of()));
		Ingredient ing3 = em.persist(new Ingredient("ing3", Set.of()));
		Component comp1 = em.persist(new Component("comp1", Set.of()));
		Component comp2 = em.persist(new Component("comp2", Set.of()));
		Component comp3 = em.persist(new Component("comp3", Set.of()));
		IngredientQuota iq1 = em.persist(new IngredientQuota(1, prodFake1, ing1, 70D, Set.of()));
		IngredientQuota iq2 = em.persist(new IngredientQuota(1, prodFake4, ing2, 20D, Set.of()));
		IngredientQuota iq3 = em.persist(new IngredientQuota(2, prodFake4, ing3, null, Set.of()));
		ComponentQuota cq1 = em.persist(new ComponentQuota(1, iq1, comp1, 70D));
		ComponentQuota cq2 = em.persist(new ComponentQuota(2, iq1, comp2, null));
		ComponentQuota cq3 = em.persist(new ComponentQuota(1, iq3, comp3, 30D));

		em.flush();
		em.clear();
		
		ArrayList<Product> fetchedProducts = new ArrayList<Product>();
		fetchedProducts
			.addAll(
				prodRepo.findAllSubselecting(ProductSpecs.getProductByMainTraitsSpec("purina", null, null, null, null, null, null, null, null, null))
			);

		assertThat(fetchedProducts).containsExactly(prodFake4, prodFake3, prodFake2, prodFake1);
		assertThat(fetchedProducts.get(0).getIngQuotasList()).containsExactly(iq2, iq3);
		assertThat(fetchedProducts.get(1).getIngQuotasList()).isEmpty();
		assertThat(fetchedProducts.get(2).getIngQuotasList()).isEmpty();
		assertThat(fetchedProducts.get(3).getIngQuotasList()).containsExactly(iq1);
		Iterator<IngredientQuota> prod4iter = fetchedProducts.get(0).getIngQuotasList().iterator();
		assertThat(prod4iter.next().getCompQuotasList()).isEmpty();
		assertThat(prod4iter.next().getCompQuotasList()).containsExactly(cq3);
		Iterator<IngredientQuota> prod1iter = fetchedProducts.get(3).getIngQuotasList().iterator();
		assertThat(prod1iter.next().getCompQuotasList()).containsExactly(cq1, cq2);
	}
	
	@Test
	void success_find_product_by_spec_for_brand_and_ing_full_subselect_default_order() {
		Brand brandDummy1 = em.persist(new Brand("Purina", Set.of()));
		Brand brandDummy2 = em.persist(new Brand("Hill's", Set.of()));
		Line lineFake1 = em.persist(new Line("Name with Blanks", brandDummy1, Set.of()));
		Line lineFake2 = em.persist(new Line("Different_name", brandDummy1, Set.of()));
		Line lineFake3 = em.persist(new Line("Name with Blanks", brandDummy2, Set.of()));
		Product prodFake1 = new Product(lineFake1, "Third_because_package", ProdType.WET, ProdPackage.BAG, 800, null);
		prodFake1.set_stage(3); em.persist(prodFake1);
		Product prodFake2 = new Product(lineFake1, "Missing_ing", ProdType.DRY, ProdPackage.BOX, 800, null);
		prodFake2.set_stage(2); em.persist(prodFake2);
		Product prodFake3 = new Product(lineFake1, "Second_because_package", ProdType.DRY, ProdPackage.BAG, 500, null);
		prodFake3.set_stage(1); em.persist(prodFake3);
		Product prodFake4 = new Product(lineFake2, "First_because_line", ProdType.DRY, ProdPackage.BAG, 800, null);
		prodFake4.set_stage(3); em.persist(prodFake4);
		Product prodFake5 = new Product(lineFake3, "Wrong_brand", ProdType.DRY, ProdPackage.BAG, 800, null);
		prodFake5.set_stage(3); em.persist(prodFake5);
		
		Ingredient ing1 = em.persist(new Ingredient("ing1", Set.of()));
		Ingredient ing2 = em.persist(new Ingredient("ing2", Set.of()));
		Ingredient ing3 = em.persist(new Ingredient("ing3", Set.of()));
		Component comp1 = em.persist(new Component("comp1", Set.of()));
		Component comp2 = em.persist(new Component("comp2", Set.of()));
		Component comp3 = em.persist(new Component("comp3", Set.of()));
		Component comp4 = em.persist(new Component("comp4", Set.of()));
		Component comp5 = em.persist(new Component("comp5", Set.of()));
		IngredientQuota iq1 = em.persist(new IngredientQuota(1, prodFake1, ing1, 70D, Set.of()));
		IngredientQuota iq2 = em.persist(new IngredientQuota(1, prodFake4, ing2, 20D, Set.of()));
		IngredientQuota iq3 = em.persist(new IngredientQuota(2, prodFake4, ing3, null, Set.of()));
		IngredientQuota iq4 = em.persist(new IngredientQuota(3, prodFake4, ing1, 30D, Set.of()));
		IngredientQuota iq5 = em.persist(new IngredientQuota(3, prodFake5, ing1, null, Set.of()));
		IngredientQuota iq6 = em.persist(new IngredientQuota(1, prodFake3, ing1, 50D, Set.of()));
		IngredientQuota iq7 = em.persist(new IngredientQuota(2, prodFake3, ing2, 50D, Set.of()));
		ComponentQuota cq1 = em.persist(new ComponentQuota(1, iq1, comp1, 70D));
		ComponentQuota cq2 = em.persist(new ComponentQuota(2, iq1, comp2, null));
		ComponentQuota cq3 = em.persist(new ComponentQuota(1, iq3, comp3, 30D));
		ComponentQuota cq4 = em.persist(new ComponentQuota(1, iq6, comp3, null));
		ComponentQuota cq5 = em.persist(new ComponentQuota(2, iq6, comp4, 50D));
		ComponentQuota cq6 = em.persist(new ComponentQuota(1, iq4, comp1, 60D));
		ComponentQuota cq7 = em.persist(new ComponentQuota(2, iq4, comp5, null));
		ComponentQuota cq8 = em.persist(new ComponentQuota(3, iq4, comp3, 20D));
		em.persist(new ComponentQuota(1, iq5, comp3, 20D));

		em.flush();
		em.clear();
		
		ArrayList<Product> fetchedProducts = new ArrayList<Product>();
		fetchedProducts
			.addAll(
				prodRepo.findAllSubselecting(
						ProductSpecs.getProductByMainTraitsSpec("purina", null, null, "ing1", null, null, null, null, null, null)
						)
			);

		assertThat(fetchedProducts).containsExactly(prodFake4, prodFake3, prodFake1);
		assertThat(fetchedProducts.get(0).getIngQuotasList()).containsExactly(iq2, iq3, iq4);
		assertThat(fetchedProducts.get(1).getIngQuotasList()).containsExactly(iq6, iq7);
		assertThat(fetchedProducts.get(2).getIngQuotasList()).containsExactly(iq1);
		Iterator<IngredientQuota> prod4iter = fetchedProducts.get(0).getIngQuotasList().iterator();
		assertThat(prod4iter.next().getCompQuotasList()).isEmpty();
		assertThat(prod4iter.next().getCompQuotasList()).containsExactly(cq3);
		assertThat(prod4iter.next().getCompQuotasList()).containsExactly(cq6, cq7, cq8);
		Iterator<IngredientQuota> prod3iter = fetchedProducts.get(1).getIngQuotasList().iterator();
		assertThat(prod3iter.next().getCompQuotasList()).containsExactly(cq4, cq5);
		assertThat(prod3iter.next().getCompQuotasList()).isEmpty();
		Iterator<IngredientQuota> prod1iter = fetchedProducts.get(2).getIngQuotasList().iterator();
		assertThat(prod1iter.next().getCompQuotasList()).containsExactly(cq1, cq2);
	}
	
	@Test
	void success_find_product_by_spec_for_brand_and_ing_and_comp_full_subselect_default_order() {
		Brand brandDummy1 = em.persist(new Brand("Purina", Set.of()));
		Brand brandDummy2 = em.persist(new Brand("Hill's", Set.of()));
		Line lineFake1 = em.persist(new Line("Name with Blanks", brandDummy1, Set.of()));
		Line lineFake2 = em.persist(new Line("Different_name", brandDummy1, Set.of()));
		Line lineFake3 = em.persist(new Line("Name with Blanks", brandDummy2, Set.of()));
		Product prodFake1 = new Product(lineFake1, "Second_because_line", ProdType.WET, ProdPackage.BAG, 800, null);
		prodFake1.set_stage(3); em.persist(prodFake1);
		Product prodFake2 = new Product(lineFake1, "Missing", ProdType.DRY, ProdPackage.BOX, 800, null);
		prodFake2.set_stage(2); em.persist(prodFake2);
		Product prodFake3 = new Product(lineFake1, "Missing", ProdType.DRY, ProdPackage.BAG, 500, null);
		prodFake3.set_stage(1); em.persist(prodFake3);
		Product prodFake4 = new Product(lineFake2, "First_because_line", ProdType.DRY, ProdPackage.BAG, 800, null);
		prodFake4.set_stage(3); em.persist(prodFake4);
		Product prodFake5 = new Product(lineFake3, "Missing", ProdType.DRY, ProdPackage.BAG, 800, null);
		prodFake5.set_stage(3); em.persist(prodFake5);
		
		Ingredient ing1 = em.persist(new Ingredient("ing1", Set.of()));
		Ingredient ing2 = em.persist(new Ingredient("ing2", Set.of()));
		Ingredient ing3 = em.persist(new Ingredient("ing3", Set.of()));
		Component comp1 = em.persist(new Component("comp1", Set.of()));
		Component comp2 = em.persist(new Component("comp2", Set.of()));
		Component comp3 = em.persist(new Component("comp3", Set.of()));
		Component comp4 = em.persist(new Component("comp4", Set.of()));
		Component comp5 = em.persist(new Component("comp5", Set.of()));
		IngredientQuota iq1 = em.persist(new IngredientQuota(1, prodFake1, ing1, 70D, Set.of()));
		IngredientQuota iq2 = em.persist(new IngredientQuota(1, prodFake4, ing2, 20D, Set.of()));
		IngredientQuota iq3 = em.persist(new IngredientQuota(2, prodFake4, ing3, null, Set.of()));
		IngredientQuota iq4 = em.persist(new IngredientQuota(3, prodFake4, ing1, 30D, Set.of()));
		IngredientQuota iq5 = em.persist(new IngredientQuota(3, prodFake5, ing3, null, Set.of()));
		IngredientQuota iq6 = em.persist(new IngredientQuota(1, prodFake3, ing1, 50D, Set.of()));
		em.persist(new IngredientQuota(2, prodFake3, ing2, 50D, Set.of()));
		ComponentQuota cq1 = em.persist(new ComponentQuota(1, iq1, comp1, 70D));
		ComponentQuota cq2 = em.persist(new ComponentQuota(2, iq1, comp2, null));
		ComponentQuota cq3 = em.persist(new ComponentQuota(1, iq3, comp1, 30D));
		em.persist(new ComponentQuota(1, iq6, comp3, null));
		em.persist(new ComponentQuota(2, iq6, comp4, 50D));
		ComponentQuota cq6 = em.persist(new ComponentQuota(1, iq4, comp1, 60D));
		ComponentQuota cq7 = em.persist(new ComponentQuota(2, iq4, comp5, null));
		ComponentQuota cq8 = em.persist(new ComponentQuota(3, iq4, comp3, 20D));
		em.persist(new ComponentQuota(1, iq5, comp1, 20D));

		em.flush();
		em.clear();
		
		ArrayList<Product> fetchedProducts = new ArrayList<Product>();
		fetchedProducts
			.addAll(
				prodRepo.findAllSubselecting(
						ProductSpecs.getProductByMainTraitsSpec("purina", null, null, "ing1", "comp1", null, null, null, null, null)
						)
			);

		assertThat(fetchedProducts).containsExactly(prodFake4, prodFake1);
		assertThat(fetchedProducts.get(0).getIngQuotasList()).containsExactly(iq2, iq3, iq4);
		assertThat(fetchedProducts.get(1).getIngQuotasList()).containsExactly(iq1);
		Iterator<IngredientQuota> prod4iter = fetchedProducts.get(0).getIngQuotasList().iterator();
		assertThat(prod4iter.next().getCompQuotasList()).isEmpty();
		assertThat(prod4iter.next().getCompQuotasList()).containsExactly(cq3);
		assertThat(prod4iter.next().getCompQuotasList()).containsExactly(cq6, cq7, cq8);
		Iterator<IngredientQuota> prod1iter = fetchedProducts.get(1).getIngQuotasList().iterator();
		assertThat(prod1iter.next().getCompQuotasList()).containsExactly(cq1, cq2);
	}
	
	@Test
	void success_find_product_by_spec_for_ing_and_comp_full_subselect_default_order() {
		Brand brandDummy1 = em.persist(new Brand("Purina", Set.of()));
		Brand brandDummy2 = em.persist(new Brand("Hill's", Set.of()));
		Line lineFake1 = em.persist(new Line("Name with Blanks", brandDummy1, Set.of()));
		Line lineFake2 = em.persist(new Line("Different_name", brandDummy1, Set.of()));
		Line lineFake3 = em.persist(new Line("Name with Blanks", brandDummy2, Set.of()));
		Product prodFake1 = new Product(lineFake1, "Third_because_type", ProdType.WET, ProdPackage.BAG, 800, null);
		prodFake1.set_stage(3); em.persist(prodFake1);
		Product prodFake2 = new Product(lineFake1, "Missing", ProdType.DRY, ProdPackage.BOX, 800, null);
		prodFake2.set_stage(2); em.persist(prodFake2);
		Product prodFake3 = new Product(lineFake1, "Second_because_type", ProdType.DRY, ProdPackage.BAG, 500, null);
		prodFake3.set_stage(1); em.persist(prodFake3);
		Product prodFake4 = new Product(lineFake2, "First_because_line", ProdType.DRY, ProdPackage.BAG, 800, null);
		prodFake4.set_stage(3); em.persist(prodFake4);
		Product prodFake5 = new Product(lineFake3, "Missing", ProdType.DRY, ProdPackage.BAG, 800, null);
		prodFake5.set_stage(3); em.persist(prodFake5);
		
		Ingredient ing1 = em.persist(new Ingredient("ing1", Set.of()));
		Ingredient ing2 = em.persist(new Ingredient("ing2", Set.of()));
		Ingredient ing3 = em.persist(new Ingredient("ing3", Set.of()));
		Component comp1 = em.persist(new Component("comp1", Set.of()));
		Component comp2 = em.persist(new Component("comp2", Set.of()));
		Component comp3 = em.persist(new Component("comp3", Set.of()));
		em.persist(new Component("comp4", Set.of()));
		Component comp5 = em.persist(new Component("comp5", Set.of()));
		IngredientQuota iq1 = em.persist(new IngredientQuota(1, prodFake1, ing1, 70D, Set.of()));
		IngredientQuota iq2 = em.persist(new IngredientQuota(1, prodFake4, ing2, 20D, Set.of()));
		IngredientQuota iq3 = em.persist(new IngredientQuota(2, prodFake4, ing3, null, Set.of()));
		IngredientQuota iq4 = em.persist(new IngredientQuota(3, prodFake4, ing1, 30D, Set.of()));
		IngredientQuota iq5 = em.persist(new IngredientQuota(3, prodFake5, ing1, null, Set.of()));
		IngredientQuota iq6 = em.persist(new IngredientQuota(1, prodFake3, ing1, 50D, Set.of()));
		IngredientQuota iq7 = em.persist(new IngredientQuota(2, prodFake3, ing2, 50D, Set.of()));
		ComponentQuota cq1 = em.persist(new ComponentQuota(1, iq1, comp1, 70D));
		ComponentQuota cq2 = em.persist(new ComponentQuota(2, iq1, comp2, null));
		ComponentQuota cq3 = em.persist(new ComponentQuota(1, iq3, comp3, 30D));
		ComponentQuota cq4 = em.persist(new ComponentQuota(1, iq6, comp3, null));
		ComponentQuota cq5 = em.persist(new ComponentQuota(2, iq6, comp1, 50D));
		ComponentQuota cq6 = em.persist(new ComponentQuota(1, iq4, comp1, 60D));
		ComponentQuota cq7 = em.persist(new ComponentQuota(2, iq4, comp5, null));
		ComponentQuota cq8 = em.persist(new ComponentQuota(3, iq4, comp3, 20D));
		em.persist(new ComponentQuota(1, iq5, comp3, 20D));

		em.flush();
		em.clear();
		
		ArrayList<Product> fetchedProducts = new ArrayList<Product>();
		fetchedProducts
			.addAll(
				prodRepo.findAllSubselecting(
						ProductSpecs.getProductByMainTraitsSpec(null, null, null, "ing1", "comp1", null, null, null, null, null)
						)
			);

		assertThat(fetchedProducts).containsExactly(prodFake4, prodFake3, prodFake1);
		assertThat(fetchedProducts.get(0).getIngQuotasList()).containsExactly(iq2, iq3, iq4);
		assertThat(fetchedProducts.get(1).getIngQuotasList()).containsExactly(iq6, iq7);
		assertThat(fetchedProducts.get(2).getIngQuotasList()).containsExactly(iq1);
		Iterator<IngredientQuota> prod4iter = fetchedProducts.get(0).getIngQuotasList().iterator();
		assertThat(prod4iter.next().getCompQuotasList()).isEmpty();
		assertThat(prod4iter.next().getCompQuotasList()).containsExactly(cq3);
		assertThat(prod4iter.next().getCompQuotasList()).containsExactly(cq6, cq7, cq8);
		Iterator<IngredientQuota> prod3iter = fetchedProducts.get(1).getIngQuotasList().iterator();
		assertThat(prod3iter.next().getCompQuotasList()).containsExactly(cq4, cq5);
		assertThat(prod3iter.next().getCompQuotasList()).isEmpty();
		Iterator<IngredientQuota> prod1iter = fetchedProducts.get(2).getIngQuotasList().iterator();
		assertThat(prod1iter.next().getCompQuotasList()).containsExactly(cq1, cq2);
	}
	
	@Test
	void success_find_product_by_spec_for_type_and_min_weight_full_subselect_default_order() {
		Brand brandDummy1 = em.persist(new Brand("Purina", Set.of()));
		Brand brandDummy2 = em.persist(new Brand("Hill's", Set.of()));
		Line lineFake1 = em.persist(new Line("Name with Blanks", brandDummy1, Set.of()));
		Line lineFake2 = em.persist(new Line("Different_name", brandDummy2, Set.of()));
		Line lineFake3 = em.persist(new Line("Name with Blanks", brandDummy2, Set.of()));
		Product prodFake1 = new Product(lineFake1, "Missing", ProdType.WET, ProdPackage.BAG, 800, null);
		prodFake1.set_stage(3); em.persist(prodFake1);
		Product prodFake2 = new Product(lineFake1, "Matching", ProdType.DRY, ProdPackage.BOX, 1000, null);
		prodFake2.set_stage(2); em.persist(prodFake2);
		Product prodFake3 = new Product(lineFake1, "Missing", ProdType.DRY, ProdPackage.BAG, 500, null);
		prodFake3.set_stage(1); em.persist(prodFake3);
		Product prodFake4 = new Product(lineFake2, "Matching", ProdType.DRY, ProdPackage.BAG, 800, null);
		prodFake4.set_stage(3); em.persist(prodFake4);
		Product prodFake5 = new Product(lineFake3, "Missing", ProdType.DRY, ProdPackage.BAG, 700, null);
		prodFake5.set_stage(3); em.persist(prodFake5);
		
		Ingredient ing1 = em.persist(new Ingredient("ing1", Set.of()));
		Ingredient ing2 = em.persist(new Ingredient("ing2", Set.of()));
		Ingredient ing3 = em.persist(new Ingredient("ing3", Set.of()));
		Component comp1 = em.persist(new Component("comp1", Set.of()));
		Component comp2 = em.persist(new Component("comp2", Set.of()));
		Component comp3 = em.persist(new Component("comp3", Set.of()));
		em.persist(new Component("comp4", Set.of()));
		Component comp5 = em.persist(new Component("comp5", Set.of()));
		IngredientQuota iq1 = em.persist(new IngredientQuota(1, prodFake1, ing1, 70D, Set.of()));
		IngredientQuota iq2 = em.persist(new IngredientQuota(1, prodFake4, ing2, 20D, Set.of()));
		IngredientQuota iq3 = em.persist(new IngredientQuota(2, prodFake4, ing3, null, Set.of()));
		IngredientQuota iq4 = em.persist(new IngredientQuota(3, prodFake4, ing1, 30D, Set.of()));
		IngredientQuota iq5 = em.persist(new IngredientQuota(3, prodFake5, ing1, null, Set.of()));
		IngredientQuota iq6 = em.persist(new IngredientQuota(1, prodFake3, ing1, 50D, Set.of()));
		em.persist(new IngredientQuota(2, prodFake3, ing2, 50D, Set.of()));
		em.persist(new ComponentQuota(1, iq1, comp1, 70D));
		em.persist(new ComponentQuota(2, iq1, comp2, null));
		ComponentQuota cq3 = em.persist(new ComponentQuota(1, iq3, comp3, 30D));
		em.persist(new ComponentQuota(1, iq6, comp3, null));
		em.persist(new ComponentQuota(2, iq6, comp1, 50D));
		ComponentQuota cq6 = em.persist(new ComponentQuota(1, iq4, comp1, 60D));
		ComponentQuota cq7 = em.persist(new ComponentQuota(2, iq4, comp5, null));
		ComponentQuota cq8 = em.persist(new ComponentQuota(3, iq4, comp3, 20D));
		em.persist(new ComponentQuota(1, iq5, comp3, 20D));

		em.flush();
		em.clear();
		
		ArrayList<Product> fetchedProducts = new ArrayList<Product>();
		fetchedProducts
			.addAll(
				prodRepo.findAllSubselecting(
						ProductSpecs.getProductByMainTraitsSpec(null, null, null, null, null, "DRY", null, 800, null, null)
						)
			);

		assertThat(fetchedProducts).containsExactly(prodFake4, prodFake2);
		assertThat(fetchedProducts.get(0).getIngQuotasList()).containsExactly(iq2, iq3, iq4);
		assertThat(fetchedProducts.get(1).getIngQuotasList()).isEmpty();
		Iterator<IngredientQuota> prod4iter = fetchedProducts.get(0).getIngQuotasList().iterator();
		assertThat(prod4iter.next().getCompQuotasList()).isEmpty();
		assertThat(prod4iter.next().getCompQuotasList()).containsExactly(cq3);
		assertThat(prod4iter.next().getCompQuotasList()).containsExactly(cq6, cq7, cq8);
	}

	@Test
	void success_find_product_by_spec_for_packaging_and_max_weight_full_subselect_default_order() {
		Brand brandDummy1 = em.persist(new Brand("Purina", Set.of()));
		Brand brandDummy2 = em.persist(new Brand("Hill's", Set.of()));
		Line lineFake1 = em.persist(new Line("Name with Blanks", brandDummy1, Set.of()));
		Line lineFake2 = em.persist(new Line("Different_name", brandDummy2, Set.of()));
		Line lineFake3 = em.persist(new Line("Name with Blanks", brandDummy2, Set.of()));
		Product prodFake1 = new Product(lineFake1, "Matching", ProdType.WET, ProdPackage.BOX, 800, null);
		prodFake1.set_stage(3); em.persist(prodFake1);
		Product prodFake2 = new Product(lineFake1, "Missing", ProdType.DRY, ProdPackage.BOX, 1000, null);
		prodFake2.set_stage(2); em.persist(prodFake2);
		Product prodFake3 = new Product(lineFake1, "Missing", ProdType.DRY, ProdPackage.CAN, 500, null);
		prodFake3.set_stage(1); em.persist(prodFake3);
		Product prodFake4 = new Product(lineFake2, "Matching", ProdType.DRY, ProdPackage.BOX, 500, null);
		prodFake4.set_stage(3); em.persist(prodFake4);
		Product prodFake5 = new Product(lineFake3, "Matching", ProdType.DRY, ProdPackage.BOX, 700, null);
		prodFake5.set_stage(3); em.persist(prodFake5);
		
		Ingredient ing1 = em.persist(new Ingredient("ing1", Set.of()));
		Ingredient ing2 = em.persist(new Ingredient("ing2", Set.of()));
		Ingredient ing3 = em.persist(new Ingredient("ing3", Set.of()));
		Component comp1 = em.persist(new Component("comp1", Set.of()));
		Component comp2 = em.persist(new Component("comp2", Set.of()));
		Component comp3 = em.persist(new Component("comp3", Set.of()));
		em.persist(new Component("comp4", Set.of()));
		Component comp5 = em.persist(new Component("comp5", Set.of()));
		IngredientQuota iq1 = em.persist(new IngredientQuota(1, prodFake1, ing1, 70D, Set.of()));
		IngredientQuota iq2 = em.persist(new IngredientQuota(1, prodFake4, ing2, 20D, Set.of()));
		IngredientQuota iq3 = em.persist(new IngredientQuota(2, prodFake4, ing3, null, Set.of()));
		IngredientQuota iq4 = em.persist(new IngredientQuota(3, prodFake4, ing1, 30D, Set.of()));
		IngredientQuota iq5 = em.persist(new IngredientQuota(3, prodFake5, ing1, null, Set.of()));
		IngredientQuota iq6 = em.persist(new IngredientQuota(1, prodFake3, ing1, 50D, Set.of()));
		em.persist(new IngredientQuota(2, prodFake3, ing2, 50D, Set.of()));
		ComponentQuota cq1 = em.persist(new ComponentQuota(1, iq1, comp1, 70D));
		ComponentQuota cq2 = em.persist(new ComponentQuota(2, iq1, comp2, null));
		ComponentQuota cq3 = em.persist(new ComponentQuota(1, iq3, comp3, 30D));
		em.persist(new ComponentQuota(1, iq6, comp3, null));
		em.persist(new ComponentQuota(2, iq6, comp1, 50D));
		ComponentQuota cq6 = em.persist(new ComponentQuota(1, iq4, comp1, 60D));
		ComponentQuota cq7 = em.persist(new ComponentQuota(2, iq4, comp5, null));
		ComponentQuota cq8 = em.persist(new ComponentQuota(3, iq4, comp3, 20D));
		ComponentQuota cq9 = em.persist(new ComponentQuota(1, iq5, comp3, 20D));

		em.flush();
		em.clear();
		
		ArrayList<Product> fetchedProducts = new ArrayList<Product>();
		fetchedProducts
			.addAll(
				prodRepo.findAllSubselecting(
						ProductSpecs.getProductByMainTraitsSpec(null, null, null, null, null, null, "BOX", null, 800, null)
						)
			);

		assertThat(fetchedProducts).containsExactly(prodFake4, prodFake5, prodFake1);
		assertThat(fetchedProducts.get(0).getIngQuotasList()).containsExactly(iq2, iq3, iq4);
		assertThat(fetchedProducts.get(1).getIngQuotasList()).containsExactly(iq5);
		assertThat(fetchedProducts.get(2).getIngQuotasList()).containsExactly(iq1);
		Iterator<IngredientQuota> prod4iter = fetchedProducts.get(0).getIngQuotasList().iterator();
		assertThat(prod4iter.next().getCompQuotasList()).isEmpty();
		assertThat(prod4iter.next().getCompQuotasList()).containsExactly(cq3);
		assertThat(prod4iter.next().getCompQuotasList()).containsExactly(cq6, cq7, cq8);
		Iterator<IngredientQuota> prod5iter = fetchedProducts.get(1).getIngQuotasList().iterator();
		assertThat(prod5iter.next().getCompQuotasList()).containsExactly(cq9);
		Iterator<IngredientQuota> prod1iter = fetchedProducts.get(2).getIngQuotasList().iterator();
		assertThat(prod1iter.next().getCompQuotasList()).containsExactly(cq1, cq2);
	}

	@Test
	void success_find_product_by_spec_for_ing_and_between_weight_full_subselect_default_order() {
		Brand brandDummy1 = em.persist(new Brand("Purina", Set.of()));
		Brand brandDummy2 = em.persist(new Brand("Hill's", Set.of()));
		Line lineFake1 = em.persist(new Line("Name with Blanks", brandDummy1, Set.of()));
		Line lineFake2 = em.persist(new Line("Different_name", brandDummy2, Set.of()));
		Line lineFake3 = em.persist(new Line("Name with Blanks", brandDummy2, Set.of()));
		Product prodFake1 = new Product(lineFake1, "Matching", ProdType.WET, ProdPackage.BOX, 1500, null);
		prodFake1.set_stage(3); em.persist(prodFake1);
		Product prodFake2 = new Product(lineFake1, "Missing", ProdType.DRY, ProdPackage.BAG, 2000, null);
		prodFake2.set_stage(2); em.persist(prodFake2);
		Product prodFake3 = new Product(lineFake1, "Matching", ProdType.DRY, ProdPackage.CAN, 500, null);
		prodFake3.set_stage(1); em.persist(prodFake3);
		Product prodFake4 = new Product(lineFake2, "Matching", ProdType.DRY, ProdPackage.BOX, 700, null);
		prodFake4.set_stage(3); em.persist(prodFake4);
		Product prodFake5 = new Product(lineFake3, "Missing", ProdType.DRY, ProdPackage.TRAY, 300, null);
		prodFake5.set_stage(3); em.persist(prodFake5);
		
		Ingredient ing1 = em.persist(new Ingredient("ing1", Set.of()));
		Ingredient ing2 = em.persist(new Ingredient("ing2", Set.of()));
		Ingredient ing3 = em.persist(new Ingredient("ing3", Set.of()));
		Component comp1 = em.persist(new Component("comp1", Set.of()));
		Component comp2 = em.persist(new Component("comp2", Set.of()));
		Component comp3 = em.persist(new Component("comp3", Set.of()));
		em.persist(new Component("comp4", Set.of()));
		Component comp5 = em.persist(new Component("comp5", Set.of()));
		IngredientQuota iq1 = em.persist(new IngredientQuota(1, prodFake1, ing1, 70D, Set.of()));
		IngredientQuota iq2 = em.persist(new IngredientQuota(1, prodFake4, ing2, 20D, Set.of()));
		IngredientQuota iq3 = em.persist(new IngredientQuota(2, prodFake4, ing3, null, Set.of()));
		IngredientQuota iq4 = em.persist(new IngredientQuota(3, prodFake4, ing1, 30D, Set.of()));
		IngredientQuota iq5 = em.persist(new IngredientQuota(3, prodFake5, ing1, null, Set.of()));
		IngredientQuota iq6 = em.persist(new IngredientQuota(1, prodFake3, ing1, 50D, Set.of()));
		IngredientQuota iq7 = em.persist(new IngredientQuota(2, prodFake3, ing2, 50D, Set.of()));
		ComponentQuota cq1 = em.persist(new ComponentQuota(1, iq1, comp1, 70D));
		ComponentQuota cq2 = em.persist(new ComponentQuota(2, iq1, comp2, null));
		ComponentQuota cq3 = em.persist(new ComponentQuota(1, iq3, comp3, 30D));
		ComponentQuota cq4 = em.persist(new ComponentQuota(1, iq6, comp3, null));
		ComponentQuota cq5 = em.persist(new ComponentQuota(2, iq6, comp1, 50D));
		ComponentQuota cq6 = em.persist(new ComponentQuota(1, iq4, comp1, 60D));
		ComponentQuota cq7 = em.persist(new ComponentQuota(2, iq4, comp5, null));
		ComponentQuota cq8 = em.persist(new ComponentQuota(3, iq4, comp3, 20D));
		em.persist(new ComponentQuota(1, iq5, comp3, 20D));

		em.flush();
		em.clear();
		
		ArrayList<Product> fetchedProducts = new ArrayList<Product>();
		fetchedProducts
			.addAll(
				prodRepo.findAllSubselecting(
						ProductSpecs.getProductByMainTraitsSpec(null, null, null, "ing1", null, null, null, 500, 1500, null)
						)
			);

		assertThat(fetchedProducts).containsExactly(prodFake4, prodFake3, prodFake1);
		assertThat(fetchedProducts.get(0).getIngQuotasList()).containsExactly(iq2, iq3, iq4);
		assertThat(fetchedProducts.get(1).getIngQuotasList()).containsExactly(iq6, iq7);
		assertThat(fetchedProducts.get(2).getIngQuotasList()).containsExactly(iq1);
		Iterator<IngredientQuota> prod4iter = fetchedProducts.get(0).getIngQuotasList().iterator();
		assertThat(prod4iter.next().getCompQuotasList()).isEmpty();
		assertThat(prod4iter.next().getCompQuotasList()).containsExactly(cq3);
		assertThat(prod4iter.next().getCompQuotasList()).containsExactly(cq6, cq7, cq8);
		Iterator<IngredientQuota> prod3iter = fetchedProducts.get(1).getIngQuotasList().iterator();
		assertThat(prod3iter.next().getCompQuotasList()).containsExactly(cq4, cq5);
		assertThat(prod3iter.next().getCompQuotasList()).isEmpty();
		Iterator<IngredientQuota> prod1iter = fetchedProducts.get(2).getIngQuotasList().iterator();
		assertThat(prod1iter.next().getCompQuotasList()).containsExactly(cq1, cq2);
	}

}
