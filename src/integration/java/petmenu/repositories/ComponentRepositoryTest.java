package petmenu.repositories;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Set;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ActiveProfiles;

import petmenu.BaseContainerSetup;
import petmenu.config.datasources.LiquibaseConfig;
import petmenu.config.datasources.ProductsDataSourceConfig;
import petmenu.config.datasources.UsersDataSourceConfig;
import petmenu.entities.products.Brand;
import petmenu.entities.products.Component;
import petmenu.entities.products.ComponentQuota;
import petmenu.entities.products.Ingredient;
import petmenu.entities.products.IngredientQuota;
import petmenu.entities.products.Line;
import petmenu.entities.products.Product;
import petmenu.entities.products.Product.ProdPackage;
import petmenu.entities.products.Product.ProdType;
import petmenu.repositories.products.ComponentRepository;

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ActiveProfiles("integrationTest")
@Import({ ProductsDataSourceConfig.class, UsersDataSourceConfig.class, LiquibaseConfig.class })

public class ComponentRepositoryTest extends BaseContainerSetup {
	
	@Autowired
	private ComponentRepository compRepo;
	@Autowired
	private TestEntityManager em;


	@Test
	void success_find_component_by_partial_ingredient_order_by_name() {
		
		Brand brandDummy = em.persist(new Brand("brand_ dummy", Set.of()));
		Line lineDummy = em.persist(new Line("line_dummy", brandDummy, Set.of()));
		Product prodDummy = em.persist(new Product(lineDummy, "Third_because_package", ProdType.WET, ProdPackage.BAG, 800, null));
		
		Ingredient ing1 = em.persist(new Ingredient("ing1", Set.of()));
		Ingredient ing2 = em.persist(new Ingredient("ing2", Set.of()));
		Ingredient ing3 = em.persist(new Ingredient("ing3", Set.of()));
		Component comp1 = em.persist(new Component("comp1", Set.of()));
		Component comp2 = em.persist(new Component("comp2", Set.of()));
		Component comp3 = em.persist(new Component("comp3", Set.of()));
		Component comp4 = em.persist(new Component("comp4", Set.of()));
		Component comp5 = em.persist(new Component("comp5", Set.of()));
		IngredientQuota iq1 = em.persist(new IngredientQuota(1, prodDummy, ing1, 70D, Set.of()));
		em.persist(new IngredientQuota(1, prodDummy, ing2, 20D, Set.of()));
		IngredientQuota iq3 = em.persist(new IngredientQuota(2, prodDummy, ing3, null, Set.of()));
		IngredientQuota iq4 = em.persist(new IngredientQuota(3, prodDummy, ing1, 30D, Set.of()));
		IngredientQuota iq5 = em.persist(new IngredientQuota(3, prodDummy, ing1, null, Set.of()));
		IngredientQuota iq6 = em.persist(new IngredientQuota(1, prodDummy, ing1, 50D, Set.of()));
		em.persist(new IngredientQuota(2, prodDummy, ing2, 50D, Set.of()));
		em.persist(new ComponentQuota(1, iq1, comp1, 70D));
		em.persist(new ComponentQuota(2, iq1, comp2, null));
		em.persist(new ComponentQuota(1, iq3, comp3, 30D));
		em.persist(new ComponentQuota(1, iq6, comp3, null));
		em.persist(new ComponentQuota(2, iq6, comp4, 50D));
		em.persist(new ComponentQuota(1, iq4, comp1, 60D));
		em.persist(new ComponentQuota(2, iq4, comp5, null));
		em.persist(new ComponentQuota(3, iq4, comp3, 20D));
		em.persist(new ComponentQuota(1, iq5, comp3, 20D));

		em.flush();
		em.clear();
		
		assertThat(compRepo.findComponentByIngredient("g1")).containsExactly(comp1, comp2, comp3, comp4, comp5);
	}

}
