package petmenu.repositories;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Set;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ActiveProfiles;

import petmenu.BaseContainerSetup;
import petmenu.config.datasources.LiquibaseConfig;
import petmenu.config.datasources.ProductsDataSourceConfig;
import petmenu.config.datasources.UsersDataSourceConfig;
import petmenu.entities.products.Brand;
import petmenu.entities.products.Line;
import petmenu.repositories.products.BrandRepository;

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ActiveProfiles("integrationTest")
@Import({ ProductsDataSourceConfig.class, UsersDataSourceConfig.class, LiquibaseConfig.class })

public class BrandRepositoryTest extends BaseContainerSetup {

	@Autowired
	private BrandRepository brandRepo;
	@Autowired
	private TestEntityManager em;

	@Test
	void success_find_brand_by_id() {
		Brand brandFake = new Brand("Purina", Set.of());
		Integer brandFakeId = brandRepo.save(brandFake).getId();
		Brand fetchedBrand = brandRepo.findBrandById(brandFakeId);

		assertThat(fetchedBrand).isEqualTo(brandFake);
	}

	@Test
	void success_find_brand_by_name_case_insensitive_and_blank_chars() {
		Brand brandFake = brandRepo.save(new Brand("Name with Blanks", Set.of()));
		Brand fetchedBrand = brandRepo.findBrandByName("name With BLANKS");

		assertThat(fetchedBrand).isEqualTo(brandFake);
	}

	@Test
	void fail_find_brand_by_name() {
		brandRepo.save(new Brand("Name", Set.of()));
		Brand fetchedBrand = brandRepo.findBrandByName("Different Name");

		assertThat(fetchedBrand).isEqualTo(null);
	}
	
	@Test
	void success_find_brand_by_id_fetching_lines() {
		Brand brandFake1 = new Brand("Purina", Set.of());
		Brand brandFake2 = new Brand("Non_matching_brand", Set.of());
		Line lineFake1 = new Line("Line1", brandFake1, Set.of());
		Line lineFake2 = new Line("Line2", brandFake1, Set.of());
		Line lineFake3 = new Line("Line_from_non_matching_brand", brandFake2, Set.of());
		brandFake1.setLines(Set.of(lineFake1, lineFake2));
		brandFake2.setLines(Set.of(lineFake3));
		
		Integer brandFakeId = em.persistAndGetId(brandFake1, Integer.class);
		em.persist(brandFake2);

		em.flush();
		em.clear();
		
		Brand fetchedBrand = brandRepo.findBrandByIdFetchLines(brandFakeId);

		assertThat(fetchedBrand).isEqualTo(brandFake1);
		assertThat(fetchedBrand.getLines()).containsOnly(lineFake1, lineFake2);
	}
	
	@Test
	void success_find_brand_by_id_fetching_empty_lines() {
		Brand brandFake = new Brand("Purina", Set.of());
		
		Integer brandFakeId = em.persistAndGetId(brandFake, Integer.class);
		em.flush();
		em.clear();
		
		Brand fetchedBrand = brandRepo.findBrandByIdFetchLines(brandFakeId);

		assertThat(fetchedBrand).isEqualTo(fetchedBrand);
		assertThat(fetchedBrand.getLines()).isEmpty();
	}
	
	@Test
	void success_find_all_sorting_by_name_ascending() {
		Brand brandFake1 = em.persistFlushFind(new Brand("0_first", Set.of()));
		Brand brandFake2 = em.persistFlushFind(new Brand("A_second", Set.of()));
		Brand brandFake3 = em.persistFlushFind(new Brand("A_third", Set.of()));
		Brand brandFake4 = em.persistFlushFind(new Brand("g_fourth", Set.of()));
		Brand brandFake5 = em.persistFlushFind(new Brand("Z_last", Set.of()));

		em.clear();
		
		assertThat(brandRepo.findAllByOrderByNameAsc())
			.containsExactly(brandFake1, brandFake2, brandFake3, brandFake4, brandFake5);

	}


}
