package services;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.eq;

import java.util.Set;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import petmenu.entities.products.Brand;
import petmenu.repositories.products.BrandRepository;
import petmenu.services.products.BrandService;

@ExtendWith(MockitoExtension.class)
class BrandServiceTest {
	
	@Mock
	private BrandRepository brandRepo; 
	@InjectMocks
	private BrandService brandService;

	@Test
	void success_create_object_from_name_fetching_existing() {
		Brand brandDummy = new Brand("Purina", Set.of());
		when(brandRepo.findBrandByName("Purina")).thenReturn(brandDummy);
		
		assertThat(brandService.createFrom("Purina")).isEqualTo(brandDummy);
	}
	
	@Test
	void success_create_object_from_name_persisting_new_record() {
		Brand brandDummy = new Brand("Purina", Set.of());
		when(brandRepo.findBrandByName("Purina")).thenReturn(null);
		when(brandRepo.save(eq(brandDummy))).thenReturn(brandDummy);
		
		assertThat(brandService.createFrom("Purina")).isEqualTo(brandDummy);
		verify(brandRepo, times(1)).save(eq(brandDummy));
	}

}
