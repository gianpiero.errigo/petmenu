databaseChangeLog {
  changeSet(id: '''1652480848153-1''', author: '''javier (generated)''') {
    createSequence(cycle: false, incrementBy: 1, maxValue: 9223372036854775806, minValue: 1, sequenceName: '''hibernate_sequence''', startValue: 1)
  }

  changeSet(id: '''1652480848153-2''', author: '''javier (generated)''') {
    createTable(tableName: '''brand''') {
      column(name: '''id''', type: '''INT''') {
        constraints(nullable: false, primaryKey: true)
      }
      column(name: '''name''', type: '''VARCHAR(255)''')
    }
  }

  changeSet(id: '''1652480848153-3''', author: '''javier (generated)''') {
    createTable(tableName: '''component''') {
      column(name: '''id''', type: '''INT''') {
        constraints(nullable: false, primaryKey: true)
      }
      column(name: '''name''', type: '''VARCHAR(255)''')
    }
  }

  changeSet(id: '''1652480848153-4''', author: '''javier (generated)''') {
    createTable(tableName: '''component_quota''') {
      column(name: '''id''', type: '''BINARY(16)''') {
        constraints(nullable: false, primaryKey: true)
      }
      column(name: '''comp_perc''', type: '''DOUBLE''', defaultValueComputed: '''NULL''')
      column(name: '''comp''', type: '''INT''', defaultValueComputed: '''NULL''')
      column(name: '''iq''', type: '''BINARY(16)''', defaultValueComputed: '''NULL''')
      column(name: '''cq_position''', type: '''INT''', defaultValueComputed: '''NULL''')
    }
  }

  changeSet(id: '''1652480848153-5''', author: '''javier (generated)''') {
    createTable(tableName: '''ingredient''') {
      column(name: '''id''', type: '''INT''') {
        constraints(nullable: false, primaryKey: true)
      }
      column(name: '''name''', type: '''VARCHAR(255)''')
    }
  }

  changeSet(id: '''1652480848153-6''', author: '''javier (generated)''') {
    createTable(tableName: '''ingredient_quota''') {
      column(name: '''id''', type: '''BINARY(16)''') {
        constraints(nullable: false, primaryKey: true)
      }
      column(name: '''ing_perc''', type: '''DOUBLE''', defaultValueComputed: '''NULL''')
      column(name: '''ing''', type: '''INT''', defaultValueComputed: '''NULL''')
      column(name: '''product''', type: '''INT''', defaultValueComputed: '''NULL''')
      column(name: '''iq_position''', type: '''INT''', defaultValueComputed: '''NULL''')
    }
  }

  changeSet(id: '''1652480848153-7''', author: '''javier (generated)''') {
    createTable(tableName: '''line''') {
      column(name: '''id''', type: '''INT''') {
        constraints(nullable: false, primaryKey: true)
      }
      column(name: '''name''', type: '''VARCHAR(255)''')
      column(name: '''brand''', type: '''INT''', defaultValueComputed: '''NULL''')
    }
  }

  changeSet(id: '''1652480848153-8''', author: '''javier (generated)''') {
    createTable(tableName: '''product''') {
      column(name: '''id''', type: '''INT''') {
        constraints(nullable: false, primaryKey: true)
      }
      column(name: '''_stage''', type: '''INT''', defaultValueComputed: '''NULL''')
      column(name: '''calcium''', type: '''DOUBLE''', defaultValueComputed: '''NULL''')
      column(name: '''carbs''', type: '''DOUBLE''', defaultValueComputed: '''NULL''')
      column(name: '''energy''', type: '''DOUBLE''', defaultValueComputed: '''NULL''')
      column(name: '''has_back_img''', type: '''BIT''', defaultValueBoolean: false)
      column(name: '''has_details_one_img''', type: '''BIT''', defaultValueBoolean: false)
      column(name: '''has_details_two_img''', type: '''BIT''', defaultValueBoolean: false)
      column(name: '''has_front_img''', type: '''BIT''', defaultValueBoolean: false)
      column(name: '''moisture''', type: '''DOUBLE''', defaultValueComputed: '''NULL''')
      column(name: '''net_weight''', type: '''INT''', defaultValueComputed: '''NULL''')
      column(name: '''prod_package''', type: '''VARCHAR(255)''')
      column(name: '''phosphorus''', type: '''DOUBLE''', defaultValueComputed: '''NULL''')
      column(name: '''raw_ashes''', type: '''DOUBLE''', defaultValueComputed: '''NULL''')
      column(name: '''raw_fibre''', type: '''DOUBLE''', defaultValueComputed: '''NULL''')
      column(name: '''raw_oil_fat''', type: '''DOUBLE''', defaultValueComputed: '''NULL''')
      column(name: '''raw_protein''', type: '''DOUBLE''', defaultValueComputed: '''NULL''')
      column(name: '''recipe''', type: '''VARCHAR(255)''')
      column(name: '''sodium''', type: '''DOUBLE''', defaultValueComputed: '''NULL''')
      column(name: '''prod_type''', type: '''VARCHAR(255)''')
      column(name: '''line''', type: '''INT''', defaultValueComputed: '''NULL''')
    }
  }

  changeSet(id: '''1652480848153-9''', author: '''javier (generated)''') {
    createIndex(indexName: '''FK4s80tptbuo69hqvgud5s2k99b''', tableName: '''ingredient_quota''') {
      column(defaultValueComputed: '''NULL''', name: '''ing''')
    }
  }

  changeSet(id: '''1652480848153-10''', author: '''javier (generated)''') {
    createIndex(indexName: '''FK4skbjypat3h2cpf8w11krf2qe''', tableName: '''ingredient_quota''') {
      column(defaultValueComputed: '''NULL''', name: '''product''')
    }
  }

  changeSet(id: '''1652480848153-11''', author: '''javier (generated)''') {
    createIndex(indexName: '''FK94wtyg33eiwnrgip6t3ardmek''', tableName: '''product''') {
      column(defaultValueComputed: '''NULL''', name: '''line''')
    }
  }

  changeSet(id: '''1652480848153-12''', author: '''javier (generated)''') {
    createIndex(indexName: '''FKdq56k7gq69lxjpk21eadvvrv6''', tableName: '''component_quota''') {
      column(defaultValueComputed: '''NULL''', name: '''comp''')
    }
  }

  changeSet(id: '''1652480848153-13''', author: '''javier (generated)''') {
    createIndex(indexName: '''FKdrwsi541endx81wug8lqldm1j''', tableName: '''component_quota''') {
      column(defaultValueComputed: '''NULL''', name: '''iq''')
    }
  }

  changeSet(id: '''1652480848153-14''', author: '''javier (generated)''') {
    createIndex(indexName: '''FKwr1h3hduf34dgw8wy8cfk8mi''', tableName: '''line''') {
      column(defaultValueComputed: '''NULL''', name: '''brand''')
    }
  }

  changeSet(id: '''1652480848153-15''', author: '''javier (generated)''') {
    addForeignKeyConstraint(baseColumnNames: '''ing''', baseTableName: '''ingredient_quota''', constraintName: '''FK4s80tptbuo69hqvgud5s2k99b''', deferrable: false, initiallyDeferred: false, onDelete: '''RESTRICT''', onUpdate: '''RESTRICT''', referencedColumnNames: '''id''', referencedTableName: '''ingredient''', validate: true)
  }

  changeSet(id: '''1652480848153-16''', author: '''javier (generated)''') {
    addForeignKeyConstraint(baseColumnNames: '''product''', baseTableName: '''ingredient_quota''', constraintName: '''FK4skbjypat3h2cpf8w11krf2qe''', deferrable: false, initiallyDeferred: false, onDelete: '''RESTRICT''', onUpdate: '''RESTRICT''', referencedColumnNames: '''id''', referencedTableName: '''product''', validate: true)
  }

  changeSet(id: '''1652480848153-17''', author: '''javier (generated)''') {
    addForeignKeyConstraint(baseColumnNames: '''line''', baseTableName: '''product''', constraintName: '''FK94wtyg33eiwnrgip6t3ardmek''', deferrable: false, initiallyDeferred: false, onDelete: '''RESTRICT''', onUpdate: '''RESTRICT''', referencedColumnNames: '''id''', referencedTableName: '''line''', validate: true)
  }

  changeSet(id: '''1652480848153-18''', author: '''javier (generated)''') {
    addForeignKeyConstraint(baseColumnNames: '''comp''', baseTableName: '''component_quota''', constraintName: '''FKdq56k7gq69lxjpk21eadvvrv6''', deferrable: false, initiallyDeferred: false, onDelete: '''RESTRICT''', onUpdate: '''RESTRICT''', referencedColumnNames: '''id''', referencedTableName: '''component''', validate: true)
  }

  changeSet(id: '''1652480848153-19''', author: '''javier (generated)''') {
    addForeignKeyConstraint(baseColumnNames: '''iq''', baseTableName: '''component_quota''', constraintName: '''FKdrwsi541endx81wug8lqldm1j''', deferrable: false, initiallyDeferred: false, onDelete: '''RESTRICT''', onUpdate: '''RESTRICT''', referencedColumnNames: '''id''', referencedTableName: '''ingredient_quota''', validate: true)
  }

  changeSet(id: '''1652480848153-20''', author: '''javier (generated)''') {
    addForeignKeyConstraint(baseColumnNames: '''brand''', baseTableName: '''line''', constraintName: '''FKwr1h3hduf34dgw8wy8cfk8mi''', deferrable: false, initiallyDeferred: false, onDelete: '''RESTRICT''', onUpdate: '''RESTRICT''', referencedColumnNames: '''id''', referencedTableName: '''brand''', validate: true)
  }

}
