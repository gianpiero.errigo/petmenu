databaseChangeLog {
  changeSet(id: '''1652480851013-1''', author: '''javier (generated)''') {
    createSequence(cycle: false, incrementBy: 1, maxValue: 9223372036854775806, minValue: 1, sequenceName: '''hibernate_sequence''', startValue: 1)
  }

  changeSet(id: '''1652480851013-2''', author: '''javier (generated)''') {
    createTable(tableName: '''user''') {
      column(name: '''id''', type: '''VARCHAR(255)''') {
        constraints(nullable: false, primaryKey: true)
      }
      column(name: '''kc_id''', type: '''VARCHAR(255)''')
      column(name: '''name''', type: '''VARCHAR(255)''')
      column(name: '''last_commit''', type: '''datetime''', defaultValueComputed: '''NULL''')
      column(name: '''edit_count''', type: '''INT''', defaultValueComputed: '''NULL''')
      column(name: '''new_count''', type: '''INT''', defaultValueComputed: '''NULL''')
    }
  }

  changeSet(id: '''1652480851013-3''', author: '''javier (generated)''') {
    createTable(tableName: '''user_commit''') {
      column(name: '''id''', type: '''INT''') {
        constraints(nullable: false, primaryKey: true)
      }
      column(name: '''uctype''', type: '''INT''', defaultValueComputed: '''NULL''')
      column(name: '''prod_id''', type: '''INT''', defaultValueComputed: '''NULL''')
      column(name: '''stage''', type: '''INT''', defaultValueComputed: '''NULL''')
      column(name: '''time''', type: '''datetime''', defaultValueComputed: '''NULL''')
      column(name: '''user''', type: '''VARCHAR(255)''')
    }
  }

  changeSet(id: '''1652480851013-4''', author: '''javier (generated)''') {
    createIndex(indexName: '''FK1w8gtw0xg2xvkolxqcdn3cypq''', tableName: '''user_commit''') {
      column(name: '''user''')
    }
  }

  changeSet(id: '''1652480851013-5''', author: '''javier (generated)''') {
    addForeignKeyConstraint(baseColumnNames: '''user''', baseTableName: '''user_commit''', constraintName: '''FK1w8gtw0xg2xvkolxqcdn3cypq''', deferrable: false, initiallyDeferred: false, onDelete: '''RESTRICT''', onUpdate: '''RESTRICT''', referencedColumnNames: '''id''', referencedTableName: '''user''', validate: true)
  }

}
