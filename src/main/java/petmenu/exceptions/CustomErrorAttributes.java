package petmenu.exceptions;

import java.util.Map;

import org.springframework.boot.web.error.ErrorAttributeOptions;
import org.springframework.boot.web.servlet.error.DefaultErrorAttributes;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.WebRequest;


/**
 * Customize default SpringBoot {@link ErrorController} response
 * adding a {@code detail} entry
 * populated with parent exception, if any
 * 
 * @author javier
 */
@Component
public class CustomErrorAttributes extends DefaultErrorAttributes {

   	@Override
    public Map<String, Object> getErrorAttributes(WebRequest request, ErrorAttributeOptions options) {
        
   		Map<String, Object> errorAttributes = super.getErrorAttributes(request, options);
   		
   		Throwable throwable = getError(request);
   		
   		if (throwable != null) {
   			Throwable cause = throwable.getCause();
        
            if (cause != null)
            	errorAttributes.put("detail", cause.getMessage());
        
   		}
   		
        return errorAttributes;
    }

}
