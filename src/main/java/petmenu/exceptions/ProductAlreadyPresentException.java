package petmenu.exceptions;

public class ProductAlreadyPresentException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public ProductAlreadyPresentException() {
        super();
    }
    public ProductAlreadyPresentException(String message, Throwable cause) {
        super(message, cause);
    }
    public ProductAlreadyPresentException(String message) {
        super(message);
    }
    public ProductAlreadyPresentException(Throwable cause) {
        super(cause);
    }
}
