package petmenu.config.datasources;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.zaxxer.hikari.HikariDataSource;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = "petmenu.repositories.products",
        entityManagerFactoryRef = "productsEntityManagerFactory",
        transactionManagerRef= "productsTransactionManager"
		)

public class ProductsDataSourceConfig {
	
    @Bean
    @Primary
    @ConfigurationProperties("petmenu.datasource.products")
    public DataSourceProperties productsDataSourceProperties() {
        return new DataSourceProperties();
    }
 
    @Bean
    @Primary
    @ConfigurationProperties("petmenu.datasource.products.configuration")
    public DataSource productsDataSource() {
        return productsDataSourceProperties()
        		.initializeDataSourceBuilder()
                .type(HikariDataSource.class).build();
    }
    
    @Primary
    @Bean
    public LocalContainerEntityManagerFactoryBean productsEntityManagerFactory(EntityManagerFactoryBuilder builder) {
        
    	Map<String, Object> properties = new HashMap<String, Object>();
        properties.put("hibernate.hbm2ddl.auto", "validate");
        
    	return builder
    			.dataSource(productsDataSource())
    			.packages("petmenu.entities.products")
                .properties(properties)
                .build();
    }
    
    @Primary
    @Bean
    public PlatformTransactionManager productsTransactionManager(
    		final @Qualifier("productsEntityManagerFactory") LocalContainerEntityManagerFactoryBean productsEntityManagerFactory) {
        return new JpaTransactionManager(productsEntityManagerFactory.getObject());
    }

}
