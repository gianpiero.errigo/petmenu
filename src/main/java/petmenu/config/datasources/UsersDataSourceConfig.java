package petmenu.config.datasources;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.zaxxer.hikari.HikariDataSource;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = "petmenu.repositories.users",
        entityManagerFactoryRef = "usersEntityManagerFactory",
        transactionManagerRef= "usersTransactionManager"
		)

public class UsersDataSourceConfig {
	
    @Bean
    @ConfigurationProperties("petmenu.datasource.users")
    public DataSourceProperties usersDataSourceProperties() {
        return new DataSourceProperties();
    }
 
    @Bean
    @ConfigurationProperties("petmenu.datasource.users.configuration")
    public DataSource usersDataSource() {
        return usersDataSourceProperties()
        		.initializeDataSourceBuilder()
                .type(HikariDataSource.class).build();
    }
    
    @Bean
    public LocalContainerEntityManagerFactoryBean usersEntityManagerFactory(EntityManagerFactoryBuilder builder) {
    	
    	Map<String, Object> properties = new HashMap<String, Object>();
        properties.put("hibernate.hbm2ddl.auto", "validate");
    	
        return builder
                .dataSource(usersDataSource())
                .packages("petmenu.entities.users")
                .properties(properties)
                .build();
    }
    
    @Bean
    public PlatformTransactionManager usersTransactionManager(
    		final @Qualifier("usersEntityManagerFactory") LocalContainerEntityManagerFactoryBean usersEntityManagerFactory) {
        return new JpaTransactionManager(usersEntityManagerFactory.getObject());
    }

}
