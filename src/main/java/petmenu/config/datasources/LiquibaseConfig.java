package petmenu.config.datasources;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import liquibase.integration.spring.SpringLiquibase;

@Configuration
public class LiquibaseConfig {
	
	@Bean
	public SpringLiquibase productsLiquibase(
			DataSource productsDataSource,
			@Value("${petmenu.liquibase.products.changelog}") String changelog) {
	   
		SpringLiquibase liquibase = new SpringLiquibase();
	    liquibase.setChangeLog(changelog);
	    liquibase.setDataSource(productsDataSource);
	    return liquibase;
	}
	
	@Bean
	public SpringLiquibase usersLiquibase(
			@Qualifier("usersDataSource") DataSource usersDataSource,
			@Value("${petmenu.liquibase.users.changelog}") String changelog) {
	    
		SpringLiquibase liquibase = new SpringLiquibase();
	    liquibase.setChangeLog(changelog);
	    liquibase.setDataSource(usersDataSource);
	    return liquibase;
	}

}
