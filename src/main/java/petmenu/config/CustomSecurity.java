package petmenu.config;

import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.stereotype.Component;

@Component
public class CustomSecurity {
	
	public boolean hasUserId(@AuthenticationPrincipal Jwt principal, String userId) {
        return principal.getClaim("https://petmenu.eu/URISafeSub").equals(userId);
    }
}
