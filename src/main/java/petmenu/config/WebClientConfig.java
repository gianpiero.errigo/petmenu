package petmenu.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.core.convert.converter.Converter;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientManager;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientProvider;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientProviderBuilder;
import org.springframework.security.oauth2.client.endpoint.DefaultClientCredentialsTokenResponseClient;
import org.springframework.security.oauth2.client.endpoint.OAuth2ClientCredentialsGrantRequest;
import org.springframework.security.oauth2.client.endpoint.OAuth2ClientCredentialsGrantRequestEntityConverter;
import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository;
import org.springframework.security.oauth2.client.web.DefaultOAuth2AuthorizedClientManager;
import org.springframework.security.oauth2.client.web.OAuth2AuthorizedClientRepository;
import org.springframework.security.oauth2.client.web.reactive.function.client.ServletOAuth2AuthorizedClientExchangeFilterFunction;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.reactive.function.client.WebClient;


@Component
public class WebClientConfig {
	
	@Value("${petmenu.auth0.management.audience}")
    private String audience;

	@Bean
	public WebClient authClient(OAuth2AuthorizedClientManager authorizedClientManager) {
		ServletOAuth2AuthorizedClientExchangeFilterFunction oauth2 = 
			new ServletOAuth2AuthorizedClientExchangeFilterFunction(authorizedClientManager);
	    oauth2.setDefaultClientRegistrationId("auth0_management");
	    return WebClient.builder()
	      .filter(oauth2)
	      .build();
	}
	
	@Bean
	OAuth2AuthorizedClientManager auth0AuthorizedClientManager(
			ClientRegistrationRepository clientRegistrationRepository,
			OAuth2AuthorizedClientRepository authorizedClientRepository) {
		
		Converter<OAuth2ClientCredentialsGrantRequest, MultiValueMap<String, String>> addAudienceToBodyConverter = 
				(req) -> {
		
			MultiValueMap<String, String> audEntry = new LinkedMultiValueMap<String, String>();
			audEntry.add("audience", audience);
			return audEntry;
		};
		
		OAuth2ClientCredentialsGrantRequestEntityConverter auth0RequestEntityConverter = 
				new OAuth2ClientCredentialsGrantRequestEntityConverter();
		auth0RequestEntityConverter.addParametersConverter(addAudienceToBodyConverter);
		
		DefaultClientCredentialsTokenResponseClient auth0ClientCredentialsTokenResponseClient = 
				new DefaultClientCredentialsTokenResponseClient();
		auth0ClientCredentialsTokenResponseClient.setRequestEntityConverter(auth0RequestEntityConverter);
		
		OAuth2AuthorizedClientProvider auth0AuthorizedClientProvider =
				OAuth2AuthorizedClientProviderBuilder.builder()
					.refreshToken()
					.clientCredentials(
							configurer -> configurer.accessTokenResponseClient(auth0ClientCredentialsTokenResponseClient)
					)
					.build();
		
		DefaultOAuth2AuthorizedClientManager auth0AuthorizedClientManager = 
				new DefaultOAuth2AuthorizedClientManager(
						clientRegistrationRepository,
						authorizedClientRepository
				);
		
		auth0AuthorizedClientManager.setAuthorizedClientProvider(auth0AuthorizedClientProvider);

		return auth0AuthorizedClientManager;
	}
}
