package petmenu.repositories.products;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import petmenu.entities.products.Component;

public interface ComponentRepository extends CrudRepository<Component, Integer>{

	@Query("SELECT c FROM Component c WHERE c.name =:name")
	Component findComponentByName(@Param("name")String name);
	
	@Query("SELECT c FROM Component c WHERE c.id =:id")
	Component findComponentById(@Param("id")Integer id);

	@Query("SELECT c FROM Component c LEFT JOIN FETCH c.compQuotas WHERE c.id =:id")
	Component findComponentByIdFetchCQList(@Param("id")Integer id);

	@Query("SELECT c FROM Component c LEFT JOIN FETCH c.compQuotas WHERE c.name =:name")
	Component findComponentByNameFetchCQList(@Param("name")String name);

	List<Component> findAllByOrderByNameAsc();
	
	@Query("SELECT c FROM Component c WHERE EXISTS "
			+ "(SELECT cq FROM ComponentQuota cq LEFT JOIN cq.iq iq LEFT JOIN iq.ing ing WHERE ing.name LIKE %:partialIngName%)"
			+ "ORDER BY c.name ASC")
	List<Component> findComponentByIngredient(@Param("partialIngName")String partialIngName);

}
