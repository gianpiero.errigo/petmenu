package petmenu.repositories.products;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import petmenu.entities.products.Brand;
import petmenu.entities.products.Line;

public interface LineRepository extends CrudRepository<Line, Integer> {
	
	@Query("SELECT l FROM Line l WHERE l.id = :id")
	Line findLineById(@Param("id")Integer id);
	
	@Query("SELECT l FROM Line l WHERE l.name = :name")
	List<Line> findLineByName(@Param("name")String name);
	
	@Query("SELECT l FROM Line l WHERE l.brand = :brand")
	List<Line> findLineByBrand(@Param("brand")Brand brand);
	
	@Query("SELECT l FROM Line l LEFT JOIN l.brand b WHERE l.name =:name AND b.name = :brandName")
	Line findLineByNameAndBrandName(@Param("name")String name, @Param("brandName")String brandName);

	@Query("SELECT l FROM Line l LEFT JOIN FETCH l.products WHERE l.id = :id")
	Line findLineByIdFetchProducts(@Param("id")Integer id);

}
