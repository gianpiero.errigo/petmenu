package petmenu.repositories.products;

import java.util.UUID;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import petmenu.entities.products.IngredientQuota;

public interface IngredientQuotaRepository extends CrudRepository<IngredientQuota, UUID>{
	
	@Query("SELECT iq FROM IngredientQuota iq LEFT JOIN FETCH iq.compQuotasList WHERE iq.id =:id")
	IngredientQuota findIngredientQuotaByIdFetchCQList(@Param("id") UUID id);

	@Query("SELECT iq FROM IngredientQuota iq WHERE iq.id =:uuid")
	IngredientQuota findIngredientQuotaById(@Param("uuid") UUID uuid);

}
