package petmenu.repositories.products;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import petmenu.entities.products.Brand;

public interface BrandRepository extends CrudRepository<Brand, Integer> {
	
	@Query("SELECT b FROM Brand b WHERE b.id =:id")
	Brand findBrandById(@Param("id")Integer id);
	
	@Query("SELECT b FROM Brand b WHERE b.name =:name")
	Brand findBrandByName(@Param("name")String name);
	
	@Query("SELECT b FROM Brand b LEFT JOIN FETCH b.lines WHERE b.id =:id")
	Brand findBrandByIdFetchLines(@Param("id")Integer id);
	
	List<Brand> findAllByOrderByNameAsc();
	
}
