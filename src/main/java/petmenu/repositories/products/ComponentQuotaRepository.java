package petmenu.repositories.products;

import java.util.UUID;

import org.springframework.data.repository.CrudRepository;

import petmenu.entities.products.ComponentQuota;

public interface ComponentQuotaRepository extends CrudRepository<ComponentQuota, UUID>{
	
	
}
