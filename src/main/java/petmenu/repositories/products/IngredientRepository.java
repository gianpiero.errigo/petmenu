package petmenu.repositories.products;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import petmenu.entities.products.Ingredient;

public interface IngredientRepository extends CrudRepository<Ingredient, Integer> {
	
	@Query("SELECT i FROM Ingredient i WHERE i.name =:name")
	Ingredient findIngredientByName(@Param("name")String name);
	
	@Query("SELECT i FROM Ingredient i WHERE i.id =:id")
	Ingredient findIngredientById(@Param("id")Integer id);

	@Query("SELECT i FROM Ingredient i LEFT JOIN FETCH i.ingQuotas WHERE i.id =:id")
	Ingredient findIngredientByIdFetchIQList(@Param("id")Integer id);

	@Query("SELECT i FROM Ingredient i LEFT JOIN FETCH i.ingQuotas WHERE i.name =:name")
	Ingredient findIngredientByNameFetchIQList(@Param("name")String name);

	List<Ingredient> findAllByOrderByNameAsc();
	
}
