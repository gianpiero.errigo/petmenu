package petmenu.repositories.products;

import java.util.ArrayList;
import java.util.LinkedList;

import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;

import org.springframework.data.jpa.domain.Specification;

import petmenu.entities.products.Component_;
import petmenu.entities.products.IngredientQuota;
import petmenu.entities.products.Ingredient_;
import petmenu.entities.products.Line;
import petmenu.entities.products.Line_;
import petmenu.entities.products.Brand;
import petmenu.entities.products.Brand_;
import petmenu.entities.products.ComponentQuota;
import petmenu.entities.products.Product_;
import petmenu.entities.products.Product;
import petmenu.entities.products.Product.ProdPackage;
import petmenu.entities.products.Product.ProdType;
import petmenu.entities.products.ComponentQuota_;
import petmenu.entities.products.IngredientQuota_;

public class ProductSpecs {
		
	public static Specification<Product> getProductByMainTraitsSpec(
				String brand, String line, String recipe, String ing, String comp,
				String type, String packaging, Integer minWeight, Integer maxWeight, Integer stage) {

		return (root, query, builder) -> {

			root
				.fetch(Product_.line)
				.fetch(Line_.brand);
		
			ArrayList<Predicate> predicates = new ArrayList<>();
			LinkedList<Order> orderCriterias = new LinkedList<>();

			Join<Product, Line> lineJoin = root.join(Product_.line, JoinType.INNER);
			Join<Line, Brand> brandJoin = lineJoin.join(Line_.brand, JoinType.INNER);
//			
//			SetJoin<Product, IngredientQuota> iqJoin = root.join(Product_.ingQuotasList, JoinType.LEFT);
//			Join<IngredientQuota, Ingredient> ingJoin = iqJoin.join(IngredientQuota_.ing, JoinType.LEFT);
//			
//			SetJoin<IngredientQuota, ComponentQuota> cqJoin = iqJoin.join(IngredientQuota_.compQuotasList, JoinType.LEFT);
//			Join<ComponentQuota, Component> compJoin = cqJoin.join(ComponentQuota_.comp, JoinType.LEFT);
//
			orderCriterias.add(builder.asc(brandJoin.get(Brand_.NAME)));
			orderCriterias.add(builder.asc(lineJoin.get(Line_.NAME)));
			orderCriterias.add(builder.asc(root.get(Product_.PROD_TYPE)));
			orderCriterias.add(builder.asc(root.get(Product_.PROD_PACKAGE)));
			orderCriterias.add(builder.asc(root.get(Product_.NET_WEIGHT)));


			if (brand != null) {
				predicates.add(builder.equal(brandJoin.get(Brand_.NAME), brand));
			
				if (line != null) {
					predicates.add(builder.equal(lineJoin.get(Line_.NAME), line));
					
					if (recipe != null) {
						predicates.add(builder.equal(root.get(Product_.RECIPE), recipe));
					} 
				} 
			}
			
			if (ing != null) {
				
				Subquery<Integer> ingSubQuery = query.subquery(Integer.class);
				Root<IngredientQuota> ingSubRoot = ingSubQuery.from(IngredientQuota.class);
				
				ArrayList<Predicate> ingPredicates = new ArrayList<>();
				ingPredicates.add(builder.equal(ingSubRoot.get(IngredientQuota_.product), root.get(Product_.ID)));
				ingPredicates.add(builder.like(ingSubRoot.join(IngredientQuota_.ing).get(Ingredient_.NAME), "%"+ing+"%"));
			
				if (comp != null) {
					Subquery<Integer> compSubQuery = ingSubQuery.subquery(Integer.class);
					Root<ComponentQuota> compSubRoot = compSubQuery.from(ComponentQuota.class);
					
					compSubQuery
						.select(builder.literal(1))
						.where(
							builder.equal(compSubRoot.get(ComponentQuota_.iq), ingSubRoot.get(IngredientQuota_.ID)),
							builder.like(compSubRoot.join(ComponentQuota_.comp).get(Component_.NAME), "%"+comp+"%")
						);
					
					ingPredicates.add(builder.exists(compSubQuery));
				}
				
				ingSubQuery
					.select(builder.literal(1))
					.where(builder.and(ingPredicates.toArray(new Predicate[ingPredicates.size()])));
				
				predicates.add(builder.exists(ingSubQuery));

			}
			
			if (type != null) {
				predicates.add(builder.equal(root.get(Product_.PROD_TYPE), ProdType.valueOf(type)));
			} 
			
			if (packaging != null) {
				predicates.add(builder.equal(root.get(Product_.PROD_PACKAGE), ProdPackage.valueOf(packaging)));
			} 
			
			if (minWeight != null && maxWeight != null) {
				predicates.add(builder.between(root.get(Product_.NET_WEIGHT), minWeight, maxWeight));
			} else if (minWeight != null) {
				predicates.add(builder.ge(root.get(Product_.NET_WEIGHT), minWeight));
			} else if (maxWeight != null) {
				predicates.add(builder.le(root.get(Product_.NET_WEIGHT), maxWeight));
			}
			
			if (stage != null) {
				predicates.add(builder.equal(root.get(Product_._STAGE), stage));
			} 
			
			query.orderBy(orderCriterias);
			
			return builder.and(predicates.toArray(new Predicate[predicates.size()]));
			
		};
		
	}
	
	
}
