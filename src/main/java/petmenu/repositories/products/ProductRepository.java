package petmenu.repositories.products;

import java.util.List;

import javax.persistence.QueryHint;

import org.hibernate.Hibernate;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import petmenu.entities.products.Line;
import petmenu.entities.products.Product;
import petmenu.entities.products.Product.ProdPackage;
import petmenu.entities.products.Product.ProdType;

public interface ProductRepository extends CrudRepository<Product, Integer>, JpaSpecificationExecutor<Product> {
	
	@EntityGraph("fullFetchProduct")
	@Query("SELECT p FROM Product p WHERE p.id=:id")
	@QueryHints(@QueryHint(name = org.hibernate.annotations.QueryHints.PASS_DISTINCT_THROUGH, value = "false"))
	Product findProductByIdFullFetch(@Param("id") Integer id);
	
	@Query("SELECT p FROM Product p WHERE p.line=:line AND p.recipe=:recipe AND p.prodPackage=:prodPackage AND p.prodType=:prodType AND p.netWeight=:netWeight")
	Product findProductByFsDto(	@Param("line") Line line,
								@Param("recipe") String recipe,
								@Param("prodPackage") ProdPackage enumPackaging,
								@Param("prodType") ProdType enumType,
								@Param("netWeight") Integer netWeight );
	
	@Query("SELECT p FROM Product p WHERE p.id=:id")
	Product findProductById(@Param("id") Integer id);
	
	@Query("SELECT p FROM Product p WHERE p.recipe=:recipe AND p.line=:line")
	List<Product> findProductByLineAndRecipe(@Param("line") Line line, @Param("recipe") String recipe);

	@EntityGraph("fullFetchProduct")
	@Query("SELECT p FROM Product p WHERE p._stage=:_stage ORDER BY p.line.brand.name ASC, p.line.name ASC, p.recipe ASC, p.netWeight DESC")
	List<Product> findProductByStageFullFetch(@Param("_stage") Integer stage);
	
	@Query("SELECT p.recipe FROM Product p LEFT JOIN p.line l LEFT JOIN l.brand b WHERE l.name=:line AND b.name=:brand")
	List<String> findRecipeByLine(@Param("brand") String brand, @Param("line") String line);

	default List<Product> findAllSubselecting(Specification<Product> spec) {
		
		List<Product> results = findAll(spec);
		results
			.forEach(p -> p.getIngQuotasList()
				.forEach(iq -> Hibernate.initialize(iq.getCompQuotasList()))
			);
		return results;
						
	}

}
