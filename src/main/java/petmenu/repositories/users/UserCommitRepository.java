package petmenu.repositories.users;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import petmenu.entities.users.UserCommit;

public interface UserCommitRepository extends CrudRepository<UserCommit, Integer> {

	@Query("SELECT uc FROM UserCommit uc WHERE uc.id =:id")
	UserCommit findUserCommitById(@Param("id")Integer id);
	
}
