package petmenu.repositories.users;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import petmenu.entities.users.User;

public interface UserRepository extends CrudRepository<User, String> {

	@Query("SELECT u FROM User u WHERE u.id =:id")
	User findUserById(@Param("id")String id);
	
	@Query("SELECT u FROM User u LEFT JOIN FETCH u.userCommitList uc WHERE u.id =:id")
	User findUserByIdFetchingUserCommitList(@Param("id")String id);

	User findByName(String name);
		
	@Query("SELECT u FROM User u LEFT JOIN FETCH u.userCommitList uc WHERE u.name =:name")
	User findByNameFetchingUserCommitList(@Param("name")String name);
	
	List<User> findAll();

	
	

}
