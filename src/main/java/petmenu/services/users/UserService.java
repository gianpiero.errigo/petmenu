package petmenu.services.users;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import petmenu.entities.users.User;
import petmenu.repositories.users.UserRepository;

@Transactional
@Service
public class UserService {
	
	@Autowired
	private UserRepository userRepo;
	
	public User createFrom(Jwt principal) {
		
		User user = findUserById(principal.getClaim("https://petmenu.eu/URISafeSub"));
		
		if(user == null) {
			user = new User();
			user.setId(principal.getClaim("https://petmenu.eu/URISafeSub"));
			user.setName(principal.getClaim("https://petmenu.eu/nickname"));
			userRepo.save(user);
		}
		
		return user;
		
	}
	
	public User findUserById(String id) {
		return userRepo.findUserById(id);
	}
	
	public User findUserByIdFetchingUserCommitList(String id) {
		return userRepo.findUserByIdFetchingUserCommitList(id);
	}
	
	public List<User> findAll() {
		return userRepo.findAll();
	}

	public User findByNameFetchingUserCommitList(String userName) {
		return userRepo.findByNameFetchingUserCommitList(userName);
	}

}
