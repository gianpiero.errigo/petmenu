package petmenu.services.users;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import petmenu.entities.users.User;
import petmenu.entities.users.UserCommit;
import petmenu.repositories.users.UserCommitRepository;

@Transactional("usersTransactionManager")
@Service
public class UserCommitService {
	
	@Autowired
	private UserService userService;
	@Autowired
	private UserCommitRepository ucRepo;

	
	public UserCommit createFrom(User user, Integer prodId, String ucType, Integer stage) {
			
		UserCommit uc = new UserCommit();
		
		user = userService.findUserByIdFetchingUserCommitList(user.getId());
		uc.setUser(user);
		uc.getUser().addUserCommit(uc);
		uc.setProdId(prodId);
		uc.setUCType(UserCommit.UCType.valueOf(ucType));
		uc.setStage(stage);
		uc.setTime(LocalDateTime.now());
		
		user.setLastCommit(uc.getTime());
		if(ucType == "NEW")
			user.setNewCount(user.getNewCount()+1);
		else if(ucType == "EDIT")
			user.setEditCount(user.getEditCount()+1);
		
		ucRepo.save(uc);
		
		return uc;
	}

}
