package petmenu.services.auth;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

@Service
public class AuthManagementService {
	
    @Value("${spring.security.oauth2.resourceserver.jwt.issuer-uri}")
    private String issuer;
	
    @Autowired
	private WebClient authClient;		
    
	public String changeName(String newName, String userId) {

	    return authClient.patch()
	      .uri("https://petmenu-dev.eu.auth0.com/api/v2/users/"+userId)
	      .contentType(MediaType.APPLICATION_JSON)
	      .bodyValue("{\"user_metadata\":{\"petmenu_username\":\""+newName+"\"}}")
	      .retrieve()
	      .bodyToMono(String.class)
	      .block();
	}
}
