package petmenu.services.products;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import petmenu.entities.products.Brand;
import petmenu.entities.products.Line;
import petmenu.repositories.products.LineRepository;

@Transactional
@Service
public class LineService {
	
	private final LineRepository lineRepo;
	private final BrandService brandService;

		
	public LineService(LineRepository lineRepo, BrandService brandService) {
		super();
		this.lineRepo = lineRepo;
		this.brandService = brandService;
	}

	/**
	 * Creates a {@link Line} entity trying to fetch by name.<br>
	 * If element doesn't exist in DB, persists a new one.
	 * 
	 * @param name name of the entity passed as a string.
	 * @param brand {@link Brand} the entity belongs to.
	 * @return Line fetched or persisted.
	 */
	public Line createFrom(String name, String brand) {
		
		Line line = lineRepo.findLineByNameAndBrandName(name, brand);
		
		if (line == null) {
			
			line = new Line();
			line.setName(name);
			brandService.createFrom(brand).addLine(line);
		} 
		
		return line;
	}
	
	/**
	 * Remove {@link Line} deleting corresponding parent
	 * {@link Brand} if not referenced by any other child.
	 * 
	 * @param line Line to remove
	 */
	public void delete(Line line) {
		Brand brand = line.getBrand();
		brand.rmLine(line);
		
		if (brand.getLines().isEmpty()) {
			brandService.delete(brand);
		}
		else {
			lineRepo.delete(line);
		}
	}
		
	public Iterable<Line> list() {
		return lineRepo.findAll();
	}
	
	public Line findLineById(Integer id) {
		return lineRepo.findLineById(id);
	}
	
	public List<Line> findByName(String name) {
		return lineRepo.findLineByName(name);
	}
	
	public List<Line> findByBrand(Brand brand) {
		return lineRepo.findLineByBrand(brand);
	}
	
	public Line findByNameAndBrandName(String name, String brand) {
		return lineRepo.findLineByNameAndBrandName(name, brand);
	}

	public Line findLineByIdFetchProducts(Integer id) {
		return lineRepo.findLineByIdFetchProducts(id);
	}

	public Line save(Line line) {
		return lineRepo.save(line);		
	}

}
