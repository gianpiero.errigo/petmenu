package petmenu.services.products;

import java.util.Iterator;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import petmenu.dtos.products.ComponentQuotaDTO;
import petmenu.entities.products.Component;
import petmenu.entities.products.ComponentQuota;
import petmenu.entities.products.IngredientQuota;
import petmenu.repositories.products.ComponentQuotaRepository;

@Transactional
@Service
public class ComponentQuotaService {
	
	private final ComponentQuotaRepository cqRepo;
	private final ComponentService compService;
	
	
	public ComponentQuotaService(ComponentQuotaRepository cqRepo, ComponentService compService) {
		super();
		this.cqRepo = cqRepo;
		this.compService = compService;
	}

	/**
	 * Creates and persists a {@link ComponentQuota}.<br>
	 * 
	 * @param iq {@link IngredientQuota} the entity belongs to.
	 * @param cqDto {@link ComponentQuotaDTO} representing the entity.
	 * @param index position of the ComponentQuota
	 * inside {@link IngredientQuota#compQuotasList compQuotasList} of the specified IngredientQuota.
	 * @return persisted ComponentQuota.
	 */
	public ComponentQuota createFrom(IngredientQuota iq, ComponentQuotaDTO cqDto, Integer index) {
		
		Component comp = compService.createFrom(cqDto.getComponent());
		
		ComponentQuota cq = new ComponentQuota();
		cq.setCompPerc(cqDto.getCompPerc());
		cq.setCqPosition(index);
		iq.addComponentQuota(cq);
		comp.addComponentQuota(cq);
		
		return cq;
	}
	
	public void delete(ComponentQuota cq) {
		
		Component oldComp = cq.getComp();
		
		cq.getIq().rmComponentQuota(cq);
		cq.getComp().rmComponentQuota(cq);

		if (oldComp.getCompQuotas().isEmpty()) 
			compService.delete(oldComp);
	}

	/**
	 * Deletes all {@link ComponentQuota}s of a specific {@link IngredientQuota},
	 * deleting their {@link Component} too, if not referenced by any other entity.</br>
	 * 
	 * @param iq IngredientQuota the ComponentQuotas belong to.
	 */
	public void deleteListFrom(IngredientQuota iq) {
		
		Iterator<ComponentQuota> cqListIter = iq.getCompQuotasList().iterator();
		
		while (cqListIter.hasNext()) {
			
			ComponentQuota cq = cqListIter.next();
			Component comp = compService.findComponentById(cq.getComp().getId());

			comp.rmComponentQuota(cq);
			cq.setIq(null);
			
			cqListIter.remove();
			
			if (comp.getCompQuotas().isEmpty())
				compService.delete(comp);			
		}
	}
	
	public Iterable<ComponentQuota> findAll() {
		return cqRepo.findAll();
	}
	
	public ComponentQuota save(ComponentQuota cq) {
		return cqRepo.save(cq);
	}

}
