package petmenu.services.products;

import java.awt.Color;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Comparator;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import net.coobird.thumbnailator.Thumbnails;
import net.coobird.thumbnailator.filters.Canvas;
import net.coobird.thumbnailator.geometry.Positions;
import petmenu.entities.products.Product;

@Service
public class UploadImageService {

	private static final String imagesDir = "/mnt/dati/prova/petmenu/imgs/";

	/**
	 * Resize the image passed as {@link MultipartFile} in three different resolutions 
	 * by <a href="https://github.com/coobird/thumbnailator" target="_top">https://github.com/coobird/thumbnailator</a> 
	 * and save them inside {@value #imagesDir}{id},
	 * where {id} is the id of the {@link Product} the image refers to.
	 * 
	 * @param prodId id of the Product the image refers to. 
	 * @param file the original image uploaded as MultipartFile.
	 * @return true only if the image has been saved in every size.
	 */
	public Boolean uploadImgFor(Integer prodId, MultipartFile file) {
		
		if (file == null)
			return false;
		
		byte[] bytes;
		Path dirPath = Path.of(imagesDir + prodId + "/");
		Path filePath = dirPath.resolve(file.getName());
		
		Color bgColor = new Color(143, 178, 218);
		
		try {
			bytes = file.getBytes();
			Files.createDirectories(dirPath);
			Files.write(filePath, bytes);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			Thumbnails.of((filePath).toFile())
			.size(240, 320)
			.addFilter(new Canvas(240, 320, Positions.CENTER, bgColor))
			.toFile(Path.of(filePath.toString()+"_sm.jpg").toFile());
			
			Thumbnails.of((filePath).toFile())
			.size(480, 640)
			.addFilter(new Canvas(480, 640, Positions.CENTER, bgColor))
			.toFile(Path.of(filePath.toString()+"_md.jpg").toFile());
			
			Thumbnails.of((filePath).toFile())
			.size(1024, 1280)
			.addFilter(new Canvas(1024, 1280, Positions.CENTER, bgColor))
			.toFile(Path.of(filePath.toString()+"_lg.jpg").toFile());
			
			Files.delete(filePath);
		
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return true;

	}
	
	/**
	 * Deletes every image and backup existent for the specified {@link Product} 
	 * together with their parent directory.
	 * @param prodId id of the Product the images to be deleted refer to.
	 * @return true if every image, backup and their parent directory have been deleted
	 */
	public Boolean deleteImgsFrom(Integer prodId) {
		
		Path dirPath = Path.of(imagesDir + prodId + "/");
		
		try {
			Files.walk(dirPath)
				.sorted(Comparator.reverseOrder())
				.forEach(t -> {
					try {
						Files.delete(t);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				});
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return true;
	}

	/**
	 * Change the images assigned to a specific slot of a {@link Product}, 
	 * saving the existing ones as backups appending <i>.prev</i> suffix to their name
	 * replacing any previous backup, and calling {@link #uploadImg}
	 * for the new file provided.
	 * 
	 * @param slot which one of the four available slots the image refers to.
	 * @param prodId id of the Product the image refers to. 
	 * @param file the original image uploaded as {@link MultipartFile}.
	 * @return true only if new image has been saved in every size, along with backups if any.
	 */
	public Boolean changeImgFor(String slot, Integer prodId, MultipartFile file) {

		String dir = imagesDir + prodId + "/";
		
		try {
			if (Files.exists(Paths.get(dir + slot + "_sm.jpg")))
				Files.move(Paths.get(dir + slot + "_sm.jpg"), Paths.get(dir + slot + "_sm.jpg.prev"), StandardCopyOption.REPLACE_EXISTING);
			if (Files.exists(Paths.get(dir + slot + "_md.jpg")))
				Files.move(Paths.get(dir + slot + "_md.jpg"), Paths.get(dir + slot + "_md.jpg.prev"), StandardCopyOption.REPLACE_EXISTING);
			if (Files.exists(Paths.get(dir + slot + "_lg.jpg")))
				Files.move(Paths.get(dir + slot + "_lg.jpg"), Paths.get(dir + slot + "_lg.jpg.prev"), StandardCopyOption.REPLACE_EXISTING);
		} catch (IOException e) {
			e.printStackTrace();
		}

		return uploadImgFor(prodId, file);

	}


}
