package petmenu.services.products;

import org.springframework.stereotype.Service;

import petmenu.dtos.products.ComponentQuotaDTO;
import petmenu.dtos.products.FirstStageDTO;
import petmenu.dtos.products.IngredientQuotaDTO;
import petmenu.dtos.products.ProductDTO;
import petmenu.dtos.products.SecondStageDTO;
import petmenu.dtos.products.ThirdStageDTO;
import petmenu.entities.products.ComponentQuota;
import petmenu.entities.products.IngredientQuota;
import petmenu.entities.products.Product;

@Service
public class StageMapperService {
	
	public FirstStageDTO fsDtoFrom(Product prod) {
		
		FirstStageDTO fsDto = new FirstStageDTO();
		
		fsDto.setBrand(prod.getLine().getBrand().getName());
		fsDto.setLine(prod.getLine().getName());
		fsDto.setRecipe(prod.getRecipe());
		fsDto.setProdType(prod.getProdType().toString());
		fsDto.setProdPackage(prod.getProdPackage().toString());
		fsDto.setNetWeight(prod.getNetWeight());
		
		return fsDto;
	}

	public SecondStageDTO ssDtoFrom(Product prod) {
		
		SecondStageDTO ssDto = new SecondStageDTO();
		
		ssDto.setFrontImg(prod.getHasFrontImg());
		ssDto.setBackImg(prod.getHasBackImg());
		ssDto.setDetailsOneImg(prod.getHasDetailsOneImg());
		ssDto.setDetailsTwoImg(prod.getHasDetailsTwoImg());
		
		return ssDto;
	}
	
	public ThirdStageDTO tsDtoFrom(Product prod) {
		
		ThirdStageDTO tsDto = new ThirdStageDTO();
		
		if(!prod.getIngQuotasList().isEmpty()) {
			
			for (IngredientQuota iq: prod.getIngQuotasList()) {
				
				tsDto.addIngredientQuotaDto(iqDtoFrom(iq));
			}
		}
		
		tsDto.getAnalysis().put("rawProtein", prod.getRawProtein());
		tsDto.getAnalysis().put("rawOilFat", prod.getRawOilFat());
		tsDto.getAnalysis().put("rawFibre", prod.getRawFibre());
		tsDto.getAnalysis().put("rawAshes", prod.getRawAshes());
		tsDto.getAnalysis().put("moisture", prod.getMoisture());
		tsDto.getAnalysis().put("energy", prod.getEnergy());
		tsDto.getAnalysis().put("calcium", prod.getCalcium());
		tsDto.getAnalysis().put("sodium", prod.getSodium());
		tsDto.getAnalysis().put("phosphorus", prod.getPhosphorus());
		
		return tsDto;
	}
	
	public ProductDTO productDtoFrom(Product prod) {
		
		ProductDTO prodDto = new ProductDTO();
		
		prodDto.setId(prod.getId().toString());
		prodDto.set_stage(prod.get_stage().toString());
		prodDto.setFsDto(fsDtoFrom(prod));
		prodDto.setSsDto(ssDtoFrom(prod));
		prodDto.setTsDto(tsDtoFrom(prod));
		
		return prodDto;
	}
	
	public IngredientQuotaDTO iqDtoFrom(IngredientQuota iq) {
		
		IngredientQuotaDTO iqDto = new IngredientQuotaDTO();
		
		iqDto.setIngredient(iq.getIng().getName());
		iqDto.setIngPerc(iq.getIngPerc());
		
		if (!iq.getCompQuotasList().isEmpty()) {
			
			for (ComponentQuota cq: iq.getCompQuotasList()) {
				
				iqDto.addComponentQuotaDto(cqDtoFrom(cq));
			}
		}
		
		return iqDto;
	}
	
	public ComponentQuotaDTO cqDtoFrom(ComponentQuota cq) {
		
		ComponentQuotaDTO cqDto = new ComponentQuotaDTO();
		
		cqDto.setComponent(cq.getComp().getName());
		cqDto.setCompPerc(cq.getCompPerc());
		
		return cqDto;
	}
	
}
