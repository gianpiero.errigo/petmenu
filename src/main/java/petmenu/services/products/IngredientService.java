package petmenu.services.products;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import petmenu.entities.products.Ingredient;
import petmenu.repositories.products.IngredientRepository;

@Transactional
@Service
public class IngredientService {
	
	private final IngredientRepository ingRepo;
	

	public IngredientService(IngredientRepository ingRepo) {
		super();
		this.ingRepo = ingRepo;
	}

	/**
	 * Creates a {@link Ingredient} entity trying to fetch by name.<br>
	 * If element doesn't exist in DB, persists a new one.
	 * 
	 * @param name name of the Ingredient passed as a string.
	 * @return Ingredient fetched or persisted
	 */
	public Ingredient createFrom(String name) {
		
		Ingredient ing = ingRepo.findIngredientByName(name);
		if (ing == null) {
			ing = new Ingredient(); 
			ing.setName(name);
			ing = ingRepo.save(ing);
		}
		
		return ing;
	}
	
	public List<Ingredient> listByName() {
		return ingRepo.findAllByOrderByNameAsc();
	}
	
	public Ingredient findIngredientById(Integer id) {
		return ingRepo.findIngredientById(id);
	}
	
	public Ingredient findIngredientByIdFetchIQList(Integer id) {
		return ingRepo.findIngredientByIdFetchIQList(id);
	}
	
	public Ingredient findIngredientByNameFetchIQList(String name) {
		return ingRepo.findIngredientByNameFetchIQList(name);
	}

	public void delete(Ingredient ing) {
		ingRepo.delete(ing);
	}
		
}
