package petmenu.services.products;

import java.util.Iterator;
import java.util.UUID;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import petmenu.dtos.products.ComponentQuotaDTO;
import petmenu.dtos.products.IngredientQuotaDTO;
import petmenu.entities.products.Ingredient;
import petmenu.entities.products.IngredientQuota;
import petmenu.entities.products.Product;
import petmenu.repositories.products.IngredientQuotaRepository;

@Transactional
@Service
public class IngredientQuotaService {
	
	private final IngredientQuotaRepository iqRepo;
	private final IngredientService ingService;
	private final ComponentQuotaService cqService;

	
	public IngredientQuotaService(IngredientQuotaRepository iqRepo, IngredientService ingService,
			ComponentQuotaService cqService) {
		super();
		this.iqRepo = iqRepo;
		this.ingService = ingService;
		this.cqService = cqService;
	}

	/**
	 * Creates and persists an {@link IngredientQuota}.<br>
	 * Fetches or creates parent {@link Ingredient} through {@link IngredientService#createFrom(String name)}.<br>
	 * If the passed argument {@link IngredientQuotaDTO}<br>
	 * contains any {@link ComponentQuotaDTO},<br>
	 * a call to
	 * {@link ComponentQuotaService#createFrom(UUID iqId, ComponentQuotaDTO cqDto) createFrom(UUID, ComponentQuotaDTO)}<br>
	 * gets issued for each one.
	 * 
	 * @param prod {@link Product} the entity belongs to.
	 * @param iqDto IngredientQuotaDTO representing the entity.
	 * @param index position of the IngredientQuota
	 * inside {@link Product#ingQuotasList ingQuotasList} of the specified Product.
	 * @return persisted IngredientQuota.
	 */
	public IngredientQuota createFrom(Product prod, IngredientQuotaDTO iqDto, Integer index) {
		
		Ingredient ing = ingService.createFrom(iqDto.getIngredient());
		
		IngredientQuota iq = new IngredientQuota();
		ing.addIngredientQuota(iq);
		iq.setIngPerc(iqDto.getIngPerc());
		iq.setIqPosition(index);
		prod.addIngredientQuota(iq);

		if (!iqDto.getCompQuotasList().isEmpty()) {
			for (int i=0; i < iqDto.getCompQuotasList().size(); i++) {
				
				ComponentQuotaDTO cqDto = iqDto.getCompQuotasList().get(i);
				cqService.createFrom(iq, cqDto, i);
			};
			
		}

		return iq;
	}
	
	public void delete(IngredientQuota iq, Product fromProd) {
		
		cqService.deleteListFrom(iq);
				
		Ingredient ing = ingService.findIngredientById(iq.getIng().getId());

		fromProd.rmIngredientQuota(iq);
		ing.rmIngredientQuota(iq);

		if (ing.getIngQuotas().isEmpty())
			ingService.delete(ing);
	}
	
	/**
	 * Deletes all {@link IngredientQuota}s of a specific {@link Product},
	 * deleting their {@link Ingredient} too, if not referenced by any other entity.</br>
	 * Calls {@link ComponentQuotaService#deleteListFrom(IngredientQuota)} for every element of the list.
	 * 
	 * @param prodId id of the Product the IngredientQuotas belong to.
	 */
	public void deleteListFrom(Product prod) {
		
		Iterator<IngredientQuota> iqListIter = prod.getIngQuotasList().iterator();
		while (iqListIter.hasNext()) {
			
			IngredientQuota iq = iqRepo.findIngredientQuotaByIdFetchCQList(iqListIter.next().getId());
			
			cqService.deleteListFrom(iq);
			
			Ingredient ing = ingService.findIngredientById(iq.getIng().getId());

			ing.rmIngredientQuota(iq);
			iq.setProduct(null);
			
			iqListIter.remove();
			
			if (ing.getIngQuotas().isEmpty())
				ingService.delete(ing);			
		}
	}
	
	public IngredientQuota findIngredientQuotaByIdFetchCQList(UUID id) {
		return iqRepo.findIngredientQuotaByIdFetchCQList(id);
	}
	
	public Iterable<IngredientQuota> findAll() {
		return iqRepo.findAll();
	}
	
	public IngredientQuota save(IngredientQuota iq) {
		return iqRepo.save(iq);
	}

}
