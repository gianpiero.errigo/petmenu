package petmenu.services.products;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import petmenu.dtos.products.FirstStageDTO;
import petmenu.dtos.products.IngredientQuotaDTO;
import petmenu.dtos.products.ThirdStageDTO;
import petmenu.entities.products.Line;
import petmenu.entities.products.Product;
import petmenu.entities.products.Product.ProdPackage;
import petmenu.entities.products.Product.ProdType;
import petmenu.exceptions.ProductAlreadyPresentException;
import petmenu.repositories.products.ProductRepository;
import petmenu.repositories.products.ProductSpecs;

@Transactional
@Service
public class ProductService {

	private final ProductRepository prodRepo;
	private final LineService lineService;
	private final IngredientQuotaService iqService;
	private final UploadImageService uiService;

	
	public ProductService(ProductRepository prodRepo, LineService lineService, BrandService brandService,
			IngredientQuotaService iqService, UploadImageService uiService) {
		super();
		this.prodRepo = prodRepo;
		this.lineService = lineService;
		this.iqService = iqService;
		this.uiService = uiService;
	}

	/**
	 * Creates a {@link Product} entity trying to fetch by identification params.<br>
	 * If parent {@link Line} doesn't exist in DB, persists a new one.
	 * 
	 * @param fsDto {@link FirstStageDTO} representing the entity.
	 * @return persisted Product.
	 */
	public Product createFrom(FirstStageDTO fsDto) throws ProductAlreadyPresentException {

		Product product = findProductByFsDto(fsDto);
		
		if (product != null)
			throw new ProductAlreadyPresentException(product.getId().toString());

		product = new Product();
		lineService.createFrom(fsDto.getLine(), fsDto.getBrand()).addProduct(product);
		
		product.setProdPackage(ProdPackage.valueOf(fsDto.getProdPackage()));
		product.setProdType(ProdType.valueOf(fsDto.getProdType()));
		product.setRecipe(fsDto.getRecipe());
		product.setNetWeight(fsDto.getNetWeight());
		product.set_stage(1);

		return product;
	}

	/**
	 * Update identification parameters of the {@link Product},<br>
	 * checking they will not conflict with the ones
	 * from any already existing Product.<br>
	 * If new parent {@link Line} is different from the old one<br>
	 * remove the Product from the latter<br>
	 * and call {@link LineService#delete} if not referenced by any other Product.<br>
	 * If new parent Line doesn't exist in DB persist a new one.<br>
	 * 
	 * 
	 * @param id    id of the entity to update.
	 * @param fsDto {@link FirstStageDTO} representing the entity.
	 * @return the updated Product
	 * @throws ProductAlreadyPresentException if after the updates<br>
	 * the entity would overlap with already existing one.
	 */
	public Product updateFrom(Integer id, FirstStageDTO fsDto) throws ProductAlreadyPresentException {

		Product product = findProductByFsDto(fsDto);

		if (product != null)
			throw new ProductAlreadyPresentException(product.getId().toString());

		product = prodRepo.findProductById(id);
		Line oldLine = product.getLine();
		Line newLine = lineService.createFrom(fsDto.getLine(), fsDto.getBrand());

		if (!oldLine.equals(newLine)) {
		
			oldLine.rmProduct(product);
			newLine.addProduct(product);
			lineService.save(newLine);
			
			if (oldLine.getProducts().isEmpty())
				lineService.delete(oldLine);
		}
		
		product.setProdPackage(ProdPackage.valueOf(fsDto.getProdPackage()));
		product.setProdType(ProdType.valueOf(fsDto.getProdType()));
		product.setRecipe(fsDto.getRecipe());
		product.setNetWeight(fsDto.getNetWeight());

		return product;
	}

	/**
	 * Remove {@link Product} deleting corresponding parent {@link Line} if not
	 * referenced by any other child calling {@link LineService#delete}.
	 * 
	 * @param prodId id of the Product to remove
	 */
	public void delete(Integer prodId) {
		
		Product prod = prodRepo.findProductByIdFullFetch(prodId);
		Line line = lineService.findLineById(prod.getLine().getId());
		
		if(prod.get_stage() > 2)
			iqService.deleteListFrom(prod);
		
		if(prod.get_stage() > 1)
			uiService.deleteImgsFrom(prodId);

		line.rmProduct(prod);
		if (line.getProducts().isEmpty())
			lineService.delete(line);
	}
	
	public void setSecondStage(Integer prodId, 
			MultipartFile frontImg,
			MultipartFile backImg,
			MultipartFile detailsOneImg,
			MultipartFile detailsTwoImg) throws ProductAlreadyPresentException {
		
		Product prod = prodRepo.findProductById(prodId);
		
		if (prod.get_stage() > 1) {
			throw new ProductAlreadyPresentException();
		}

		prod.setHasFrontImg(uiService.uploadImgFor(prodId, frontImg));
		
		prod.setHasBackImg(uiService.uploadImgFor(prodId, backImg));

		prod.setHasDetailsOneImg(uiService.uploadImgFor(prodId, detailsOneImg));

		prod.setHasDetailsTwoImg(uiService.uploadImgFor(prodId, detailsTwoImg));
	
		prod.set_stage(2);
	}
	
	public void updateSecondStage(Integer prodId, 
			MultipartFile frontImg,
			MultipartFile backImg,
			MultipartFile detailsOneImg,
			MultipartFile detailsTwoImg) {
		
		Product prod = prodRepo.findProductById(prodId);

		prod.setHasFrontImg(uiService.changeImgFor("frontImg", prodId, frontImg));
	
		prod.setHasBackImg(uiService.changeImgFor("backImg", prodId, backImg));

		prod.setHasDetailsOneImg(uiService.changeImgFor("detailsOneImg", prodId, detailsOneImg));

		prod.setHasDetailsTwoImg(uiService.changeImgFor("detailsTwoImg", prodId, detailsTwoImg));
	}
	
	public void setThirdStage(Integer prodId, ThirdStageDTO thirdStageDTO) throws ProductAlreadyPresentException {
		
		Product prod = prodRepo.findProductByIdFullFetch(prodId);
		
		if (prod.get_stage() > 2) {
			throw new ProductAlreadyPresentException();
		}
		
		for (int i=0; i < thirdStageDTO.getIngQuotasList().size(); i++) {
			
			IngredientQuotaDTO iqDto = thirdStageDTO.getIngQuotasList().get(i);
			iqService.createFrom(prod, iqDto, i);
		};
		
		thirdStageDTO.analysisToProduct(prod);
		prod.set_stage(3);
	}
	
	public void updateThirdStage(Integer prodId, ThirdStageDTO thirdStageDTO) {
		
		Product prod = prodRepo.findProductByIdFullFetch(prodId);
		
		iqService.deleteListFrom(prod);
		
		for (int i=0; i < thirdStageDTO.getIngQuotasList().size(); i++) {
			
			IngredientQuotaDTO iqDto = thirdStageDTO.getIngQuotasList().get(i);
			iqService.createFrom(prod, iqDto, i);
		};
		
		thirdStageDTO.analysisToProduct(prod);
		prod.set_stage(3);
	}

	public Product save(Product prod) {
		return prodRepo.save(prod);
	}
	
	public Iterable<Product> list() {
		return prodRepo.findAll();
	}

	public Product findProductById(Integer id) {
		return prodRepo.findProductById(id);
	}

	public Product findProductByIdFullFetch(Integer id) {
		return prodRepo.findProductByIdFullFetch(id);
	}

	private Product findProductByFsDto(FirstStageDTO fsDto) {

		return prodRepo.findProductByFsDto(
			lineService.findByNameAndBrandName(fsDto.getLine(), fsDto.getBrand()),
			fsDto.getRecipe(), ProdPackage.valueOf(fsDto.getProdPackage()), ProdType.valueOf(fsDto.getProdType()),
			fsDto.getNetWeight());
	}

	public List<Product> findProductByStageFullFetch(Integer stage) {
		return prodRepo.findProductByStageFullFetch(stage);
	}
	
	public List<String> findRecipeByLine(String brand, String line) {
		return prodRepo.findRecipeByLine(brand, line);
	}

	// Specifications methods

	public List<Product> findProductByMainTraitsSpec(String brand, String line, String recipe, String ing, String comp, String type, String packaging, Integer minWeight, Integer maxWeight, Integer stage) {
		return prodRepo.findAllSubselecting(ProductSpecs.getProductByMainTraitsSpec(brand, line, recipe, ing, comp, type, packaging, minWeight, maxWeight, stage));
	}
	
}
