package petmenu.services.products;


import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import petmenu.entities.products.Component;
import petmenu.repositories.products.ComponentRepository;

@Transactional
@Service
public class ComponentService {
	
	private final ComponentRepository compRepo;
	
	
	public ComponentService(ComponentRepository compRepo) {
		super();
		this.compRepo = compRepo;
	}

	/**
	 * Creates a {@link Component} entity trying to fetch by name.<br>
	 * If element doesn't exist in DB, persists a new one.
	 * 
	 * @param name name of the Component passed as a string.
	 * @return Component fetched or persisted.
	 */
	public Component createFrom(String name) {
			
		Component comp = compRepo.findComponentByNameFetchCQList(name);
		if (comp == null) {
			comp = new Component(); 
			comp.setName(name);
			comp = compRepo.save(comp);
		}
		
		return comp;
	}
	
	public List<Component> listByName() {
		return compRepo.findAllByOrderByNameAsc();
	}
	
	public void delete (Component comp) {
		compRepo.delete(comp);
	}

	public Component findComponentByNameFetchCQList(String name) {
		return compRepo.findComponentByNameFetchCQList(name);
	}

	public Component findComponentById(Integer id) {
		return compRepo.findComponentById(id);
	}
	
	public Component findComponentByIdFetchCQList(Integer id) {
		return compRepo.findComponentByIdFetchCQList(id);
	}

}
