package petmenu.services.products;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import petmenu.entities.products.Brand;
import petmenu.repositories.products.BrandRepository;

@Transactional
@Service
public class BrandService {
	
	private final BrandRepository brandRepo;

	
	public BrandService(BrandRepository brandRepo) {
		super();
		this.brandRepo = brandRepo;
	}

	/**
	 * Creates a {@link Brand} entity trying to fetch by name.<br>
	 * If element doesn't exist in DB, persists a new one.
	 * 
	 * @param name name of the entity passed as a string.
	 * @return Brand fetched or persisted
	 */
	public Brand createFrom(String name) {
		
		Brand brand = brandRepo.findBrandByName(name);
		
		if (brand == null) {
		
			brand = new Brand();
			brand.setName(name);
			brandRepo.save(brand);
		}
		
		return brand;
	}
	
	public void delete(Brand brand) {
		brandRepo.delete(brand);
	}
	
	public Iterable<Brand> list() {
		return brandRepo.findAll();
	}
	
	public List<Brand> listByName() {
		return brandRepo.findAllByOrderByNameAsc();
	}
	
	public Brand findByName(String name) {
		return brandRepo.findBrandByName(name);
	}
	
	public Brand findById(Integer id) {
		return brandRepo.findBrandById(id);
	}
	
	public Boolean exists(Integer id) {
		return brandRepo.existsById(id);
	}

	public Brand findBrandByIdFetchLines(Integer id) {
		return brandRepo.findBrandByIdFetchLines(id);
	}

}
