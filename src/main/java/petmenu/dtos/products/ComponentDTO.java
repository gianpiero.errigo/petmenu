package petmenu.dtos.products;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


public class ComponentDTO {
	
	private String name;
	
	private List<ComponentQuotaDTO> compQuotasList = new ArrayList<ComponentQuotaDTO>();
	
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public List<ComponentQuotaDTO> getCompQuotasList() {
		return compQuotasList;
	}

	public void setCompQuotasList(List<ComponentQuotaDTO> compQuotasList) {
		this.compQuotasList = compQuotasList;
	}
	
	public Boolean addIngredientQuotaDto(ComponentQuotaDTO cqDto) {
		return this.getCompQuotasList().add(cqDto);
	}

	@Override
	public int hashCode() {
		return Objects.hash(compQuotasList, name);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ComponentDTO other = (ComponentDTO) obj;
		return Objects.equals(compQuotasList, other.compQuotasList) && Objects.equals(name, other.name);
	}
	

}
