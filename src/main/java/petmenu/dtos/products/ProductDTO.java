package petmenu.dtos.products;

import java.util.Objects;

public class ProductDTO {

	private String id;
	
	private String _stage;
	
	private FirstStageDTO fsDto;
	
	private SecondStageDTO ssDto;
	
	private ThirdStageDTO tsDto;


	public ProductDTO() {}

	public ProductDTO(String id, String _stage, FirstStageDTO fsDto, SecondStageDTO ssDto, ThirdStageDTO tsDto) {
		super();
		this.id = id;
		this._stage = _stage;
		this.fsDto = fsDto;
		this.ssDto = ssDto;
		this.tsDto = tsDto;
	}
	
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String get_stage() {
		return _stage;
	}

	public void set_stage(String _stage) {
		this._stage = _stage;
	}

	public FirstStageDTO getFsDto() {
		return fsDto;
	}

	public void setFsDto(FirstStageDTO fsDto) {
		this.fsDto = fsDto;
	}

	public SecondStageDTO getSsDto() {
		return ssDto;
	}

	public void setSsDto(SecondStageDTO ssDto) {
		this.ssDto = ssDto;
	}

	public ThirdStageDTO getTsDto() {
		return tsDto;
	}

	public void setTsDto(ThirdStageDTO tsDto) {
		this.tsDto = tsDto;
	}

	@Override
	public int hashCode() {
		return Objects.hash(_stage, fsDto, id, ssDto, tsDto);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProductDTO other = (ProductDTO) obj;
		return Objects.equals(_stage, other._stage) && Objects.equals(fsDto, other.fsDto)
				&& Objects.equals(id, other.id) && Objects.equals(ssDto, other.ssDto)
				&& Objects.equals(tsDto, other.tsDto);
	}
	
}
