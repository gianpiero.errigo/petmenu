package petmenu.dtos.products;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotBlank;

import petmenu.entities.products.Ingredient;
import petmenu.entities.products.IngredientQuota;

public class IngredientQuotaDTO {
	
	private String ingredient;
	
	private Double ingPerc;
	
	private List<ComponentQuotaDTO> compQuotasList = new ArrayList<ComponentQuotaDTO>();


	public IngredientQuotaDTO() {}

	public IngredientQuotaDTO(String ingredient, Double ingPerc, List<ComponentQuotaDTO> compQuotasList) {
		super();
		this.ingredient = ingredient;
		this.ingPerc = ingPerc;
		this.compQuotasList = compQuotasList;
	}
	

	@NotBlank
	public String getIngredient() {
		return ingredient;
	}

	public void setIngredient(String ingredient) {
		this.ingredient = ingredient;
	}

	@Max(100)
	public Double getIngPerc() {
		return ingPerc;
	}

	public void setIngPerc(Double ingPerc) {
		this.ingPerc = ingPerc;
	}
	
	public List<@Valid ComponentQuotaDTO> getCompQuotasList() {
		return compQuotasList;
	}
	
	public void setCompQuotasList(List<ComponentQuotaDTO> compQuotasList) {
		this.compQuotasList = compQuotasList;
	}
	
	public Boolean addComponentQuotaDto(ComponentQuotaDTO cqDto) {
		return this.getCompQuotasList().add(cqDto);
	}

		
	public IngredientQuota toIngredientQuota() {
		
		IngredientQuota iq = new IngredientQuota();
		
		iq.setIng(new Ingredient());
		iq.getIng().setName(ingredient);
		iq.setIngPerc(ingPerc);
		
		return iq;
	}

	@Override
	public int hashCode() {
		return Objects.hash(compQuotasList, ingPerc, ingredient);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		IngredientQuotaDTO other = (IngredientQuotaDTO) obj;
		return Objects.equals(compQuotasList, other.compQuotasList) && Objects.equals(ingPerc, other.ingPerc)
				&& Objects.equals(ingredient, other.ingredient);
	}
	

}
