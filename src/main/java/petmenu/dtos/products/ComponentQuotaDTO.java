package petmenu.dtos.products;

import java.util.Objects;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotBlank;

import petmenu.entities.products.ComponentQuota;
import petmenu.entities.products.IngredientQuota;

public class ComponentQuotaDTO {

	private String component;
	
	private Double compPerc;
	
	
	public ComponentQuotaDTO() {}
	
	public ComponentQuotaDTO(String component, Double compPerc) {
		super();
		this.component = component;
		this.compPerc = compPerc;
	}
	

	@NotBlank
	public String getComponent() {
		return component;
	}

	public void setComponent(String component) {
		this.component = component;
	}
	
	@Max(100)
	public Double getCompPerc() {
		return compPerc;
	}

	public void setCompPerc(Double compPerc) {
		this.compPerc = compPerc;
	}
	
	public ComponentQuota toComponentQuota(IngredientQuota iq) {
		
		ComponentQuota cq = new ComponentQuota();
		cq.setIq(iq);
		
		return cq;
	}

	@Override
	public int hashCode() {
		return Objects.hash(compPerc, component);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ComponentQuotaDTO other = (ComponentQuotaDTO) obj;
		return Objects.equals(compPerc, other.compPerc) && Objects.equals(component, other.component);
	}
	
	
}
