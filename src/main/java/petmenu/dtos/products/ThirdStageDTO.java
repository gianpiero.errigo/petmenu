package petmenu.dtos.products;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import javax.validation.Valid;

import petmenu.entities.products.Product;

public class ThirdStageDTO {
	
	private List<IngredientQuotaDTO> ingQuotasList = new ArrayList<IngredientQuotaDTO>();

	private Map<String, Double> analysis = new LinkedHashMap<String, Double>();

	
	public ThirdStageDTO() {}
	
	public ThirdStageDTO(List<IngredientQuotaDTO> ingQuotasList, Map<String, Double> analysis) {
		super();
		this.ingQuotasList = ingQuotasList;
		this.analysis = analysis;
	}
	
	
	public List<@Valid IngredientQuotaDTO> getIngQuotasList() {
		return ingQuotasList;
	}

	public void setIngQuotasList(List<IngredientQuotaDTO> ingQuotasList) {
		this.ingQuotasList = ingQuotasList;
	}
	
	public Boolean addIngredientQuotaDto(IngredientQuotaDTO iqDto) {
		return this.getIngQuotasList().add(iqDto);
	}
	
	public Map<String, Double> getAnalysis() {
		return analysis;
	}

	public void setAnalysis(Map<String, Double> analysis) {
		this.analysis = analysis;
	}
	
		
	public boolean analysisToProduct(Product prod) {
		
		List<String> mandatoryAnalysis = Arrays.asList("rawProtein", "rawFibre", "rawOilFat", "rawAshes", "moisture", "energy");
		
		if (!mandatoryAnalysis.stream().allMatch(this.getAnalysis().keySet()::contains))
			throw new IllegalArgumentException();
		
		prod.setRawProtein(this.getAnalysis().get("rawProtein"));
		prod.setRawFibre(this.getAnalysis().get("rawFibre"));
		prod.setRawOilFat(this.getAnalysis().get("rawOilFat"));
		prod.setRawAshes(this.getAnalysis().get("rawAshes"));
		prod.setMoisture(this.getAnalysis().get("moisture"));
		prod.setEnergy(this.getAnalysis().get("energy"));
		
		prod.setCalcium(this.getAnalysis().get("calcium"));
		prod.setSodium(this.getAnalysis().get("sodium"));
		prod.setPhosphorus(this.getAnalysis().get("phosphorus"));

		return true;
	}

	@Override
	public int hashCode() {
		return Objects.hash(analysis, ingQuotasList);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ThirdStageDTO other = (ThirdStageDTO) obj;
		return Objects.equals(analysis, other.analysis) && Objects.equals(ingQuotasList, other.ingQuotasList);
	}

}
