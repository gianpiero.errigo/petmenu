package petmenu.dtos.products;

import java.util.Objects;

public class SecondStageDTO {
	
	public SecondStageDTO() {};
	
	public SecondStageDTO(Boolean frontImg, Boolean backImg, Boolean detailsOneImg, Boolean detailsTwoImg) {
		super();
		this.frontImg = frontImg;
		this.backImg = backImg;
		this.detailsOneImg = detailsOneImg;
		this.detailsTwoImg = detailsTwoImg;
	}

	private Boolean frontImg = false;
	
	private Boolean backImg = false;
	
	private Boolean detailsOneImg = false;
	
	private Boolean detailsTwoImg = false;
	
		
	public Boolean getFrontImg() {
		return frontImg;
	}

	public void setFrontImg(Boolean frontImg) {
		this.frontImg = frontImg;
	}
	
	public Boolean getBackImg() {
		return backImg;
	}

	public void setBackImg(Boolean backImg) {
		this.backImg = backImg;
	}

	public Boolean getDetailsOneImg() {
		return detailsOneImg;
	}

	public void setDetailsOneImg(Boolean detailsOneImg) {
		this.detailsOneImg = detailsOneImg;
	}

	public Boolean getDetailsTwoImg() {
		return detailsTwoImg;
	}

	public void setDetailsTwoImg(Boolean detailsTwoImg) {
		this.detailsTwoImg = detailsTwoImg;
	}

	@Override
	public int hashCode() {
		return Objects.hash(backImg, detailsOneImg, detailsTwoImg, frontImg);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SecondStageDTO other = (SecondStageDTO) obj;
		return Objects.equals(backImg, other.backImg) && Objects.equals(detailsOneImg, other.detailsOneImg)
				&& Objects.equals(detailsTwoImg, other.detailsTwoImg) && Objects.equals(frontImg, other.frontImg);
	}
}
