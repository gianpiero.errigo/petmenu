package petmenu.dtos.products;

import java.util.Objects;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

public class FirstStageDTO {
	
	public FirstStageDTO() {};
	
	public FirstStageDTO(String brand, String line, String recipe,
			String prodType, String prodPackage, Integer netWeight) {
		this.brand = brand;
		this.line = line;
		this.recipe = recipe;
		this.prodType = prodType;
		this.prodPackage = prodPackage;
		this.netWeight = netWeight;
	}

	private String brand;
	
	private String line;
	
	private String recipe;
	
	private String prodType;
	
	private String prodPackage;
	
	private Integer netWeight;
	
	@NotBlank
	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}
	
	@NotBlank
	public String getLine() {
		return line;
	}

	public void setLine(String line) {
		this.line = line;
	}

	@NotBlank
	public String getRecipe() {
		return recipe;
	}

	public void setRecipe(String recipe) {
		this.recipe = recipe;
	}
	
	@NotBlank
	public String getProdType() {
		return prodType;
	}

	public void setProdType(String prodType) {
		this.prodType = prodType;
	}
	
	@NotBlank
	public String getProdPackage() {
		return prodPackage;
	}

	public void setProdPackage(String prodPackage) {
		this.prodPackage = prodPackage;
	}
	
	@Positive @NotNull
	public Integer getNetWeight() {
		return netWeight;
	}

	public void setNetWeight(Integer netWeight) {
		this.netWeight = netWeight;
	}

	@Override
	public int hashCode() {
		return Objects.hash(brand, line, netWeight, prodPackage, prodType, recipe);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FirstStageDTO other = (FirstStageDTO) obj;
		return Objects.equals(brand, other.brand) && Objects.equals(line, other.line)
				&& Objects.equals(netWeight, other.netWeight) && Objects.equals(prodPackage, other.prodPackage)
				&& Objects.equals(prodType, other.prodType) && Objects.equals(recipe, other.recipe);
	}
	
	
}
