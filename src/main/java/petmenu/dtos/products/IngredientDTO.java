package petmenu.dtos.products;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


public class IngredientDTO {
	
	private String name;

	private List<IngredientQuotaDTO> ingQuotasList = new ArrayList<IngredientQuotaDTO>();

	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public List<IngredientQuotaDTO> getIngQuotasList() {
		return ingQuotasList;
	}

	public void setIngQuotasList(List<IngredientQuotaDTO> ingQuotasList) {
		this.ingQuotasList = ingQuotasList;
	}
	
	public Boolean addIngredientQuotaDto(IngredientQuotaDTO iqDto) {
		return this.getIngQuotasList().add(iqDto);
	}

	@Override
	public int hashCode() {
		return Objects.hash(ingQuotasList, name);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		IngredientDTO other = (IngredientDTO) obj;
		return Objects.equals(ingQuotasList, other.ingQuotasList) && Objects.equals(name, other.name);
	}
	
}
