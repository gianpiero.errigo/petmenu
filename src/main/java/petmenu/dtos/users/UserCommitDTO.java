package petmenu.dtos.users;

import java.time.LocalDateTime;

import petmenu.entities.users.UserCommit;
import petmenu.entities.users.UserCommit.UCType;

public class UserCommitDTO {

	private Integer prodId;
	
	private String ucType;
	
	private Integer stage;
	
	private LocalDateTime time;

	
	public Integer getProdId() {
		return prodId;
	}

	public void setProdId(Integer prodId) {
		this.prodId = prodId;
	}

	public String getUcType() {
		return ucType;
	}

	public void setUcType(String ucType) {
		this.ucType = ucType;
	}

	public Integer getStage() {
		return stage;
	}

	public void setStage(Integer stage) {
		this.stage = stage;
	}

	public LocalDateTime getTime() {
		return time;
	}

	public void setTime(LocalDateTime time) {
		this.time = time;
	}
	
	public UserCommitDTO fromUserCommit(UserCommit uc) {
		
		UserCommitDTO ucDto = new UserCommitDTO();
		
		ucDto.setProdId(uc.getProdId());
		ucDto.setUcType(uc.getUCType().toString());
		ucDto.setStage(uc.getStage());
		ucDto.setTime(uc.getTime());
		
		return ucDto;
	}
	
	public UserCommit toUserCommit() {
		
		UserCommit uc = new UserCommit();
		
		uc.setProdId(getProdId());
		uc.setUCType(UCType.valueOf(getUcType()));
		uc.setStage(getStage());
		uc.setTime(getTime());
		
		return uc;
	}
}
