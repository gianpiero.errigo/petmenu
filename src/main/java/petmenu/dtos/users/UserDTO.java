package petmenu.dtos.users;

import java.time.LocalDateTime;

import petmenu.entities.users.User;

public class UserDTO {

	private String id;
	
	private String name;
	
	private LocalDateTime lastCommit;
	
	private Integer newCount = 0;
	
	private Integer editCount = 0;

	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public LocalDateTime getLastCommit() {
		return lastCommit;
	}

	public void setLastCommit(LocalDateTime lastCommit) {
		this.lastCommit = lastCommit;
	}
	
	public Integer getNewCount() {
		return newCount;
	}

	public void setNewCount(Integer newCount) {
		this.newCount = newCount;
	}

	public Integer getEditCount() {
		return editCount;
	}

	public void setEditCount(Integer editCount) {
		this.editCount = editCount;
	}

	public User toUser() {
		
		User user = new User();
		user.setId(getId());
		user.setName(getName());
		user.setName(getName());
		user.setNewCount(newCount);
		user.getEditCount();
		
		return user;
	}
	
	public UserDTO fromUser(User u) {
		
		this.setId(u.getId());
		this.setName(u.getName());
		this.setLastCommit(u.getLastCommit());
		this.setNewCount(u.getNewCount());
		this.setEditCount(u.getEditCount());
				
		return this;
	}

	
}
