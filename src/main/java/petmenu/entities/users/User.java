package petmenu.entities.users;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class User {
	
	private String id;
	
	private String name;
	
	private List<UserCommit> userCommitList = new ArrayList<UserCommit>();
	
	private LocalDateTime lastCommit;
	
	private Integer newCount = 0;
	
	private Integer editCount = 0;
	
	
	@Id
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
	public List<UserCommit> getUserCommitList() {
		return userCommitList;
	}

	public void setUserCommitList(List<UserCommit> userCommitList) {
		this.userCommitList = userCommitList;
	}

	public void addUserCommit(UserCommit uc) {
		this.userCommitList.add(uc);
	}

	public LocalDateTime getLastCommit() {
		return lastCommit;
	}

	public void setLastCommit(LocalDateTime lastCommit) {
		this.lastCommit = lastCommit;
	}

	public Integer getNewCount() {
		return newCount;
	}

	public void setNewCount(Integer newCount) {
		this.newCount = newCount;
	}

	public Integer getEditCount() {
		return editCount;
	}

	public void setEditCount(Integer editCount) {
		this.editCount = editCount;
	}


}
