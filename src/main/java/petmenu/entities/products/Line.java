package petmenu.entities.products;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotBlank;


@Entity
public class Line {

	private Integer id;
	
	private String name;
	
	private Brand brand;
	
	private Set<Product> products = new HashSet<Product>();
	

	public Line() {}
	
	public Line(String name, Brand brand, Set<Product> products) {
		super();
		this.name = name;
		this.brand = brand;
		this.products = products;
	}
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}

	@NotBlank
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "brand")
    public Brand getBrand() {
        return brand;
	}
	
	public void setBrand(Brand brand) {
		this.brand = brand;
	}

	@OneToMany(mappedBy = "line", cascade = CascadeType.ALL, orphanRemoval = true)
	public Set<Product> getProducts(){
		return products;
	}
	
	public void setProducts(Set<Product> products) {
		this.products = products;
	}
	
	public boolean addProduct(Product prod) {
		boolean success =  getProducts().add(prod);
		prod.setLine(this);
		return success;
	}
	
	public boolean rmProduct(Product prod) {
		boolean success = getProducts().remove(prod);
		prod.setLine(null);
		return success;
	}

	@Override
	public int hashCode() {
		return Objects.hash(brand, name);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Line other = (Line) obj;
		return Objects.equals(brand, other.brand) && Objects.equals(name, other.name);
	}

	@Override
	public String toString() {
		return "Line@" + (brand != null ? brand.getName() : "null") + "."  + name;
	}

}
