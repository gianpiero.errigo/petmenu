package petmenu.entities.products;

import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.NamedSubgraph;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.validation.constraints.NotBlank;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;


@Entity
@NamedEntityGraph(name = "fullFetchProduct", 
	attributeNodes = {
		@NamedAttributeNode(value = "line", subgraph = "lineFetchBrand"),
		@NamedAttributeNode(value = "ingQuotasList", subgraph = "fullFetchIq")
	},
	subgraphs = {
		@NamedSubgraph(name = "lineFetchBrand",
			attributeNodes = @NamedAttributeNode(value = "brand")
		),
		@NamedSubgraph(name = "fullFetchIq",
			attributeNodes = { 
				@NamedAttributeNode(value = "ing"),
				@NamedAttributeNode(value = "compQuotasList", subgraph = "cqFetchComp")
			}
		),
		@NamedSubgraph(name = "cqFetchComp",
			attributeNodes = @NamedAttributeNode(value = "comp")
		)
	}
)

public class Product {
	
	private Integer id;
	
	private Line line;
	
	private String recipe;
	
	public static enum ProdType {
		WET, DRY
	};
	
	public static enum ProdPackage {
		BAG, BOX, CAN, TRAY, POUCH
	};

	private Boolean hasFrontImg = false;
	
	private Boolean hasBackImg = false;
	
	private Boolean hasDetailsOneImg = false;
	
	private Boolean hasDetailsTwoImg = false;
	
	private ProdType prodType;
	
	private ProdPackage prodPackage;
	
	private Integer netWeight;
	
	private Double rawProtein;
	
	private Double rawFibre;
	
	private Double rawOilFat;
	
	private Double rawAshes;
	
	private Double moisture;
	
	private Double calcium;
	
	private Double sodium;

	private Double phosphorus;

	private Double energy;
	
	private Double carbs;
	
	private Set<IngredientQuota> ingQuotasList = new LinkedHashSet<IngredientQuota>();
	
	private Integer _stage = 0;

	
	public Product() {}
	
	public Product(Line line, String recipe, ProdType prodType, ProdPackage prodPackage, Integer netWeight,
			Set<IngredientQuota> ingQuotasList) {
		super();
		this.line = line;
		this.recipe = recipe;
		this.netWeight = netWeight;
		this.prodType = prodType;
		this.prodPackage = prodPackage;
		this.ingQuotasList = ingQuotasList;
	}

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}

	@NotBlank
	public String getRecipe() {
		return recipe;
	}

	public void setRecipe(String recipe) {
		this.recipe = recipe;
	}

	public Integer getNetWeight() {
		return netWeight;
	}

	public void setNetWeight(Integer netWeight) {
		this.netWeight = netWeight;
	}
	
	public Boolean getHasFrontImg() {
		return hasFrontImg;
	}

	public void setHasFrontImg(Boolean hasFrontImg) {
		this.hasFrontImg = hasFrontImg;
	}

	public Boolean getHasBackImg() {
		return hasBackImg;
	}

	public void setHasBackImg(Boolean hasBackImg) {
		this.hasBackImg = hasBackImg;
	}

	public Boolean getHasDetailsOneImg() {
		return hasDetailsOneImg;
	}

	public void setHasDetailsOneImg(Boolean hasDetailsOneImg) {
		this.hasDetailsOneImg = hasDetailsOneImg;
	}

	public Boolean getHasDetailsTwoImg() {
		return hasDetailsTwoImg;
	}

	public void setHasDetailsTwoImg(Boolean hasDetailsTwoImg) {
		this.hasDetailsTwoImg = hasDetailsTwoImg;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "line")
	public Line getLine(){
		return line;
	}
	
	public void setLine(Line line) {
		this.line = line;
	}

	@Enumerated(EnumType.STRING)
	public ProdType getProdType() {
		return prodType;
	}

	public void setProdType(ProdType prodType) {
		this.prodType = prodType;
	}

	@Enumerated(EnumType.STRING)
	public ProdPackage getProdPackage() {
		return prodPackage;
	}

	public void setProdPackage(ProdPackage prodPackage) {
		this.prodPackage = prodPackage;
	}
	
	@Column(precision = 3, scale = 1)
	public Double getRawProtein() {
		return rawProtein;
	}

	public void setRawProtein(Double rawProtein) {
		this.rawProtein = rawProtein;
	}

	@Column(precision = 3, scale = 1)
	public Double getRawFibre() {
		return rawFibre;
	}

	public void setRawFibre(Double rawFibre) {
		this.rawFibre = rawFibre;
	}

	@Column(precision = 3, scale = 1)
	public Double getRawOilFat() {
		return rawOilFat;
	}

	public void setRawOilFat(Double rawOilFat) {
		this.rawOilFat = rawOilFat;
	}

	@Column(precision = 3, scale = 1)
	public Double getRawAshes() {
		return rawAshes;
	}

	public void setRawAshes(Double rawAshes) {
		this.rawAshes = rawAshes;
	}

	@Column(precision = 3, scale = 1)
	public Double getMoisture() {
		return moisture;
	}

	public void setMoisture(Double moisture) {
		this.moisture = moisture;
	}
	
	@Column(precision = 3, scale = 1)
	public Double getCalcium() {
		return calcium;
	}

	public void setCalcium(Double calcium) {
		this.calcium = calcium;
	}
	
	@Column(precision = 3, scale = 1)
	public Double getSodium() {
		return sodium;
	}

	public void setSodium(Double sodium) {
		this.sodium = sodium;
	}

	@Column(precision = 3, scale = 1)
	public Double getPhosphorus() {
		return phosphorus;
	}

	public void setPhosphorus(Double phosphorus) {
		this.phosphorus = phosphorus;
	}

	public Double getEnergy() {
		return energy;
	}

	public void setEnergy(Double energy) {
		this.energy = energy;
	}

	@Column(precision = 3, scale = 1)
	public Double getCarbs() {
		return carbs;
	}

	public void setCarbs(Double carbs) {
		this.carbs = carbs;
	}
	
	@OneToMany(mappedBy = "product", cascade = CascadeType.ALL, orphanRemoval = true)
	@OrderBy("iqPosition")
	@Fetch(FetchMode.SUBSELECT)
	public Set<IngredientQuota> getIngQuotasList() {
		return ingQuotasList;
	}
	
	public void setIngQuotasList(Set<IngredientQuota> ingQuotasList) {
		this.ingQuotasList = ingQuotasList;
	}
	
	public boolean addIngredientQuota(IngredientQuota iq) {
		boolean success =  getIngQuotasList().add(iq);
		iq.setProduct(this);
		return success;
	}
	
	public boolean rmIngredientQuota(IngredientQuota iq) {
		boolean success = getIngQuotasList().remove(iq);
		iq.setProduct(null);
		return success;
	}
	
	public Integer get_stage() {
		return _stage;
	}

	public void set_stage(Integer _stage) {
		this._stage = _stage;
	}

	@Override
	public int hashCode() {
		return Objects.hash(line, netWeight, prodPackage, prodType, recipe);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Product other = (Product) obj;
		return Objects.equals(line, other.line) && Objects.equals(netWeight, other.netWeight)
				&& prodPackage == other.prodPackage && prodType == other.prodType
				&& Objects.equals(recipe, other.recipe);
	}

	@Override
	public String toString() {
		return "Product@" +
				(line != null 
					? (line.getBrand() != null ? line.getBrand().getName() : "null") + "." +
						line.getName() 
					: ("null" + "." + "null")) +
				"." + recipe;
	}

}