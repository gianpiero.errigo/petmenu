package petmenu.entities.products;

import java.util.Objects;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;


@Entity
public class ComponentQuota {
	
	private UUID id;
	
	private Integer cqPosition;
	
	private IngredientQuota iq;
		
	private Component comp;
	
	private Double compPerc;
	
	
	public ComponentQuota() {}
	
	public ComponentQuota(Integer cqPosition, IngredientQuota iq, Component comp, Double compPerc) {
		super();
		this.cqPosition = cqPosition;
		this.iq = iq;
		this.comp = comp;
		this.compPerc = compPerc;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(columnDefinition = "BINARY(16)")
	public UUID getId() {
		return id;
	}
	
	public void setId(UUID id) {
		this.id = id;
	}
	
	public Integer getCqPosition() {
		return cqPosition;
	}

	public void setCqPosition(Integer cqPosition) {
		this.cqPosition = cqPosition;
	}
	
	@ManyToOne
	@JoinColumn(name = "comp")
	public Component getComp() {
		return comp;
	}

	public void setComp(Component comp) {
		this.comp = comp;
	}

	@Column(precision = 3, scale = 1)
	public Double getCompPerc() {
		return compPerc;
	}

	public void setCompPerc(Double compPerc) {
		this.compPerc = compPerc;
	}

	@ManyToOne
	@JoinColumn(name = "iq")
	public IngredientQuota getIq() {
		return iq;
	}
	
	public void setIq(IngredientQuota iq) {
		this.iq = iq;
	}

	@Override
	public int hashCode() {
		return Objects.hash(comp, iq);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ComponentQuota other = (ComponentQuota) obj;
		return Objects.equals(comp, other.comp) && Objects.equals(iq, other.iq);
	}

	@Override
	public String toString() {
		return "ComponentQuota@" + id.getMostSignificantBits() + "." +
			(comp != null ? comp.getName() : "null");
	}

	
}
