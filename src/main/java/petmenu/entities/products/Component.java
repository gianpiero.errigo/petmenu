package petmenu.entities.products;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Component {
	
	private Integer id;
	
	private String name;
	
	
	private Set<ComponentQuota> compQuotas = new HashSet<ComponentQuota>();
	
	public Component() {}
	
	public Component(String name, Set<ComponentQuota> compQuotas) {
		super();
		this.name = name;
		this.compQuotas = compQuotas;
	}

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@OneToMany(mappedBy = "comp", cascade = CascadeType.ALL, orphanRemoval = true)
	public Set<ComponentQuota> getCompQuotas() {
		return compQuotas;
	}
	
	public void setCompQuotas(Set<ComponentQuota> compQuotas) {
		this.compQuotas = compQuotas;
	}
	
	public boolean addComponentQuota(ComponentQuota cq) {
		boolean success = getCompQuotas().add(cq);
		cq.setComp(this);
		return success;
	}

	public boolean rmComponentQuota(ComponentQuota cq) {
		boolean success = getCompQuotas().remove(cq);
		cq.setComp(null);
		return success;
	}

	@Override
	public int hashCode() {
		return Objects.hash(name);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Component other = (Component) obj;
		return Objects.equals(name, other.name);
	}

	@Override
	public String toString() {
		return "Component@" + name;
	}
	
}
