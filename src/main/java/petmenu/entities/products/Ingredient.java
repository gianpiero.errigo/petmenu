package petmenu.entities.products;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Ingredient {
	
	private Integer id;
	
	private String name;
	
	private Set<IngredientQuota> ingQuotas = new HashSet<IngredientQuota>();
	

	public Ingredient() {}
	
	public Ingredient(String name, Set<IngredientQuota> ingQuotas) {
		super();
		this.name = name;
		this.ingQuotas = ingQuotas;
	}

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@OneToMany(mappedBy = "ing", cascade = CascadeType.ALL, orphanRemoval = true)
	public Set<IngredientQuota> getIngQuotas() {
		return ingQuotas;
	}

	public void setIngQuotas(Set<IngredientQuota> ingQuotas) {
		this.ingQuotas = ingQuotas;
	}
	
	public boolean addIngredientQuota(IngredientQuota iq) {
		boolean success = getIngQuotas().add(iq);
		iq.setIng(this);
		return success;
	}

	public boolean rmIngredientQuota(IngredientQuota iq) {
		boolean success = getIngQuotas().remove(iq);
		iq.setIng(null);
		return success;
	}

	@Override
	public int hashCode() {
		return Objects.hash(name);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Ingredient other = (Ingredient) obj;
		return Objects.equals(name, other.name);
	}

	@Override
	public String toString() {
		return "Ingredient@" + name;
	}

}
