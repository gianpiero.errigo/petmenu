package petmenu.entities.products;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Brand {

	private Integer id;
	
	private String name;
	
	private Set<Line> lines = new HashSet<Line>(); 
	
	
	public Brand() {};
	
	public Brand(String name, Set<Line> lines) {
		super();
		this.name = name;
		this.lines = lines;
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@JsonIgnore
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	@NotBlank
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@JsonIgnore
	@OneToMany(mappedBy = "brand", cascade = CascadeType.ALL, orphanRemoval = true)
    public Set<Line> getLines() {
        return lines;
    }

	public void setLines(Set<Line> lines) {
		this.lines = lines;
	}
	
	public boolean addLine(Line line) {
		boolean success = getLines().add(line);
		line.setBrand(this);
		return success;
	}

	public boolean rmLine(Line line) {
		boolean success = getLines().remove(line);
		line.setBrand(null);
		return success;
	}

	@Override
	public int hashCode() {
		return Objects.hash(name);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Brand other = (Brand) obj;
		return Objects.equals(name, other.name);
	}

	@Override
	public String toString() {
		return "Brand@" + name;
	}
	
}
