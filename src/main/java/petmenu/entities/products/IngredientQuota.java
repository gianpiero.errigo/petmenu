package petmenu.entities.products;

import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;


@Entity
public class IngredientQuota implements Comparable<IngredientQuota>{
	
	private UUID id;
	
	private Integer iqPosition;
	
	private Product product;
	
	private Ingredient ing;
	
	private Double ingPerc;
	
	private Set<ComponentQuota> compQuotasList = new LinkedHashSet<ComponentQuota>();


	public IngredientQuota() {}
	
	public IngredientQuota(Integer iqPosition, Product product, Ingredient ing, Double ingPerc,
			Set<ComponentQuota> compQuotasList) {
		super();
		this.iqPosition = iqPosition;
		this.product = product;
		this.ing = ing;
		this.ingPerc = ingPerc;
		this.compQuotasList = compQuotasList;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(columnDefinition = "BINARY(16)")
	public UUID getId() {
		return id;
	}
	
	public void setId(UUID id) {
		this.id = id;
	}
	
	public Integer getIqPosition() {
		return iqPosition;
	}

	public void setIqPosition(Integer iqPosition) {
		this.iqPosition = iqPosition;
	}

	@ManyToOne
	@JoinColumn(name = "product")
	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	@ManyToOne
	@JoinColumn(name = "ing")
	public Ingredient getIng() {
		return ing;
	}

	public void setIng(Ingredient ing) {
		this.ing = ing;
	}

	@Column(precision = 3, scale = 1)
	public Double getIngPerc() {
		return ingPerc;
	}

	public void setIngPerc(Double ingPerc) {
		this.ingPerc = ingPerc;
	}

	@OneToMany(mappedBy = "iq", cascade = CascadeType.ALL, orphanRemoval = true)
	@OrderBy("cqPosition")
	@Fetch(FetchMode.SUBSELECT)
	public Set<ComponentQuota> getCompQuotasList() {
		return compQuotasList;
	}

	public void setCompQuotasList(Set<ComponentQuota> compQuotasList) {
		this.compQuotasList = compQuotasList;
	}
	
	public boolean addComponentQuota(ComponentQuota cq) {
		boolean success = this.getCompQuotasList().add(cq);
		cq.setIq(this);
		return success;
	}
	
	public boolean rmComponentQuota(ComponentQuota cq) {
		boolean success = this.getCompQuotasList().remove(cq);
		cq.setIq(null);
		return success;
	}

	@Override
	public int compareTo(IngredientQuota iq) {
		return this.getIngPerc().compareTo(-iq.getIngPerc());
	}

	@Override
	public int hashCode() {
		return Objects.hash(ing, product);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		IngredientQuota other = (IngredientQuota) obj;
		return Objects.equals(ing, other.ing) && Objects.equals(product, other.product);
	}

	@Override
	public String toString() {
		return "IngredientQuota@" + id.getMostSignificantBits() + "." +
			(ing != null ? ing.getName() : "null");
	}

}

