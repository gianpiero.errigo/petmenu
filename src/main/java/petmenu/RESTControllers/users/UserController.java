package petmenu.RESTControllers.users;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import petmenu.dtos.users.UserCommitDTO;
import petmenu.dtos.users.UserDTO;
import petmenu.services.users.UserService;

@RestController
@CrossOrigin(value = "${petmenu.allowed.cors.origins}")
@RequestMapping("/user")
public class UserController {
	
	@Autowired
	private UserService userService;
	
	@GetMapping(value = "/list", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> listUser() {
		
		List<UserDTO> usersDto = userService.findAll()
									.stream()
									.map(user -> new UserDTO().fromUser(user))
									.collect(Collectors.toList());
		
		return new ResponseEntity<List<UserDTO>>(usersDto, HttpStatus.OK);
	}
	
	@GetMapping(value = "/{userId}/commitlist", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> listUserCommit(@PathVariable("userId") String userId) {

		List<UserCommitDTO> ucDtoList = userService.findUserByIdFetchingUserCommitList(userId)
									.getUserCommitList()
									.stream()
									.map(uc -> new UserCommitDTO().fromUserCommit(uc))
									.collect(Collectors.toList());

		return new ResponseEntity<List<UserCommitDTO>>(ucDtoList, HttpStatus.ACCEPTED);

	}
}
