package petmenu.RESTControllers.products;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import petmenu.entities.products.Component;
import petmenu.services.products.ComponentService;

@RestController
@CrossOrigin(value = "${petmenu.allowed.cors.origins}")
@RequestMapping("/component")
public class ComponentController {
	
	@Autowired
	private ComponentService compService;
	
	@GetMapping(value = "/search", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> listIngredients() {
		
		return new ResponseEntity<List<String>>(
			compService.listByName().stream()
				.map(Component::getName)
				.collect(Collectors.toList()),
			HttpStatus.OK);

	}

}
