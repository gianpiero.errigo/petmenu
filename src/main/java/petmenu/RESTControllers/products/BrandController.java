package petmenu.RESTControllers.products;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import petmenu.entities.products.Brand;
import petmenu.services.products.BrandService;

@RestController
@CrossOrigin(value = "${petmenu.allowed.cors.origins}")
@RequestMapping("/brand")
public class BrandController {
	
	@Autowired
	private BrandService brandService;
	
	@GetMapping(value = "/search", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> listBrands() {
		
		return new ResponseEntity<List<String>>(
			brandService.listByName().stream()
				.map(Brand::getName)
				.collect(Collectors.toList()),
			HttpStatus.OK);

	}

}
