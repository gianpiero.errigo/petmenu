package petmenu.RESTControllers.products;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import petmenu.dtos.products.FirstStageDTO;
import petmenu.dtos.products.ProductDTO;
import petmenu.dtos.products.SecondStageDTO;
import petmenu.dtos.products.ThirdStageDTO;
import petmenu.entities.products.Product;
import petmenu.entities.users.User;
import petmenu.exceptions.ProductAlreadyPresentException;
import petmenu.services.products.ProductService;
import petmenu.services.products.StageMapperService;
import petmenu.services.users.UserCommitService;

@RestController
@CrossOrigin(value = "${petmenu.allowed.cors.origins}")
@RequestMapping(value = "/product", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
public class ProductController {

	@Autowired
	private ProductService prodService;
	@Autowired
	private StageMapperService smService;
	@Autowired
	private UserCommitService ucService;

	
	@GetMapping("/{id}/firststage")
	public ResponseEntity<?> viewFirstStage(@PathVariable("id") Integer prodId) {

		Product prod = prodService.findProductById(prodId);
		return new ResponseEntity<FirstStageDTO>(smService.fsDtoFrom(prod), HttpStatus.OK);
	}
	
	@PostMapping
	public ResponseEntity<Map<String, Integer>> firstStageInput(
			@RequestBody @Valid FirstStageDTO fsDto,
			@AuthenticationPrincipal(expression = "@userService.createFrom(#this)") User user) {

		Product product;

		try {
			
			product = prodService.createFrom(fsDto);
		} 
		
		catch (ProductAlreadyPresentException exc) {

			throw new ResponseStatusException(HttpStatus.CONFLICT, "Product already present in DB", exc);
		}
		
		ucService.createFrom(user, product.getId(), "NEW", 1);
		
		Map<String, Integer> res = Map.of("id", product.getId(), "_stage", product.get_stage());

		return new ResponseEntity<Map<String, Integer>>(res, HttpStatus.CREATED);
	}
	
	@PutMapping("/{id}/firststage")
	public ResponseEntity<String> firstStageEdit(@PathVariable("id") Integer id,
			@RequestBody @Valid FirstStageDTO fsDto,
			@AuthenticationPrincipal(expression = "@userService.createFrom(#this)") User user) {
		
		try {

			prodService.updateFrom(id, fsDto);
		}

		catch (ProductAlreadyPresentException exc) {

			throw new ResponseStatusException(HttpStatus.CONFLICT, "Product already present in DB", exc);
		}
		
		ucService.createFrom(user, id, "EDIT", 1);

		return new ResponseEntity<String>("{\"msg\":\"Product updated\"}", HttpStatus.OK);
	}
	
	@GetMapping("/{id}/secondstage")
	public ResponseEntity<?> viewSecondStage(@PathVariable("id") Integer prodId) {

		Product prod = prodService.findProductById(prodId);
		return new ResponseEntity<SecondStageDTO>(smService.ssDtoFrom(prod), HttpStatus.OK);
	}

	@PostMapping(value = "/{id}/secondstage", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	public ResponseEntity<?> secondStageInput(
			@PathVariable("id") Integer prodId,
			@Nullable @RequestPart(required = false) MultipartFile frontImg,
			@Nullable @RequestPart(required = false) MultipartFile backImg,
			@Nullable @RequestPart(required = false) MultipartFile detailsOneImg,
			@Nullable @RequestPart(required = false) MultipartFile detailsTwoImg,
			@AuthenticationPrincipal(expression = "@userService.createFrom(#this)") User user) {
		
		try {

			prodService.setSecondStage(prodId, frontImg, backImg, detailsOneImg, detailsTwoImg);
		}
		
		catch(ProductAlreadyPresentException exc) {
			
			throw new ResponseStatusException(HttpStatus.CONFLICT, "Product already present in DB", exc);
		}

		ucService.createFrom(user, prodId, "NEW", 2);
		
		Map<String, Integer> res = Map.of("id", prodId, "_stage", 2);
		
		return new ResponseEntity<Map<String, Integer>>(res, HttpStatus.CREATED);
	}
	
	@PutMapping(value = "/{id}/secondstage", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	public ResponseEntity<?> secondStageEdit(
			@PathVariable("id") Integer prodId,
			@Nullable @RequestPart(required = false) MultipartFile frontImg,
			@Nullable @RequestPart(required = false) MultipartFile backImg,
			@Nullable @RequestPart(required = false) MultipartFile detailsOneImg,
			@Nullable @RequestPart(required = false) MultipartFile detailsTwoImg,
			@AuthenticationPrincipal(expression = "@userService.createFrom(#this)") User user) {

		prodService.updateSecondStage(prodId, frontImg, backImg, detailsOneImg, detailsTwoImg);
		
		ucService.createFrom(user, prodId, "EDIT", 2);

		return new ResponseEntity<String>("{\"msg\":\"Stage 2 updated\"}", HttpStatus.OK);
	}
	
	@GetMapping("/{id}/thirdstage")
	public ResponseEntity<?> viewThirdStage(@PathVariable("id") Integer prodId) {

		Product prod = prodService.findProductByIdFullFetch(prodId);
	
		return new ResponseEntity<ThirdStageDTO>(smService.tsDtoFrom(prod), HttpStatus.OK);
	}

	@PostMapping("/{id}/thirdstage")
	public ResponseEntity<Map<String, Integer>> thirdStageInput(
			@PathVariable("id") Integer prodId,
			@RequestBody @Valid ThirdStageDTO thirdStageDTO,
			@AuthenticationPrincipal(expression = "@userService.createFrom(#this)") User user) {

		try {
			
			prodService.setThirdStage(prodId, thirdStageDTO);
		}
		
		catch(ProductAlreadyPresentException exc) {
			
			throw new ResponseStatusException(HttpStatus.CONFLICT, "Product already present in DB", exc);
		}

		ucService.createFrom(user, prodId, "NEW", 3);

		Map<String, Integer> res = Map.of("id", prodId, "_stage", 3);
				
		return new ResponseEntity<Map<String, Integer>>(res, HttpStatus.CREATED);
	}
	
	@PutMapping("/{id}/thirdstage")
	public ResponseEntity<String> thirdStageEdit(
			@PathVariable("id") Integer prodId,
			@RequestBody @Valid ThirdStageDTO thirdStageDTO,
			@AuthenticationPrincipal(expression = "@userService.createFrom(#this)") User user) {
		
		prodService.updateThirdStage(prodId, thirdStageDTO);

		ucService.createFrom(user, prodId, "EDIT", 3);

		return new ResponseEntity<String>("{\"msg\":\"Stage 3 updated\"}", HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<ProductDTO> viewProductC(@PathVariable("id") Integer prodId) {

		Product prod = prodService.findProductByIdFullFetch(prodId);
		
		return new ResponseEntity<ProductDTO>(smService.productDtoFrom(prod), HttpStatus.OK);
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<String> deleteProduct(@PathVariable("id") Integer prodId) {

		prodService.delete(prodId);

		return new ResponseEntity<String>("{\"msg\":\"Product "+prodId+" deleted\"}", HttpStatus.OK);
	}
	
	@GetMapping("/search")
	public ResponseEntity<List<ProductDTO>> listProducts(
			@RequestParam(required = false) String brand,
			@RequestParam(required = false) String line,
			@RequestParam(required = false) String recipe,
			@RequestParam(required = false) String ing,
			@RequestParam(required = false) String comp,
			@RequestParam(required = false) String prodType,
			@RequestParam(required = false) String prodPackage,
			@RequestParam(required = false) Integer minWeight,
			@RequestParam(required = false) Integer maxWeight,
			@RequestParam(required = false) Integer stage) {

		return new ResponseEntity<List<ProductDTO>>(
			prodService.findProductByMainTraitsSpec(brand, line, recipe, ing, comp, prodType, prodPackage, minWeight, maxWeight, stage)
				.stream()
				.map(prod -> smService.productDtoFrom(prod))
				.collect(Collectors.toList()),
			HttpStatus.OK);
	}
	
	@GetMapping("/search/stage")
	public ResponseEntity<List<ProductDTO>> listProducts(@RequestParam Integer stage) {

		return new ResponseEntity<List<ProductDTO>>(
			prodService.findProductByStageFullFetch(stage)
				.stream()
				.map(prod -> smService.productDtoFrom(prod))
				.collect(Collectors.toList()),
			HttpStatus.OK);
	}
	
	@GetMapping("/search/firstStageByLine")
	public ResponseEntity<List<String>> searchFirstStage(@RequestParam String brand, @RequestParam String line) {
		return new ResponseEntity<List<String>>(prodService.findRecipeByLine(brand, line), HttpStatus.OK);
	}
	
	@GetMapping("/test")
	public ResponseEntity<String> testJson() {
		return new ResponseEntity<String>("{\"root\":{\"prop1\":\"1\",\"prop2\":\"2\"}}", HttpStatus.OK);
	}

}
