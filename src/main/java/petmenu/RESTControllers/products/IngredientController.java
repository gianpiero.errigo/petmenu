package petmenu.RESTControllers.products;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import petmenu.entities.products.Ingredient;
import petmenu.services.products.IngredientService;

@RestController
@CrossOrigin(value = "${petmenu.allowed.cors.origins}")
@RequestMapping("/ingredient")
public class IngredientController {
	
	@Autowired
	private IngredientService ingService;
	
	@GetMapping(value = "/search", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> listIngredients() {
		
		return new ResponseEntity<List<String>>(
			ingService.listByName().stream()
				.map(Ingredient::getName)
				.collect(Collectors.toList()),
			HttpStatus.OK);

	}

}
