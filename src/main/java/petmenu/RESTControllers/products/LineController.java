package petmenu.RESTControllers.products;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import petmenu.entities.products.Line;
import petmenu.services.products.BrandService;
import petmenu.services.products.LineService;

@RestController
@CrossOrigin(value = "${petmenu.allowed.cors.origins}")
@RequestMapping("/line")
public class LineController {
	
	@Autowired
	private LineService lineService;
	@Autowired
	private BrandService brandService;
	
	@GetMapping(value = "/search", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> listLinesByBrand(@RequestParam String brand) {
		
		return new ResponseEntity<List<String>>(
			lineService.findByBrand(brandService.findByName(brand)).stream()
				.map(Line::getName)
				.collect(Collectors.toList()),
			HttpStatus.OK);

	}

}
