package petmenu.RESTControllers.auth;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import petmenu.services.auth.AuthManagementService;

@RestController
@CrossOrigin(value = "${petmenu.allowed.cors.origins}")
@RequestMapping("/auth")
public class AuthManagementController {
	
	@Autowired
	private AuthManagementService authService;
	
	@PatchMapping(value = "/change/name", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> changeName(
			@RequestBody @Valid String newName,
			@AuthenticationPrincipal Jwt principal
			) {
					
		return new ResponseEntity<String>(authService.changeName(newName, principal.getSubject()), HttpStatus.OK);

	}

}
