FROM adoptopenjdk/openjdk11:alpine-jre

RUN addgroup -S user --gid 1000 && adduser -S user -G user --uid 1000
USER user

ENV SERVER_ADDRESS=192.168.1.10
ENV SERVER_PORT=9080
ENV CORS_ORIGINS=https://gianpiero.errigo.gitlab.io

ENV PRODUCTS_DB_URL=jdbc:mariadb://localhost:3306/PetMenu_products_db
ENV PRODUCTS_DB_USER=root
ENV PRODUCTS_DB_PASS=root

ENV USERS_DB_URL=jdbc:mariadb://localhost:3306/PetMenu_users_db
ENV USERS_DB_USER=root
ENV USERS_DB_PASS=root

ENV AUTH_SERVER_URL=http://79.9.147.80:8180/auth
ENV AUTH_SSL_REQUIRED=none

VOLUME /tmp
COPY run.sh /bin/run.sh
COPY app.jar .
ENTRYPOINT ["run.sh"]
