#!/bin/sh

DOCKER_ENVS="SERVER_ADDRESS \
			SERVER_PORT \
			CORS_ORIGINS \
			PRODUCTS_DB_URL \
			PRODUCTS_DB_USER \
			PRODUCTS_DB_PASS \
			USERS_DB_URL \
			USERS_DB_USER \
			USERS_DB_PASS \
			AUTH_SERVER_URL \
			AUTH_SSL_REQUIRED"
MISSING_ENVS=''
for docker_env_name in $DOCKER_ENVS
	do
		eval DOCKER_ENV=\$$docker_env_name
		if [ -z $DOCKER_ENV ]
			then MISSING_ENVS="$MISSING_ENVS $docker_env_name"
		fi
	done

if [ ! -z $MISSING_ENVS ]
	then echo "Missing value for for environment variables: $MISSING_ENVS"
	exit 1
fi

set -x

exec java ${JAVA_OPTS} -jar /app.jar \
	--server.address=$SERVER_ADDRESS \
	--server.port=$SERVER_PORT \
	--petmenu.allowed.cors.origins=$CORS_ORIGINS \
	--petmenu.datasource.products.url=$PRODUCTS_DB_URL \
	--petmenu.datasource.products.username=$PRODUCTS_DB_USER \
	--petmenu.datasource.products.password=$PRODUCTS_DB_PASS \
	--petmenu.datasource.users.url=$USERS_DB_URL \
	--petmenu.datasource.users.username=$USERS_DB_USER \
	--petmenu.datasource.users.password=$USERS_DB_PASS \
	--keycloak.auth-server-url=$AUTH_SERVER_URL \
	--keycloak.ssl-required=$AUTH_SSL_REQUIRED
